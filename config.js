var config = {};

config.select = 'Select';
config.ongoing = {
    'variety' : 'Variety',
    'trial' : 'Trial',
    'set_forget' : 'Set it & Forget it'
};
config.prefs = {
    'Joy of Food' : 'joy_of_food',
    'Food as Fuel': 'food_as_fuel',
    'Feeding Families': 'feeding_families',
    'Solution Seekers': 'solution_seekers'
};
module.exports = config;