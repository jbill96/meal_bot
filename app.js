"use strict";
const builder = require("botbuilder");
const botbuilder_azure = require("botbuilder-azure");
const azure = require('azure-storage');
const path = require('path');
const https = require('https');
const date = require('date-and-time');
const fs = require('fs');
const uuid = require('uuid');
const async = require('async');
const util = require('util');
const restify = require('restify');
const express = require('express');
var assert = require('assert');
require('dotenv').config();
const config = require('./config.js');
const apiai = require('./apiai.js');
const recognizer = new apiai(process.env.APIAI_TOKEN);
const translator = require('./translator.js');
const logger = require('./log.js');
const meal_rec_db = require('./db.js');

var intents = new builder.IntentDialog({
    recognizers: [recognizer]
});

const SITE = 'https://devgalahadchat.azurewebsites.net/meal_bot/%s.png';
const REC_PIC_SITE = 'https://demo.galahadgroupinc.com/imgr/%s'

// Setup Restify Server
var server = express();
server.use('/images', express.static(__dirname + '/images'));
server.use('/meal_bot', express.static(__dirname + '/meal_bot'));
server.listen(process.env.port || process.env.PORT || 3978, function () {
    logger.info('started server', {
        port: 3978
    });
});

// Create chat connector for communicating with the Bot Framework Service
var connector = new builder.ChatConnector({
    appId: process.env.MicrosoftAppId,
    appPassword: process.env.MicrosoftAppPassword,
    stateEndpoint: process.env.BotStateEndpoint,
    openIdMetadata: process.env.BotOpenIdMetadata
});

server.post('/api/messages', connector.listen());

server.get('/userData/', (req, res) => {
    res.send('hey');
});

var bot = new builder.UniversalBot(connector);

// Handle message from user
bot.dialog('/', intents);

// resets a user's session
intents.matches('delete data', (session, args) => {
    logger.warn('reseting session');
    session.userData = {};
    session.conversationData = {}
    session.privateConversationData = {}
    session.send('RESET SESSION');
});

// initial greetings
intents.matches('smalltalk.greetings.hello', (session, args) => {
    logger.info({
        intent: 'smalltalk.greetings.hello',
        messages: args.messages,
        lang: args.lang
    });
    //console.log(session);
    session.userData.lang = args.lang;
    session.userData.recipe_ban_list = [];
    session.userData.cart = {};
    send_messages(session, args.messages);
});

intents.matches('order_cancel', (session, args) => {
    logger.info({
        intent: 'order_cancel',
        messages: args.messages
    });
    send_messages(session, args.messages);
});

intents.matches('order_cancel - no', (session, args) => {
    logger.info({
        intent: 'order_cancel - no',
        messages: args.messages
    });
    send_messages(session, args.messages);
});

intents.matches('order_cancel - yes', (session, args) => {
    logger.info({
        intent: 'order_cancel - yes',
        messages: args.messages
    });
    send_messages(session, args.messages);
});

intents.matches('order_cancel_followup - no', (session, args) => {
    logger.info({
        intent: 'order_cancel_followup - no',
        messages: args.messages
    });
    send_messages(session, args.messages);
});

intents.matches('order_cancel_followup - yes', (session, args) => {
    logger.info({
        intent: 'order_cancel_followup - yes',
        messages: args.messages
    });
    send_messages(session, args.messages);
});

intents.matches('acct_mgmt', (session, args) => {
    logger.info({
        intent: 'acct_mgmt',
        messages: args.messages
    });
    send_messages(session, args.messages);
});


intents.matches('meal_plan-yes', (session, args) => {
    logger.info({
        intent: 'meal_plan-yes',
        messages: args.messages
    });
    send_messages(session, args.messages);

    var msg = new builder.Message(session)
        .addAttachment({
            contentType: "application/vnd.microsoft.card.adaptive",
            content: {
                type: "AdaptiveCard",
                "body": [{
                        "type": "TextBlock",
                        "text": 'Proteins',
                        "size": "large"
                    },
                    {
                        "type": "Input.ChoiceSet",
                        "id": "pick_list",
                        "isMultiSelect": true,
                        "isRequired": true,
                        "style": "compact",
                        "choices": [{
                                "title": "Chicken",
                                "value": "Chicken"
                            },
                            {
                                "title": "Beef",
                                "value": "Beef"
                            },
                            {
                                "title": "Turkey",
                                "value": "Turkey"
                            },
                            {
                                "title": "Pork",
                                "value": "Pork"
                            },
                            {
                                "title": "Other Meat",
                                "value": "other meat"
                            },
                            {
                                "title": "Tofu",
                                "value": "Tofu"
                            },
                            {
                                "title": "Lentil",
                                "value": "Lentil"
                            },
                            {
                                "title": "Egg",
                                "value": "Egg"
                            },
                            {
                                "title": "Fish",
                                "value": "Fish"
                            },
                            {
                                "title": "Shellfish",
                                "value": "Shellfish"
                            },
                            {
                                "title": "Cheese",
                                "value": "Cheese"
                            },
                            {
                                "title": "Other Protein",
                                "value": "other protein"
                            }
                        ]
                    }
                ],
                "actions": [{
                    "type": "Action.Submit",
                    "title": config.select,
                    "data": {
                        // any additional data
                    }
                }]
            }
        });
    session.send(msg);
});


intents.matches('meal_plan-yes-veg', (session, args) => {
    logger.info({
        intent: 'meal_plan-yes-veg',
        messages: args.messages
    });

    var key = 'protein';
    add_param_to_user_cart(key, parse_params(args.parameters, key), session);
    send_messages(session, args.messages);
    var msg = new builder.Message(session)
        .addAttachment({
            contentType: "application/vnd.microsoft.card.adaptive",
            content: {
                type: "AdaptiveCard",
                "body": [{
                        "type": "TextBlock",
                        "text": 'Vegetables',
                        "size": "large"
                    },
                    {
                        "type": "Input.ChoiceSet",
                        "id": "pick_list",
                        "isMultiSelect": true,
                        "isRequired": true,
                        "style": "compact",
                        "choices": [{
                                "title": "Lettuce",
                                "value": "Lettuce"
                            },
                            {
                                "title": "Onion",
                                "value": "Onion"
                            },
                            {
                                "title": "Tomato",
                                "value": "Tomato"
                            },
                            {
                                "title": "Mushroom",
                                "value": "Mushroom"
                            },
                            {
                                "title": "Corn",
                                "value": "Corn"
                            },
                            {
                                "title": "Kale",
                                "value": "Kale"
                            },
                            {
                                "title": "Beans",
                                "value": "Beans"
                            },
                            {
                                "title": "Spinach",
                                "value": "Spinach"
                            },
                            {
                                "title": "Broccoli",
                                "value": "Broccoli"
                            },
                            {
                                "title": "Brussel Sprouts",
                                "value": "Brussel Sprouts"
                            },
                            {
                                "title": "Cabbage",
                                "value": "Cabbage"
                            },
                            {
                                "title": "Carrots",
                                "value": "Carrots"
                            },
                            {
                                "title": "Peppers",
                                "value": "Peppers"
                            },
                            {
                                "title": "Squash",
                                "value": "Squash"
                            },
                            {
                                "title": "Artichoke",
                                "value": "Artichoke"
                            },
                            {
                                "title": "Green Beans",
                                "value": "green beans"
                            },
                            {
                                "title": "Peas",
                                "value": "Peas"
                            },
                            {
                                "title": "Cauliflower",
                                "value": "Cauliflower"
                            },
                            {
                                "title": "Carrot",
                                "value": "Carrot"
                            },
                            {
                                "title": "Cucumber",
                                "value": "Cucumber"
                            },
                            {
                                "title": "Pumpkin",
                                "value": "Pumpkin"
                            },
                            {
                                "title": "Sweet Potato",
                                "value": "Sweet Potato"
                            },
                            {
                                "title": "Zucchini",
                                "value": "Zucchini"
                            },
                            {
                                "title": "Shallots",
                                "value": "Shallots"
                            },
                            {
                                "title": "Asparagus",
                                "value": "Asparagus"
                            },
                            {
                                "title": "Other Vegetable",
                                "value": "other vegetable"
                            },
                            {
                                "title": "Mixed Green Salad",
                                "value": "mixed green salad"
                            },
                            {
                                "title": "Cobb Salad",
                                "value": "cobb salad"
                            },
                            {
                                "title": "Garden Salad",
                                "value": "Garden Salad"
                            },
                            {
                                "title": "Fruit Salad",
                                "value": "Fruit Salad"
                            },
                            {
                                "title": "Other vegetable",
                                "value": "Other vegetable"
                            }
                        ]
                    }
                ],
                "actions": [{
                    "type": "Action.Submit",
                    "title": config.select,
                    "data": {
                        // any additional data
                    }
                }]
            }
        });
    session.send(msg);
});

intents.matches('meal_plan-yes-veg-carbs', (session, args) => {
    logger.info({
        intent: 'meal_plan-yes-veg-carbs',
        messages: args.messages
    });

    var key = 'veg';
    add_param_to_user_cart(key, parse_params(args.parameters, key), session);
    send_messages(session, args.messages);

    var msg = new builder.Message(session)
        .addAttachment({
            contentType: "application/vnd.microsoft.card.adaptive",
            content: {
                type: "AdaptiveCard",
                "body": [{
                        "type": "TextBlock",
                        "text": 'Carbohydrates',
                        "size": "large"
                    },
                    {
                        "type": "Input.ChoiceSet",
                        "id": "pick_list",
                        "isMultiSelect": true,
                        "isRequired": true,
                        "style": "compact",
                        "choices": [{
                                "title": "Pasta",
                                "value": "Pasta"
                            },
                            {
                                "title": "White Rice",
                                "value": "White Rice"
                            },
                            {
                                "title": "Brown Rice",
                                "value": "Brown Rice"
                            },
                            {
                                "title": "White Bread",
                                "value": "White Bread"
                            },
                            {
                                "title": "Non white bread",
                                "value": "Non white bread"
                            },
                            {
                                "title": "Red Potato",
                                "value": "Red Potato"
                            },
                            {
                                "title": "Russet Patato",
                                "value": "Russet Patato"
                            },
                            {
                                "title": "Grits",
                                "value": "Grits"
                            },
                            {
                                "title": "Polenta",
                                "value": "Polenta"
                            },
                            {
                                "title": "Other Grains",
                                "value": "Other Grains"
                            }
                        ]
                    }
                ],
                "actions": [{
                    "type": "Action.Submit",
                    "title": config.select,
                    "data": {
                        // any additional data
                    }
                }]
            }
        });
    session.send(msg);
});

intents.matches('meal_plan-yes-veg-carbs-prefs', (session, args) => {
    logger.info({
        intent: 'meal_plan-yes-veg-carbs-prefs',
        messages: args.messages
    });

    var key = 'carbs';
    add_param_to_user_cart(key, parse_params(args.parameters, key), session);
    send_messages(session, args.messages);

    var msg = new builder.Message(session)
        .attachmentLayout(builder.AttachmentLayout.carousel);
    var file_names = ['feeding_families', 'food_as_fuel', 'joy_of_food', 'solution_seekers'];
    var text_names = ['Feeding Families', 'Food as Fuel', 'Joy of Food', 'Solution Seekers'];

    for (var i = 0; i < file_names.length; i++) {
        msg.addAttachment(make_pref_card(session, file_names[i], text_names[i]));
    }
    session.send(msg);
});

function make_pref_card(session, file, text) {
    return new builder.HeroCard(session)
        .images([builder.CardImage.create(session, util.format(SITE, file))])
        .buttons([
            builder.CardAction.imBack(session, text, text)
        ]);
}

intents.matches('meal_plan-prefs', (session, args) => {
    logger.info({
        intent: 'meal_plan-prefs',
        messages: args.messages
    });

    var key = 'prefs';
    add_param_to_user_cart(key, parse_params(args.parameters, key), session);
    send_messages(session, args.messages);

    var msg = new builder.Message(session)
        .addAttachment({
            contentType: "application/vnd.microsoft.card.adaptive",
            content: {
                type: "AdaptiveCard",
                "body": [{
                        "type": "TextBlock",
                        "text": 'Cuisine',
                        "size": "large"
                    },
                    {
                        "type": "Input.ChoiceSet",
                        "id": "pick_list",
                        "isMultiSelect": true,
                        "isRequired": true,
                        "style": "compact",
                        "choices": [{
                                "title": "Traditional",
                                "value": "Traditional"
                            },
                            {
                                "title": "Hispanic",
                                "value": "Hispanic"
                            },
                            {
                                "title": "Asian",
                                "value": "Asian"
                            },
                            {
                                "title": "Italian",
                                "value": "Italian"
                            },
                            {
                                "title": "French",
                                "value": "French"
                            },
                            {
                                "title": "Other Cuisine",
                                "value": "Other Cuisine"
                            }
                        ]
                    }
                ],
                "actions": [{
                    "type": "Action.Submit",
                    "title": config.select,
                    "data": {
                        // any additional data
                    }
                }]
            }
        });
    session.send(msg);

});

intents.matches('meal_plan-days', (session, args) => {
    logger.info({
        intent: 'meal_plan-days',
        messages: args.messages
    });
    add_param_to_user_cart('days', args.parameters.days, session);
    send_messages(session, args.messages);
});

intents.matches('meal_plan-cuisine', (session, args) => {
    logger.info({
        intent: 'meal_plan-cuisine',
        messages: args.messages
    });

    var key = 'cuisine';
    add_param_to_user_cart(key, parse_params(args.parameters, key), session);
    send_messages(session, args.messages);

    var msg = new builder.Message(session)
        .addAttachment({
            contentType: "application/vnd.microsoft.card.adaptive",
            content: {
                type: "AdaptiveCard",
                "body": [{
                        "type": "TextBlock",
                        "text": 'Dietary Restrictions',
                        "size": "large"
                    },
                    {
                        "type": "Input.ChoiceSet",
                        "id": "pick_list",
                        "isMultiSelect": true,
                        "isRequired": true,
                        "style": "compact",
                        "choices": [{
                                "title": "None",
                                "value": "None"
                            }, {
                                "title": "Vegan",
                                "value": "Vegan"
                            },
                            {
                                "title": "Vegetarian",
                                "value": "Vegetarian"
                            },
                            {
                                "title": "Gluten Free",
                                "value": "Gluten Free"
                            },
                            {
                                "title": "Sugar Free",
                                "value": "Sugar Free"
                            },
                            {
                                "title": "Peanut Free",
                                "value": "Peanut Free"
                            }
                        ]
                    }
                ],
                "actions": [{
                    "type": "Action.Submit",
                    "title": config.select,
                    "data": {
                        // any additional data
                    }
                }]
            }
        });
    session.send(msg);
});


intents.matches('meal_plan-diet_res', (session, args) => {
    logger.info({
        intent: 'meal_plan-diet_res',
        messages: args.messages
    });

    var key = 'diet_res';
    add_param_to_user_cart(key, parse_params(args.parameters, key), session);
    send_messages(session, args.messages);
});

intents.matches('meal_plan-size', (session, args) => {
    logger.info({
        intent: 'meal_plan-size',
        messages: args.messages
    });

    add_param_to_user_cart('size', args.parameters.size, session);
    send_messages(session, args.messages);
});


function create_recipe_card(doc) {
    if (doc) {
        var name = doc.name;
        var protein0 = doc.protein0;
        var protein1 = doc.protein1;
        var veg0 = doc.veg0;
        var veg1 = doc.veg1;
        var carb = doc.carb0;
        var time = doc.time;
        var cuisine = doc.cuisine;
        var pic = doc.images;
        var price = doc.price;
        return {

            contentType: "application/vnd.microsoft.card.adaptive",
            content: {
                "type": "AdaptiveCard",
                "version": "0.5",
                "body": [{
                        "type": "TextBlock",
                        "text": name,
                        "weight": "bolder",
                        "wrap": true
                    },
                    {
                        "type": "Image",
                        "size": "stretch",
                        "url": util.format(REC_PIC_SITE, pic),
                    }
                ]
            }
        };
    } else {
        return {
            contentType: "application/vnd.microsoft.card.adaptive",
            content: {
                "type": "AdaptiveCard",
                "version": "0.5",
                "body": [{
                        "type": "TextBlock",
                        "text": 'No more recommendations to show',
                        "weight": "bolder",
                        "wrap": true
                    }
                ]
            }
        };
    }

}

function create_user_card(user) {
    var id = user.user_id;
    var protein = user.protein;
    var veg = user.veg;
    var carbs = user.carbs;
    var prefs = user.prefs;
    var cuisine = user.cuisine;
    var meal_size = user.meal_size;
    var diet_res = user.diet_res;
    var ongoing = user.ongoing;
    var prefs_file_name = config.prefs[prefs];

    return {
        contentType: "application/vnd.microsoft.card.adaptive",
        content: {
            type: "AdaptiveCard",
            "body": [{
                    "type": "TextBlock",
                    "text": "User Info",
                    "size": "large",
                    "isSubtle": true
                },
                {
                    "type": "ColumnSet",
                    "columns": [{
                        "type": "Column",
                        "width": "stretch",
                        "items": [{
                                "type": "FactSet",
                                "facts": [{
                                        "title": "Proteins:",
                                        "value": protein
                                    },
                                    {
                                        "title": "Vegetables:",
                                        "value": veg
                                    },
                                    {
                                        "title": "Carbohydrates:",
                                        "value": carbs
                                    },
                                    {
                                        "title": "Cooking Preferences:",
                                        "value": prefs
                                    },
                                    {
                                        "title": "Cuisine:",
                                        "value": cuisine
                                    },
                                    {
                                        "title": 'Meal Size:',
                                        "value": meal_size
                                    },
                                    {
                                        "title": "Dietary Restriction:",
                                        "value": diet_res
                                    },
                                    {
                                        "title": "Meal Plan:",
                                        "value": ongoing
                                    }
                                ]
                            },
                            {
                                "type": "Image",
                                "url": util.format(SITE, prefs_file_name),
                                "size": "stretch"
                            }
                        ]
                    }]
                }
            ]
        }
    };
}

function create_nutri_field(field) {
    return field.qty + field.unit
}

function create_nutri_card(rec, meal_name) {
    //console.log(rec);
    var calories = create_nutri_field(rec['nutrition']['Calories']);
    var total_fat = create_nutri_field(rec['nutrition']['Total Fat']);
    var sat_fat = create_nutri_field(rec['nutrition']['Saturated fat']);
    var sugar = create_nutri_field(rec['nutrition']['Sugar']);
    var chol = create_nutri_field(rec['nutrition']['Cholesterol']);
    var carbs = create_nutri_field(rec['nutrition']['Carbohydrates']);
    var sod = create_nutri_field(rec['nutrition']['Sodium']);
    var servings = create_nutri_field(rec['nutrition']['Yield']);
    var pro = create_nutri_field(rec['nutrition']['Protein']);
    var fib = create_nutri_field(rec['nutrition']['Fiber']);


    return {
        contentType: "application/vnd.microsoft.card.adaptive",
        content: {
            type: "AdaptiveCard",
            "body": [{
                    "type": "TextBlock",
                    "text": "Nutrition Facts",
                    "size": "large",
                    "isSubtle": true
                },
                {
                    "type": "TextBlock",
                    "text": meal_name,
                    "weight": "bolder",
                    "wrap": true
                },
                {
                    "type": "ColumnSet",
                    "columns": [{
                        "type": "Column",
                        "width": "stretch",
                        "items": [{
                            "type": "FactSet",
                            "facts": [{
                                    "title": "Servings:",
                                    "value": servings
                                },
                                {
                                    "title": "Calories:",
                                    "value": calories
                                },
                                {
                                    "title": "Total Fat:",
                                    "value": total_fat
                                },
                                {
                                    "title": "Saturated Fat:",
                                    "value": sat_fat
                                },
                                {
                                    "title": "Carbohydrates:",
                                    "value": carbs
                                },
                                {
                                    "title": "Sodium:",
                                    "value": sod
                                },
                                {
                                    "title": "Cholesterol:",
                                    "value": chol
                                },
                                {
                                    "title": "Protein:",
                                    "value": pro
                                },
                                {
                                    "title": "Fiber:",
                                    "value": fib
                                }
                            ]
                        }]
                    }]
                }
            ]
        }
    };
}

function create_ingredients_card(rec, meal_name) {
    //console.log(rec);
    var protein = rec['protein'].toString();
    var veg = rec['veg'].toString();
    var carb = rec['carb'].toString();

    return {
        contentType: "application/vnd.microsoft.card.adaptive",
        content: {
            type: "AdaptiveCard",
            "body": [{
                    "type": "TextBlock",
                    "text": "Ingredients",
                    "size": "large",
                    "isSubtle": true
                }, {
                    "type": "TextBlock",
                    "text": meal_name,
                    "weight": "bolder",
                    "wrap": true
                },
                {
                    "type": "ColumnSet",
                    "columns": [{
                        "type": "Column",
                        "width": "stretch",
                        "items": [{
                            "type": "FactSet",
                            "facts": [{
                                    "title": "Proteins:",
                                    "value": protein
                                },
                                {
                                    "title": "Vegetables:",
                                    "value": veg
                                },
                                {
                                    "title": "Carbohydrates:",
                                    "value": carb
                                }
                            ]
                        }]
                    }]
                }
            ]
        }
    };
}

intents.matches('meal_plan-feedback', (session, args) => {
    logger.info({
        intent: 'meal_plan-feedback',
        messages: args.messages
    });
    send_messages(session, args.messages);

    var msg = new builder.Message(session)
        .attachmentLayout(builder.AttachmentLayout.carousel);
    var file_names = ['set_forget', 'variety', 'trial'];
    var text_names = ['Set and Forget', 'Variety', 'Trial'];

    for (var i = 0; i < file_names.length; i++) {
        msg.addAttachment(make_pref_card(session, file_names[i], text_names[i]));
    }
    session.send(msg);

});

intents.matches('meal_plan-budget', (session, args) => {
    logger.info({
        intent: 'meal_plan-budget',
        messages: args.messages
    });
    // TODO (if you want)
    // persist user budget choice

    // query db
    var query = create_query(session.userData.cart);
    var limit = parseInt(session.userData.cart.days, 10);
    var proteins = session.userData.cart.protein;

    async.waterfall([
        meal_rec_db.connect,
        function (db, callback) {
            return meal_rec_db.find_recipes(db, query, limit, callback);
        },
        function (docs, db, callback) {
            db.close();
            var sorted_docs = sort_recipes(docs, proteins);
            callback(null, sorted_docs);
        }
    ], (err, docs) => {
        assert.equal(null, err);
        logger.info({
            message: 'recieved ' + docs.length + ' recipes from db'
        });

        send_messages(session, args.messages.slice(0, 2));

        session.userData.cart.recipe_cart = docs;
        session.userData.cart.budget_op = args.parameters.budget;
        session.userData.cart.budget_val = args.parameters.number;


        var msg = new builder.Message(session)
            .attachmentLayout(builder.AttachmentLayout.carousel);

        docs.forEach((doc) => {
            if (doc) {
                msg.addAttachment(create_recipe_card(doc));
            }
        });

        session.send(msg);
        send_messages(session, args.messages.slice(2, args.messages.length));

    });
});

intents.matches('meal_plan-ingredients', (session, args) => {
    logger.info({
        intent: 'meal_plan-ingredients',
        message: args.messages
    });
    var meal_name = args.parameters.meal_name;

    async.waterfall([
        meal_rec_db.connect,
        function (db, callback) {
            return meal_rec_db.find_recipes(db, {
                name: meal_name
            }, 1, callback);
        }
    ], (err, docs) => {
        assert.equal(null, err);
        //console.log(docs[0]);
        var msg = new builder.Message(session)
            .addAttachment(create_ingredients_card(docs[0], meal_name));

        session.send(msg);
        send_messages(session, args.messages);
    });
});

intents.matches('meal_plan-calories', (session, args) => {
    logger.info({
        intent: 'meal_plan-calories',
        message: args.messages
    });
    var meal_name = args.parameters.meal_name;

    async.waterfall([
        meal_rec_db.connect,
        function (db, callback) {
            return meal_rec_db.find_recipes(db, {
                name: meal_name
            }, 1, callback);
        }
    ], (err, docs) => {

        var msg = new builder.Message(session)
            .addAttachment(create_nutri_card(docs[0], meal_name));

        session.send(msg);
        send_messages(session, args.messages);
    });
});

intents.matches('meal_plan-show-recipes', (session, args) => {
    logger.info({
        intent: 'meal_plan-show-recipes',
        message: args.messages
    });

    send_messages(session, args.messages.slice(0, 1));
    var docs = session.userData.cart.recipe_cart;
    var msg = new builder.Message(session)
        .attachmentLayout(builder.AttachmentLayout.carousel);

    docs.forEach((doc) => {
        if (doc) {
            msg.addAttachment(create_recipe_card(doc));
        }
    });

    session.send(msg);
    send_messages(session, args.messages.slice(1, args.messages.length));
});

intents.matches('meal_plan-replace', (session, args) => {
    logger.info({
        intent: 'meal_plan-replace',
        message: args.messages
    });
    var cart = session.userData.cart.recipe_cart;


    send_messages(session, args.messages.slice(0, 1));
    var meal_name = args.parameters.meal_name;
    var name_list = [];
    for (let rec of cart) {
        name_list.push(rec['name']);
    }

    var replace_idx = get_to_replace_index(meal_name, cart);
    session.userData.recipe_ban_list.push(cart[replace_idx]);

    for (let rec of session.userData.recipe_ban_list) {
        name_list.push(rec['name']);
    }

    var new_cart = cart.splice(replace_idx, 1);
    cart = new_cart;
    var query = create_query(session.userData.cart);
    query.name = {
        $not: {
            $in: name_list
        }
    };
    var limit = parseInt(session.userData.cart.days, 10);

    async.waterfall([
        meal_rec_db.connect,
        function (db, callback) {
            return meal_rec_db.find_recipes(db, query, 1, callback);
        }
    ], (err, docs) => {
        assert.equal(null, err);
        var doc = docs[0];
        if(doc){
            session.userData.cart.recipe_cart.push(doc);
        }
        logger.info({
            message: 'updating recommended recipes'
        });

        var msg = new builder.Message(session)
            .addAttachment(create_recipe_card(doc));

        session.send(msg);
        send_messages(session, args.messages.slice(1, args.messages.length));
    });
});

intents.matches('meal_plan-ongoing', (session, args) => {
    logger.info({
        intent: 'meal_plan-ongoing',
        messages: args.messages
    });

    var key = 'ongoing';
    var ongoing_val = parse_params(args.parameters, key);
    ongoing_val = ongoing_val.length > 0 ? ongoing_val[0] : '';

    add_param_to_user_cart(key, ongoing_val, session);

    var user = make_user(session.userData.cart, session.message.user.id);

    async.waterfall([
        meal_rec_db.connect,
        function (db, callback) {
            return meal_rec_db.insert_user(db, user, callback);
        }
    ], (err, result) => {
        assert.equal(null, err);
        var res = '';

        if (ongoing_val == 'set and forget') {
            res = 'Great, you’ve selected the Set and Forget Plan.We will send you weekly meal suggestions and you will have the opportunity to swap out any meals before your order is finalized.';
        } else if (ongoing_val == 'variety') {
            res = 'Great, you’ve selected the Variety plan. We will remind you to rate your meals and make any updates to your preferences every week before your next order.';
        } else if (ongoing_val == 'trial') {
            res = 'Great, you’ve selected the Tasty Trial plan. Enjoy the 1st week of meals and we will check back with you for feedback and to plan your next order.';
        } else {
            res = '';
        }

        var res_msg = [];
        res_msg.push(res);

        send_messages(session, res_msg);
        send_messages(session, [args.messages[0]]);

        var msg = new builder.Message(session)
            .addAttachment(create_user_card(user));

        session.send(msg);

        res = '';
        res_msg = [];
        if (parseInt(session.userData.cart.days, 10) === 7) {
            res = 'Here is your shopping cart for the meals you selected, www.cherylshoppingcart010818.com. And some good news. Based on those items, I have found coupons that will save you an additional $11.43 on your order in addition to the $25 full cart discount';
            res_msg.push(args.messages[1]);
            res_msg.push(res);
            res_msg.push(args.messages.slice(3, args.messages.length));
            send_messages(session, res_msg);
        } else {
            send_messages(session, args.messages.slice(1, args.messages.length));
        }
    });
});

function ls_eq(a, b) {
    return a <= b;
}

function gt_eq(a, b) {
    return a >= b;
}

intents.matches('meal_plan-budgeted-recipes', (session, args) => {
    logger.info({
        intent: 'meal_plan-budgeted-recipes',
        messages: args.messages
    });
    var cart = session.userData.cart;

    var docs = session.userData.cart.recipe_cart;
    var budgeted_docs = [];
    var budget_op = cart.budget_op;
    var budget_val = cart.budget_val;
    var op = null;

    switch (budget_op) {
        case 'under':
            op = ls_eq;
            break;
        case 'less than':
            op = ls_eq;
            break;
        case 'greater than':
            op = gt_eq;
            break;
        case 'more than':
            op = gt_eq;
            break;
        default:
            op = ls_eq;
            break;
    }

    docs.forEach((doc) => {
        if (op(doc['price'], budget_val)) {
            budgeted_docs.push(doc);
        }
    });

    var res = 'Each of the meals are within your budget of $%d. You can see the detailed prices later in your cart'

    send_messages(session, [util.format(res, parseInt(session.userData.cart.budget_val, 10))]);
    var msg = new builder.Message(session)
        .attachmentLayout(builder.AttachmentLayout.carousel);

    budgeted_docs.forEach((doc) => {
        if (doc) {
            msg.addAttachment(create_recipe_card(doc));
        }
    });

    session.send(msg);
    send_messages(session, args.messages);
});

intents.onDefault((session, args) => {
    send_messages(session, args.messages);
});

intents.matches('meal_plan-no', (session, args) => {
    logger.info({
        intent: 'meal_plan-no',
        messages: args.messages
    });
    send_messages(session, args.messages);
});

function send_messages(session, messages) {
    if (messages) {
        for (var i = 0; i < messages.length; i++) {
            var msg = new builder.Message(session)
                .text(messages[i])
                .speak(messages[i])
                .inputHint(builder.InputHint.acceptingInput);
            session.send(msg);
        }
    }
}

function add_param_to_user_cart(key, val, session) {
    session.userData.cart[key] = val;
}

function parse_params(params, key) {
    if (params.pick_list) {
        logger.info({
            message: 'returning pick list'
        });
        return params.pick_list;
    } else if (params[key]) {
        logger.info({
            message: 'returning from params'
        });
        return params[key];
    } else {
        logger.info({
            message: 'no pick list returned'
        });
        return [];
    }
}

function make_user(cart, id) {
    return {
        user_id: id,
        protein: cart.protein.toString(),
        veg: cart.veg.toString(),
        carbs: cart.carbs.toString(),
        prefs: cart.prefs.toString(),
        cuisine: cart.cuisine.toString(),
        diet_res: cart.diet_res.toString(),
        meal_size: cart.size.toString(),
        ongoing: cart.ongoing.toString()
    };
}

function lower_array(array) {
    var new_array = [];
    for (let elem of array) {
        new_array.push(elem.toLowerCase());
    }
    return new_array;
}

function create_query(cart) {
    var protein = lower_array(cart.protein);
    var veg = lower_array(cart.veg);
    var carb = lower_array(cart.carbs);

    return {
        protein: {
            $in: protein
        },
        veg: {
            $in: veg
        },
        numProtein: {
            $lt: 2
        }
    };
}

function check_protein(protein, protein_list) {
    for (let pro of protein_list) {
        if (pro == protein) {
            return true;
        }
    }
    return false;
}

function sort_recipes(docs, proteins) {
    var doc_length = docs.length;
    var pro_length = proteins.length;
    var sorted_docs = [];
    var rec_ban_set = new Set();
    var p_idx = 0;

    for (var i = 0; i < doc_length; i++) {
        var input_rec = null;
        if (p_idx === pro_length) {
            p_idx -= pro_length;
        }
        for (var j = 0; j < doc_length; j++) {
            if (!rec_ban_set.has(j)) {
                var cur_rec = docs[j];
                var cur_rec_protein = cur_rec.protein;
                if (check_protein(proteins[p_idx], cur_rec_protein)) {
                    input_rec = cur_rec;
                    rec_ban_set.add(j);
                    break;
                }
            }
        }
        p_idx += 1;
        if (input_rec) {
            sorted_docs.push(input_rec);
        }
    }

    if (rec_ban_set.size !== doc_length) {
        for (var i = 0; i < doc_length; i++) {
            if (!rec_ban_set.has(i)) {
                sorted_docs.push(docs[i]);
            }
        }
    }
    return sorted_docs;
}

function get_to_replace_index(meal_name, recipes) {
    for (var i = 0; i < recipes.length; i++) {
        if (recipes[i]['name'] === meal_name) {
            return i;
        }
    }
    return 0;
}