var entities  = [{
    "value": "Kombu Tea Grilled Chicken Thigh",
    "synonyms": ["Kombu Tea Grilled Chicken Thigh"]
},{
    "value": "Fennel-Rubbed Pork Tenderloin with Roasted Fennel Wedges",
    "synonyms": ["Fennel-Rubbed Pork Tenderloin with Roasted Fennel Wedges"]
},{
    "value": "Grandmommy's Mexicali Meatloaf",
    "synonyms": ["Grandmommy's Mexicali Meatloaf"]
},{
    "value": "Broccoli Rice Bake",
    "synonyms": ["Broccoli Rice Bake"]
},{
    "value": "Popcorn Trail Mix",
    "synonyms": ["Popcorn Trail Mix"]
},{
    "value": "Tropical Banana Muffins",
    "synonyms": ["Tropical Banana Muffins"]
},{
    "value": "Amy's Cheesey , Bacon savory bites .",
    "synonyms": ["Amy's Cheesey , Bacon savory bites ."]
},{
    "value": "Best Ever Fettuccine Alfredo",
    "synonyms": ["Best Ever Fettuccine Alfredo"]
},{
    "value": "Meringue Topping... You Don't Weep, It Doesn't Either!!",
    "synonyms": ["Meringue Topping... You Don't Weep, It Doesn't Either!!"]
},{
    "value": "Balsamic Chicken Pasta with Fresh Cheese",
    "synonyms": ["Balsamic Chicken Pasta with Fresh Cheese"]
},{
    "value": "Creme Curd Cups",
    "synonyms": ["Creme Curd Cups"]
},{
    "value": "Barbecue Baked Potatoes",
    "synonyms": ["Barbecue Baked Potatoes"]
},{
    "value": "Peach Scones",
    "synonyms": ["Peach Scones"]
},{
    "value": "Half-Time Sunshine Bars",
    "synonyms": ["Half-Time Sunshine Bars"]
},{
    "value": "Shalom Bayit Kugel Potato Kugel",
    "synonyms": ["Shalom Bayit Kugel Potato Kugel"]
},{
    "value": "Honey Sriracha Chicken Wings",
    "synonyms": ["Honey Sriracha Chicken Wings"]
},{
    "value": "cheese omelette",
    "synonyms": ["cheese omelette"]
},{
    "value": "Shrimp and Caper Salad",
    "synonyms": ["Shrimp and Caper Salad"]
},{
    "value": "Chicken Sausage Stir Fry",
    "synonyms": ["Chicken Sausage Stir Fry"]
},{
    "value": "Peanut Butter & Jelly Pie",
    "synonyms": ["Peanut Butter & Jelly Pie"]
},{
    "value": "Easy Cheese Sauce",
    "synonyms": ["Easy Cheese Sauce"]
},{
    "value": "Wake-Up Quesadilla",
    "synonyms": ["Wake-Up Quesadilla"]
},{
    "value": "Peanut Butter Glazed Brownie Doughnuts",
    "synonyms": ["Peanut Butter Glazed Brownie Doughnuts"]
},{
    "value": "Cornmeal Crackers with Pumpkin Seeds",
    "synonyms": ["Cornmeal Crackers with Pumpkin Seeds"]
},{
    "value": "Boiled Onions",
    "synonyms": ["Boiled Onions"]
},{
    "value": "Spicy Korean Temple Noodles",
    "synonyms": ["Spicy Korean Temple Noodles"]
},{
    "value": "Orange - Rosemary Chicken",
    "synonyms": ["Orange - Rosemary Chicken"]
},{
    "value": "Veal Chops with Rosemary Butter",
    "synonyms": ["Veal Chops with Rosemary Butter"]
},{
    "value": "Our Spaghetti Sauce",
    "synonyms": ["Our Spaghetti Sauce"]
},{
    "value": "Curly Q Noodle Chicken Soup",
    "synonyms": ["Curly Q Noodle Chicken Soup"]
},{
    "value": "Stuffed peppers",
    "synonyms": ["Stuffed peppers"]
},{
    "value": "Black Olive Spread",
    "synonyms": ["Black Olive Spread"]
},{
    "value": "Modenese Pork Chops",
    "synonyms": ["Modenese Pork Chops"]
},{
    "value": "Lemon Whippersnaps",
    "synonyms": ["Lemon Whippersnaps"]
},{
    "value": "Fresh Mozzarella & Tomato Salad With Balsamic Vinaigrette",
    "synonyms": ["Fresh Mozzarella & Tomato Salad With Balsamic Vinaigrette"]
},{
    "value": "Madame Wu's Spicy Noodles",
    "synonyms": ["Madame Wu's Spicy Noodles"]
},{
    "value": "Miso Chicken",
    "synonyms": ["Miso Chicken"]
},{
    "value": "Tuna Noodle Florentine",
    "synonyms": ["Tuna Noodle Florentine"]
},{
    "value": "Chilled Vegetable Pizza Appetizer",
    "synonyms": ["Chilled Vegetable Pizza Appetizer"]
},{
    "value": "The Cheesy Riri",
    "synonyms": ["The Cheesy Riri"]
},{
    "value": "One-Pot Chicken & Penne Parmesan",
    "synonyms": ["One-Pot Chicken & Penne Parmesan"]
},{
    "value": "P. F. Changs Sauce for Fried Green Beans Appetizer",
    "synonyms": ["P. F. Changs Sauce for Fried Green Beans Appetizer"]
},{
    "value": "Asiago Breaded Porkchops",
    "synonyms": ["Asiago Breaded Porkchops"]
},{
    "value": "Crab Cakes With Roasted Pepper Remoulade Sauce",
    "synonyms": ["Crab Cakes With Roasted Pepper Remoulade Sauce"]
},{
    "value": "Penne With Black Kale, Sausage, and Roasted Peppers",
    "synonyms": ["Penne With Black Kale, Sausage, and Roasted Peppers"]
},{
    "value": "Scallop Ceviche",
    "synonyms": ["Scallop Ceviche"]
},{
    "value": "Marco Canora's Braised Red Cabbage",
    "synonyms": ["Marco Canora's Braised Red Cabbage"]
},{
    "value": "Ham and Egg Salad",
    "synonyms": ["Ham and Egg Salad"]
},{
    "value": "Simple Peanut Butter and Tomato Sandwich",
    "synonyms": ["Simple Peanut Butter and Tomato Sandwich"]
},{
    "value": "Chicken Taco Bowl",
    "synonyms": ["Chicken Taco Bowl"]
},{
    "value": "Paella",
    "synonyms": ["Paella"]
},{
    "value": "Michelle's Turkey Burgers with Lemon Mayonnaise",
    "synonyms": ["Michelle's Turkey Burgers with Lemon Mayonnaise"]
},{
    "value": "Garlic and Dill Salmon",
    "synonyms": ["Garlic and Dill Salmon"]
},{
    "value": "Pineapple Marinade",
    "synonyms": ["Pineapple Marinade"]
},{
    "value": "Potato & Smoked Sausage Hash",
    "synonyms": ["Potato & Smoked Sausage Hash"]
},{
    "value": "Yellow Squash Saute",
    "synonyms": ["Yellow Squash Saute"]
},{
    "value": "The British Bulldog! Traditional Layered Beef Steak Suet Pudding",
    "synonyms": ["The British Bulldog! Traditional Layered Beef Steak Suet Pudding"]
},{
    "value": "Hootenanny Pancakes",
    "synonyms": ["Hootenanny Pancakes"]
},{
    "value": "Rosemary-Crumb Beef Tenderloin with Pancetta-Roasted Tomatoes",
    "synonyms": ["Rosemary-Crumb Beef Tenderloin with Pancetta-Roasted Tomatoes"]
},{
    "value": "Creamy Baked Chicken Breasts",
    "synonyms": ["Creamy Baked Chicken Breasts"]
},{
    "value": "Pumpkin Spice Trifle",
    "synonyms": ["Pumpkin Spice Trifle"]
},{
    "value": "Apple crunch",
    "synonyms": ["Apple crunch"]
},{
    "value": "Mini Baked Donuts",
    "synonyms": ["Mini Baked Donuts"]
},{
    "value": "Ww 7 Point Chicken Noodle Casserole",
    "synonyms": ["Ww 7 Point Chicken Noodle Casserole"]
},{
    "value": "Easy Clam Pasta",
    "synonyms": ["Easy Clam Pasta"]
},{
    "value": "Turkey Porcupine Meatballs",
    "synonyms": ["Turkey Porcupine Meatballs"]
},{
    "value": "Cornmeal Mush",
    "synonyms": ["Cornmeal Mush"]
},{
    "value": "Open-Face Portabella Sandwiches",
    "synonyms": ["Open-Face Portabella Sandwiches"]
},{
    "value": "Yummy Garlic Knots",
    "synonyms": ["Yummy Garlic Knots"]
},{
    "value": "Coffee Shop Style Easy Cake Mix Biscotti",
    "synonyms": ["Coffee Shop Style Easy Cake Mix Biscotti"]
},{
    "value": "BBq Steak Sandwiches With a Rainbow of Peppers",
    "synonyms": ["BBq Steak Sandwiches With a Rainbow of Peppers"]
},{
    "value": "Chestnut-Apple Soup With Calvados Cream",
    "synonyms": ["Chestnut-Apple Soup With Calvados Cream"]
},{
    "value": "Chicken Taco Dinner Salad",
    "synonyms": ["Chicken Taco Dinner Salad"]
},{
    "value": "Tortellini and Greens Bake",
    "synonyms": ["Tortellini and Greens Bake"]
},{
    "value": "Lady Bird Johnson's Spoon Bread",
    "synonyms": ["Lady Bird Johnson's Spoon Bread"]
},{
    "value": "Twice-Fried Chicken - Malaysia",
    "synonyms": ["Twice-Fried Chicken - Malaysia"]
},{
    "value": "Easy Shrimp Tostadas",
    "synonyms": ["Easy Shrimp Tostadas"]
},{
    "value": "Double Chocolate Cupcakes",
    "synonyms": ["Double Chocolate Cupcakes"]
},{
    "value": "Pumpkin Spice Pound Cake",
    "synonyms": ["Pumpkin Spice Pound Cake"]
},{
    "value": "Spicy Beef Chili With Apples",
    "synonyms": ["Spicy Beef Chili With Apples"]
},{
    "value": "Peanut Clusters",
    "synonyms": ["Peanut Clusters"]
},{
    "value": "Boozy Rice with Nuts and Berries",
    "synonyms": ["Boozy Rice with Nuts and Berries"]
},{
    "value": "Yumm Asparagus",
    "synonyms": ["Yumm Asparagus"]
},{
    "value": "Baked Cheese Sandwich",
    "synonyms": ["Baked Cheese Sandwich"]
},{
    "value": "Smoked Turkey Club Salad",
    "synonyms": ["Smoked Turkey Club Salad"]
},{
    "value": "Saucy Pasta All-In-One Pot",
    "synonyms": ["Saucy Pasta All-In-One Pot"]
},{
    "value": "Tony's Crabmeat Au Gratin",
    "synonyms": ["Tony's Crabmeat Au Gratin"]
},{
    "value": "Cheesy Sunshine Spinach Salad",
    "synonyms": ["Cheesy Sunshine Spinach Salad"]
},{
    "value": "Chocolate Chip Muffins II",
    "synonyms": ["Chocolate Chip Muffins II"]
},{
    "value": "Baked Kumera Sweet Potato With Sweet Smoked Paprika",
    "synonyms": ["Baked Kumera Sweet Potato With Sweet Smoked Paprika"]
},{
    "value": "Mommy's Fried chicken",
    "synonyms": ["Mommy's Fried chicken"]
},{
    "value": "Muffins Basic and Variations",
    "synonyms": ["Muffins Basic and Variations"]
},{
    "value": "Peanut Butter Cookie Dough Brownies",
    "synonyms": ["Peanut Butter Cookie Dough Brownies"]
},{
    "value": "Bair Family Macaroni Salad",
    "synonyms": ["Bair Family Macaroni Salad"]
},{
    "value": "You-\"CAN\"-Do-It FIG NEWTONS Peach Cobbler",
    "synonyms": ["You-\"CAN\"-Do-It FIG NEWTONS Peach Cobbler"]
},{
    "value": "Claudia's Zucchini Bread",
    "synonyms": ["Claudia's Zucchini Bread"]
},{
    "value": "Almost Companion Bread's Baked Eggs",
    "synonyms": ["Almost Companion Bread's Baked Eggs"]
},{
    "value": "Peanut Butter Fingers",
    "synonyms": ["Peanut Butter Fingers"]
},{
    "value": "Xampinyons En Salsa - Mushrooms in Sauce",
    "synonyms": ["Xampinyons En Salsa - Mushrooms in Sauce"]
},{
    "value": "Southwestern Burgers",
    "synonyms": ["Southwestern Burgers"]
},{
    "value": "Bamboo Shoot, Mushroom, and Long Bean Stir-Fry",
    "synonyms": ["Bamboo Shoot, Mushroom, and Long Bean Stir-Fry"]
},{
    "value": "Italian Chicken",
    "synonyms": ["Italian Chicken"]
},{
    "value": "Quick Chocolate Pudding",
    "synonyms": ["Quick Chocolate Pudding"]
},{
    "value": "Vegetarian Tortilla Soup",
    "synonyms": ["Vegetarian Tortilla Soup"]
},{
    "value": "Spinach & Lentil Lasagna",
    "synonyms": ["Spinach & Lentil Lasagna"]
},{
    "value": "Crock Pot Sausage and Beans",
    "synonyms": ["Crock Pot Sausage and Beans"]
},{
    "value": "Crowd-Pleasing Creamy Casserole",
    "synonyms": ["Crowd-Pleasing Creamy Casserole"]
},{
    "value": "Green Salad With Romaine Lettuce and Mangoes",
    "synonyms": ["Green Salad With Romaine Lettuce and Mangoes"]
},{
    "value": "Saarlandischer Dippelappes or Kartoffel Charles Po",
    "synonyms": ["Saarlandischer Dippelappes or Kartoffel Charles Po"]
},{
    "value": "Chicken Satay Aus style",
    "synonyms": ["Chicken Satay Aus style"]
},{
    "value": "Arugula and Pea Salad",
    "synonyms": ["Arugula and Pea Salad"]
},{
    "value": "Chilled Asparagus With Pecans",
    "synonyms": ["Chilled Asparagus With Pecans"]
},{
    "value": "Creamy Baked Ziti",
    "synonyms": ["Creamy Baked Ziti"]
},{
    "value": "White Chocolate Mole Sauce Mexican",
    "synonyms": ["White Chocolate Mole Sauce Mexican"]
},{
    "value": "Bacon, egg, cheese and avocado sandwich",
    "synonyms": ["Bacon, egg, cheese and avocado sandwich"]
},{
    "value": "Sweet-and-Spicy Chicken Curry",
    "synonyms": ["Sweet-and-Spicy Chicken Curry"]
},{
    "value": "Yummy Ribs",
    "synonyms": ["Yummy Ribs"]
},{
    "value": "Magnificent Meatballs",
    "synonyms": ["Magnificent Meatballs"]
},{
    "value": "March of the penguins, olive and cheese bites",
    "synonyms": ["March of the penguins, olive and cheese bites"]
},{
    "value": "Szechuan Style Twice Cooked Pork",
    "synonyms": ["Szechuan Style Twice Cooked Pork"]
},{
    "value": "Crock Pot Whole Baked Chicken",
    "synonyms": ["Crock Pot Whole Baked Chicken"]
},{
    "value": "Cherry Tomato Corn Salad",
    "synonyms": ["Cherry Tomato Corn Salad"]
},{
    "value": "Feta and Mint Mini Meatloaves",
    "synonyms": ["Feta and Mint Mini Meatloaves"]
},{
    "value": "Veal Rib Chops with Caramelized Fennel and Figs",
    "synonyms": ["Veal Rib Chops with Caramelized Fennel and Figs"]
},{
    "value": "Grilled Pineapple Salsa",
    "synonyms": ["Grilled Pineapple Salsa"]
},{
    "value": "Strawberry, Pistachio, and Goat Cheese Pizza",
    "synonyms": ["Strawberry, Pistachio, and Goat Cheese Pizza"]
},{
    "value": "Best Potatoes You'll Ever Taste",
    "synonyms": ["Best Potatoes You'll Ever Taste"]
},{
    "value": "Morning Glory Muffins",
    "synonyms": ["Morning Glory Muffins"]
},{
    "value": "Quick & Easy Chicken Parmigiana",
    "synonyms": ["Quick & Easy Chicken Parmigiana"]
},{
    "value": "Potato and Onion Cream",
    "synonyms": ["Potato and Onion Cream"]
},{
    "value": "Artichokes with Creamy Lemon Sauce",
    "synonyms": ["Artichokes with Creamy Lemon Sauce"]
},{
    "value": "Pasta Bake With Goats' Cheese",
    "synonyms": ["Pasta Bake With Goats' Cheese"]
},{
    "value": "SuperBowl Empanadas",
    "synonyms": ["SuperBowl Empanadas"]
},{
    "value": "Parmesan Couscous Veggie Casserole",
    "synonyms": ["Parmesan Couscous Veggie Casserole"]
},{
    "value": "Use a Pressure Cooker! Chunky Pot au Feu",
    "synonyms": ["Use a Pressure Cooker! Chunky Pot au Feu"]
},{
    "value": "Stacked and Loaded Potato Thins",
    "synonyms": ["Stacked and Loaded Potato Thins"]
},{
    "value": "Moist Meatloaf",
    "synonyms": ["Moist Meatloaf"]
},{
    "value": "Alfredo Di Roma Fettucine Alfredo",
    "synonyms": ["Alfredo Di Roma Fettucine Alfredo"]
},{
    "value": "Lemony Yogurt Custards with Cranberry-Apple Salad",
    "synonyms": ["Lemony Yogurt Custards with Cranberry-Apple Salad"]
},{
    "value": "Walnut Cream Cake",
    "synonyms": ["Walnut Cream Cake"]
},{
    "value": "Leek Tomato Dish",
    "synonyms": ["Leek Tomato Dish"]
},{
    "value": "Homemade Ricotta, Pine Nuts, Parmesan & Fresh Mint Ravioli",
    "synonyms": ["Homemade Ricotta, Pine Nuts, Parmesan & Fresh Mint Ravioli"]
},{
    "value": "Roast Salmon With Spiced Coconut Crumbs",
    "synonyms": ["Roast Salmon With Spiced Coconut Crumbs"]
},{
    "value": "Southwestern Corn and Red Pepper Chowder",
    "synonyms": ["Southwestern Corn and Red Pepper Chowder"]
},{
    "value": "Southwestern-style Stuffed Sweet Potatoes",
    "synonyms": ["Southwestern-style Stuffed Sweet Potatoes"]
},{
    "value": "Cowboy Meatloaf and Potato Casserole",
    "synonyms": ["Cowboy Meatloaf and Potato Casserole"]
},{
    "value": "Hearty Beef and Potato Stew",
    "synonyms": ["Hearty Beef and Potato Stew"]
},{
    "value": "Chicken Salsa Casserole",
    "synonyms": ["Chicken Salsa Casserole"]
},{
    "value": "Grilled Cauliflower Crust Pizza with Greek Yogurt Pesto",
    "synonyms": ["Grilled Cauliflower Crust Pizza with Greek Yogurt Pesto"]
},{
    "value": "Baked Salmon with Tomatoes, Spinach & Mushrooms",
    "synonyms": ["Baked Salmon with Tomatoes, Spinach & Mushrooms"]
},{
    "value": "Vegetable Pot Pies",
    "synonyms": ["Vegetable Pot Pies"]
},{
    "value": "Pizza Bread with Roasted Tomatoes",
    "synonyms": ["Pizza Bread with Roasted Tomatoes"]
},{
    "value": "Fried Potato Cake",
    "synonyms": ["Fried Potato Cake"]
},{
    "value": "Quick Pierogies Potato Filled Pasta Pockets Casserole",
    "synonyms": ["Quick Pierogies Potato Filled Pasta Pockets Casserole"]
},{
    "value": "Mexican Spiced Corn Packets",
    "synonyms": ["Mexican Spiced Corn Packets"]
},{
    "value": "Richie's Stuffed Mushrooms",
    "synonyms": ["Richie's Stuffed Mushrooms"]
},{
    "value": "Brussels Sprouts With Bacon and Apple",
    "synonyms": ["Brussels Sprouts With Bacon and Apple"]
},{
    "value": "Comforting Cheese Soup with Red Pepper and Bacon",
    "synonyms": ["Comforting Cheese Soup with Red Pepper and Bacon"]
},{
    "value": "Raviolo a Mano Hand Ravioli",
    "synonyms": ["Raviolo a Mano Hand Ravioli"]
},{
    "value": "Corn Cake Casserole",
    "synonyms": ["Corn Cake Casserole"]
},{
    "value": "Microwaved Kabocha Squash with Mayonnaise",
    "synonyms": ["Microwaved Kabocha Squash with Mayonnaise"]
},{
    "value": "Sig's Potato and Vegetable Casserole",
    "synonyms": ["Sig's Potato and Vegetable Casserole"]
},{
    "value": "Dorayaki Sweet Bean Pancakes",
    "synonyms": ["Dorayaki Sweet Bean Pancakes"]
},{
    "value": "Clam Chowder",
    "synonyms": ["Clam Chowder"]
},{
    "value": "Greek Pork Tenderloin Medallions",
    "synonyms": ["Greek Pork Tenderloin Medallions"]
},{
    "value": "Quick Mushroom-Pork Steaks",
    "synonyms": ["Quick Mushroom-Pork Steaks"]
},{
    "value": "Breakfast Ring",
    "synonyms": ["Breakfast Ring"]
},{
    "value": "Japanese Pizza",
    "synonyms": ["Japanese Pizza"]
},{
    "value": "Borscht Recipe",
    "synonyms": ["Borscht Recipe"]
},{
    "value": "Pizza Cups",
    "synonyms": ["Pizza Cups"]
},{
    "value": "Nasi Lemak Rice Cooked in Coconut Milk",
    "synonyms": ["Nasi Lemak Rice Cooked in Coconut Milk"]
},{
    "value": "The power omlette",
    "synonyms": ["The power omlette"]
},{
    "value": "Turkey Cutlets with Melon Salsa",
    "synonyms": ["Turkey Cutlets with Melon Salsa"]
},{
    "value": "Indonesian Nasi Kuning",
    "synonyms": ["Indonesian Nasi Kuning"]
},{
    "value": "Creamy Potato Soup",
    "synonyms": ["Creamy Potato Soup"]
},{
    "value": "Chewy Chocolate Candies",
    "synonyms": ["Chewy Chocolate Candies"]
},{
    "value": "Baked Kale Chips",
    "synonyms": ["Baked Kale Chips"]
},{
    "value": "Pumpkin Flan",
    "synonyms": ["Pumpkin Flan"]
},{
    "value": "Peanut Butter Chocolate Waffle Sandwich",
    "synonyms": ["Peanut Butter Chocolate Waffle Sandwich"]
},{
    "value": "Easy and Super Delicious Black Bean Soup",
    "synonyms": ["Easy and Super Delicious Black Bean Soup"]
},{
    "value": "Spinach and Cheese Stuffed Manicotti",
    "synonyms": ["Spinach and Cheese Stuffed Manicotti"]
},{
    "value": "Oatmeal Lace Cookies",
    "synonyms": ["Oatmeal Lace Cookies"]
},{
    "value": "Sweet & Sour Beef Bibim Noodles with Somen",
    "synonyms": ["Sweet & Sour Beef Bibim Noodles with Somen"]
},{
    "value": "Irresistible Peanut Butter Cookies",
    "synonyms": ["Irresistible Peanut Butter Cookies"]
},{
    "value": "Classic Lasagna",
    "synonyms": ["Classic Lasagna"]
},{
    "value": "Mussels With Capers and White Wine",
    "synonyms": ["Mussels With Capers and White Wine"]
},{
    "value": "Cucumber and Tomato Salad",
    "synonyms": ["Cucumber and Tomato Salad"]
},{
    "value": "Spanish Pot Roast",
    "synonyms": ["Spanish Pot Roast"]
},{
    "value": "Grilled Spring Onions with Pistachio Butter",
    "synonyms": ["Grilled Spring Onions with Pistachio Butter"]
},{
    "value": "Potato Cabbage Chowder",
    "synonyms": ["Potato Cabbage Chowder"]
},{
    "value": "ABC Snack Mix",
    "synonyms": ["ABC Snack Mix"]
},{
    "value": "Warm Steak and Blue Cheese Salad",
    "synonyms": ["Warm Steak and Blue Cheese Salad"]
},{
    "value": "Rum Raisin Bread Pudding with Warm Vanilla Sauce",
    "synonyms": ["Rum Raisin Bread Pudding with Warm Vanilla Sauce"]
},{
    "value": "Perfect Diner Pancakes",
    "synonyms": ["Perfect Diner Pancakes"]
},{
    "value": "Yellow Split Pea Soup",
    "synonyms": ["Yellow Split Pea Soup"]
},{
    "value": "Family-Favorite Macaroni and Cheese",
    "synonyms": ["Family-Favorite Macaroni and Cheese"]
},{
    "value": "Joanna's Salsa",
    "synonyms": ["Joanna's Salsa"]
},{
    "value": "Carrot Beetroot Sweet Balls",
    "synonyms": ["Carrot Beetroot Sweet Balls"]
},{
    "value": "Snow Peas With Fresh Mint",
    "synonyms": ["Snow Peas With Fresh Mint"]
},{
    "value": "Pasta with Creamy Red Roasted Pepper Sauce",
    "synonyms": ["Pasta with Creamy Red Roasted Pepper Sauce"]
},{
    "value": "Zucchini Cakes",
    "synonyms": ["Zucchini Cakes"]
},{
    "value": "Spinach, Mushroom, and Ricotta Fettuccine",
    "synonyms": ["Spinach, Mushroom, and Ricotta Fettuccine"]
},{
    "value": "Four-Cheese Skillet Burgers",
    "synonyms": ["Four-Cheese Skillet Burgers"]
},{
    "value": "Grilled Steak with Balsamic Madeira Reduction with Baked Ricotta Parmigiana Mousse, Grilled Eggplant and Tomato Sorbet",
    "synonyms": ["Grilled Steak with Balsamic Madeira Reduction with Baked Ricotta Parmigiana Mousse, Grilled Eggplant and Tomato Sorbet"]
},{
    "value": "Pork-and-Pineapple Fried Rice",
    "synonyms": ["Pork-and-Pineapple Fried Rice"]
},{
    "value": "Spicy Chorizo Tacos with Mushrooms and Avocado Crema",
    "synonyms": ["Spicy Chorizo Tacos with Mushrooms and Avocado Crema"]
},{
    "value": "Chunky Turkey Vegetable Chili Crock Pot",
    "synonyms": ["Chunky Turkey Vegetable Chili Crock Pot"]
},{
    "value": "Grilled Gorgonzola Flat Irons",
    "synonyms": ["Grilled Gorgonzola Flat Irons"]
},{
    "value": "Chicken Cacciatore",
    "synonyms": ["Chicken Cacciatore"]
},{
    "value": "Lemon and Roasted Garlic Mayo",
    "synonyms": ["Lemon and Roasted Garlic Mayo"]
},{
    "value": "Aunt Marie's Peas",
    "synonyms": ["Aunt Marie's Peas"]
},{
    "value": "White Chocolate Cherry Toffee Popcorn",
    "synonyms": ["White Chocolate Cherry Toffee Popcorn"]
},{
    "value": "Tofu Steak",
    "synonyms": ["Tofu Steak"]
},{
    "value": "Soy-Sesame Bok Choy Rolls",
    "synonyms": ["Soy-Sesame Bok Choy Rolls"]
},{
    "value": "Nostalgic...Ketchup Flavored Udon Noodles",
    "synonyms": ["Nostalgic...Ketchup Flavored Udon Noodles"]
},{
    "value": "Around the Kitchen Chili",
    "synonyms": ["Around the Kitchen Chili"]
},{
    "value": "Slow Cooked / Crock Pot Pulled Pork",
    "synonyms": ["Slow Cooked / Crock Pot Pulled Pork"]
},{
    "value": "Moms Sweet and Sour Short Ribs",
    "synonyms": ["Moms Sweet and Sour Short Ribs"]
},{
    "value": "Macaroni and Cheese",
    "synonyms": ["Macaroni and Cheese"]
},{
    "value": "Butternut Squash Soup with Toasted Sage and Herbed Croutons",
    "synonyms": ["Butternut Squash Soup with Toasted Sage and Herbed Croutons"]
},{
    "value": "Cashew Haystacks Candy",
    "synonyms": ["Cashew Haystacks Candy"]
},{
    "value": "Chicken Noodle Salad",
    "synonyms": ["Chicken Noodle Salad"]
},{
    "value": "Grilled Vegetable Pizza",
    "synonyms": ["Grilled Vegetable Pizza"]
},{
    "value": "Cheesy Chipotle Meatloaf",
    "synonyms": ["Cheesy Chipotle Meatloaf"]
},{
    "value": "Cabbage and Caramelized Onion Tart",
    "synonyms": ["Cabbage and Caramelized Onion Tart"]
},{
    "value": "Cornmeal-Crusted Fish with Green-Tomato Tartar Sauce",
    "synonyms": ["Cornmeal-Crusted Fish with Green-Tomato Tartar Sauce"]
},{
    "value": "Spiced Pear Hand Pies",
    "synonyms": ["Spiced Pear Hand Pies"]
},{
    "value": "Wardie's Meatloaf",
    "synonyms": ["Wardie's Meatloaf"]
},{
    "value": "Kree's Sweet and \"meaty\" Vegetarian Chili",
    "synonyms": ["Kree's Sweet and \"meaty\" Vegetarian Chili"]
},{
    "value": "Lighter Spicy Garlic Shrimp",
    "synonyms": ["Lighter Spicy Garlic Shrimp"]
},{
    "value": "Memphis Chopped Coleslaw",
    "synonyms": ["Memphis Chopped Coleslaw"]
},{
    "value": "Pumpkin and Tomato Soup",
    "synonyms": ["Pumpkin and Tomato Soup"]
},{
    "value": "All-Natural Chicken Pot Pie",
    "synonyms": ["All-Natural Chicken Pot Pie"]
},{
    "value": "Breakfast Potstickers With Avocado and Goat Cheese",
    "synonyms": ["Breakfast Potstickers With Avocado and Goat Cheese"]
},{
    "value": "Bloody Mary",
    "synonyms": ["Bloody Mary"]
},{
    "value": "Strawberry Custard Cake",
    "synonyms": ["Strawberry Custard Cake"]
},{
    "value": "Chicken Alexander",
    "synonyms": ["Chicken Alexander"]
},{
    "value": "Steamed Potatoes with Dill Butter",
    "synonyms": ["Steamed Potatoes with Dill Butter"]
},{
    "value": "Pumpkin Pie Paula Deen",
    "synonyms": ["Pumpkin Pie Paula Deen"]
},{
    "value": "Black Bean, Sweet Potato and Corn Stew",
    "synonyms": ["Black Bean, Sweet Potato and Corn Stew"]
},{
    "value": "Low Carb Seafood Cocktail Sauce",
    "synonyms": ["Low Carb Seafood Cocktail Sauce"]
},{
    "value": "Pasta with Spinach & Ricotta",
    "synonyms": ["Pasta with Spinach & Ricotta"]
},{
    "value": "Asian-Style Hamburgers",
    "synonyms": ["Asian-Style Hamburgers"]
},{
    "value": "Caldo Verde",
    "synonyms": ["Caldo Verde"]
},{
    "value": "Moroccan Chicken W/ Brown Rice",
    "synonyms": ["Moroccan Chicken W/ Brown Rice"]
},{
    "value": "BOCA Jambalaya",
    "synonyms": ["BOCA Jambalaya"]
},{
    "value": "Kimchi Flavoured Mayonnaise Dressing",
    "synonyms": ["Kimchi Flavoured Mayonnaise Dressing"]
},{
    "value": "Herb Pot Roast and Vegetables",
    "synonyms": ["Herb Pot Roast and Vegetables"]
},{
    "value": "Wild Rice Stuffing/Side Dish",
    "synonyms": ["Wild Rice Stuffing/Side Dish"]
},{
    "value": "Bananas Foster Raspberry French Toast",
    "synonyms": ["Bananas Foster Raspberry French Toast"]
},{
    "value": "Black-Eyed Peas Supper",
    "synonyms": ["Black-Eyed Peas Supper"]
},{
    "value": "Southwestern Tri-Tip Steaks",
    "synonyms": ["Southwestern Tri-Tip Steaks"]
},{
    "value": "Asparagus, Green Onion, Cucumber, and Herb Salad",
    "synonyms": ["Asparagus, Green Onion, Cucumber, and Herb Salad"]
},{
    "value": "Shrimp & Cheese Appetizers",
    "synonyms": ["Shrimp & Cheese Appetizers"]
},{
    "value": "Butternut Squash Swirled Butterscotch Blondies with Cinnamon Frosting",
    "synonyms": ["Butternut Squash Swirled Butterscotch Blondies with Cinnamon Frosting"]
},{
    "value": "Meatless Black Bean Tostadas",
    "synonyms": ["Meatless Black Bean Tostadas"]
},{
    "value": "Pretzel Cereal Snack",
    "synonyms": ["Pretzel Cereal Snack"]
},{
    "value": "Helene's Seductive Prime Rib",
    "synonyms": ["Helene's Seductive Prime Rib"]
},{
    "value": "Grilled Fish Over Linguine With Roasted Pepper Sauce",
    "synonyms": ["Grilled Fish Over Linguine With Roasted Pepper Sauce"]
},{
    "value": "Stir-fried Gnocchi",
    "synonyms": ["Stir-fried Gnocchi"]
},{
    "value": "Shrimp scampi & linguine",
    "synonyms": ["Shrimp scampi & linguine"]
},{
    "value": "Mini fried patties",
    "synonyms": ["Mini fried patties"]
},{
    "value": "Black Bean Hummus",
    "synonyms": ["Black Bean Hummus"]
},{
    "value": "Turkey Stew",
    "synonyms": ["Turkey Stew"]
},{
    "value": "Exotic Fried Rice",
    "synonyms": ["Exotic Fried Rice"]
},{
    "value": "Creamy Chicken Penne",
    "synonyms": ["Creamy Chicken Penne"]
},{
    "value": "Mini Leek Quiches",
    "synonyms": ["Mini Leek Quiches"]
},{
    "value": "Blue Cheese, Apple, Pecan, Cranberry & Tofu Salad",
    "synonyms": ["Blue Cheese, Apple, Pecan, Cranberry & Tofu Salad"]
},{
    "value": "Sausage and Veggie Skillet Supper",
    "synonyms": ["Sausage and Veggie Skillet Supper"]
},{
    "value": "Classic Spaghetti & Parmesan Meatballs",
    "synonyms": ["Classic Spaghetti & Parmesan Meatballs"]
},{
    "value": "Potato Encrusted Stuffed Tilapia",
    "synonyms": ["Potato Encrusted Stuffed Tilapia"]
},{
    "value": "Caribbean BBQ Ham",
    "synonyms": ["Caribbean BBQ Ham"]
},{
    "value": "Braised Beef with Caramelized Onions and Mushrooms",
    "synonyms": ["Braised Beef with Caramelized Onions and Mushrooms"]
},{
    "value": "Fried Chicken Livers & Hearts",
    "synonyms": ["Fried Chicken Livers & Hearts"]
},{
    "value": "Low Fat Spicy Crab Dip",
    "synonyms": ["Low Fat Spicy Crab Dip"]
},{
    "value": "Barbacoa",
    "synonyms": ["Barbacoa"]
},{
    "value": "Sauteed kale with pancetta",
    "synonyms": ["Sauteed kale with pancetta"]
},{
    "value": "Fried Tofu Wrapped in Flour Batter TAHU GORENG TEPUNG",
    "synonyms": ["Fried Tofu Wrapped in Flour Batter TAHU GORENG TEPUNG"]
},{
    "value": "Cod Cooked in Foil",
    "synonyms": ["Cod Cooked in Foil"]
},{
    "value": "Easy Baked Taco Pie",
    "synonyms": ["Easy Baked Taco Pie"]
},{
    "value": "Taco Dog",
    "synonyms": ["Taco Dog"]
},{
    "value": "Baked Ziti",
    "synonyms": ["Baked Ziti"]
},{
    "value": "Easy Vegetarian Chili",
    "synonyms": ["Easy Vegetarian Chili"]
},{
    "value": "Shrimp Fettucinni Alfredo With Mushrooms and Tomato",
    "synonyms": ["Shrimp Fettucinni Alfredo With Mushrooms and Tomato"]
},{
    "value": "Bread Pudding with Whiskey Sauce",
    "synonyms": ["Bread Pudding with Whiskey Sauce"]
},{
    "value": "Marinated Jamaican Chicken",
    "synonyms": ["Marinated Jamaican Chicken"]
},{
    "value": "Grilled Mahimahi with Pineapple Sambal",
    "synonyms": ["Grilled Mahimahi with Pineapple Sambal"]
},{
    "value": "Baked Roast Beef and Brie Sliders with Caramelized Onions",
    "synonyms": ["Baked Roast Beef and Brie Sliders with Caramelized Onions"]
},{
    "value": "Mini-Mac Sliders",
    "synonyms": ["Mini-Mac Sliders"]
},{
    "value": "Five-Spice Tofu Stir-Fry with Carrots and Celery",
    "synonyms": ["Five-Spice Tofu Stir-Fry with Carrots and Celery"]
},{
    "value": "Restaurant Flavor Porcini Risotto",
    "synonyms": ["Restaurant Flavor Porcini Risotto"]
},{
    "value": "Cuban Midnight Sandwich",
    "synonyms": ["Cuban Midnight Sandwich"]
},{
    "value": "Zucchini Chocolate Cake",
    "synonyms": ["Zucchini Chocolate Cake"]
},{
    "value": "My Herb Roasted Vegetables",
    "synonyms": ["My Herb Roasted Vegetables"]
},{
    "value": "Chicken Hatch Chile Verde Skillet Macaroni and Cheese",
    "synonyms": ["Chicken Hatch Chile Verde Skillet Macaroni and Cheese"]
},{
    "value": "Lechon Kawali Crispy Pan-Fried Roasted Pork",
    "synonyms": ["Lechon Kawali Crispy Pan-Fried Roasted Pork"]
},{
    "value": "Korean spicy seafood pot",
    "synonyms": ["Korean spicy seafood pot"]
},{
    "value": "Fresh Tomato, Basil and Parmesan Pasta",
    "synonyms": ["Fresh Tomato, Basil and Parmesan Pasta"]
},{
    "value": "Stuffed Spaghetti Squash",
    "synonyms": ["Stuffed Spaghetti Squash"]
},{
    "value": "Cecylia Roznowska's Potato Pancakes Stuffed with Bacon, Mushrooms, and Onion",
    "synonyms": ["Cecylia Roznowska's Potato Pancakes Stuffed with Bacon, Mushrooms, and Onion"]
},{
    "value": "Peanut Butter Chocolate Chip Cupcakes",
    "synonyms": ["Peanut Butter Chocolate Chip Cupcakes"]
},{
    "value": "Pumpkin Cookie Butter Doodles",
    "synonyms": ["Pumpkin Cookie Butter Doodles"]
},{
    "value": "Lemon Dill Salmon",
    "synonyms": ["Lemon Dill Salmon"]
},{
    "value": "Winter Apple Salad",
    "synonyms": ["Winter Apple Salad"]
},{
    "value": "White Vegetable Lasagna",
    "synonyms": ["White Vegetable Lasagna"]
},{
    "value": "Budget friendly creamy chicken with pasta crockpot",
    "synonyms": ["Budget friendly creamy chicken with pasta crockpot"]
},{
    "value": "My Skinny Pumpkin Pie Crustless, Low Fat, Low Sugar",
    "synonyms": ["My Skinny Pumpkin Pie Crustless, Low Fat, Low Sugar"]
},{
    "value": "Chicken Tostadas",
    "synonyms": ["Chicken Tostadas"]
},{
    "value": "Roasted Butternut Squash and Red Onions",
    "synonyms": ["Roasted Butternut Squash and Red Onions"]
},{
    "value": "Crab Ragoon",
    "synonyms": ["Crab Ragoon"]
},{
    "value": "Bourbon and Molasses-Glazed Turkey Breast",
    "synonyms": ["Bourbon and Molasses-Glazed Turkey Breast"]
},{
    "value": "Ducuna",
    "synonyms": ["Ducuna"]
},{
    "value": "Divine Spaghetti Sauce",
    "synonyms": ["Divine Spaghetti Sauce"]
},{
    "value": "Pizza Omelet",
    "synonyms": ["Pizza Omelet"]
},{
    "value": "Easy Roasted Turkey Legs",
    "synonyms": ["Easy Roasted Turkey Legs"]
},{
    "value": "Shimajiro Rice with an egg-free version",
    "synonyms": ["Shimajiro Rice with an egg-free version"]
},{
    "value": "Traditional Roast Stuffed Turkey",
    "synonyms": ["Traditional Roast Stuffed Turkey"]
},{
    "value": "Grilled Orange Sesame Chicken and Vegetables",
    "synonyms": ["Grilled Orange Sesame Chicken and Vegetables"]
},{
    "value": "Vegetable Rolls",
    "synonyms": ["Vegetable Rolls"]
},{
    "value": "Fisherman's Chowder",
    "synonyms": ["Fisherman's Chowder"]
},{
    "value": "Acadian Beef Casserole",
    "synonyms": ["Acadian Beef Casserole"]
},{
    "value": "Salmon BLT with Spicy Garlic Mayo",
    "synonyms": ["Salmon BLT with Spicy Garlic Mayo"]
},{
    "value": "Grilled Steak Tacos with Herb Sauce",
    "synonyms": ["Grilled Steak Tacos with Herb Sauce"]
},{
    "value": "Croatian Fish gregada",
    "synonyms": ["Croatian Fish gregada"]
},{
    "value": "Tuna Salad, Deli Style",
    "synonyms": ["Tuna Salad, Deli Style"]
},{
    "value": "Simple and Delicious Tartare Sauce in the Microwave",
    "synonyms": ["Simple and Delicious Tartare Sauce in the Microwave"]
},{
    "value": "Stuffed peppers",
    "synonyms": ["Stuffed peppers"]
},{
    "value": "Roasted Sweet Potatoes With Cinnamon Pecan Crunch",
    "synonyms": ["Roasted Sweet Potatoes With Cinnamon Pecan Crunch"]
},{
    "value": "Fancy Shrimp Cocktail",
    "synonyms": ["Fancy Shrimp Cocktail"]
},{
    "value": "Souped-up Meatloaf",
    "synonyms": ["Souped-up Meatloaf"]
},{
    "value": "Green Soup",
    "synonyms": ["Green Soup"]
},{
    "value": "Honey Glazed Scallops",
    "synonyms": ["Honey Glazed Scallops"]
},{
    "value": "Fish Cushions With Papaya-Raisin-Dip",
    "synonyms": ["Fish Cushions With Papaya-Raisin-Dip"]
},{
    "value": "Seeduction Bread",
    "synonyms": ["Seeduction Bread"]
},{
    "value": "Crab \"soup Dumplings\" Dim Sum",
    "synonyms": ["Crab \"soup Dumplings\" Dim Sum"]
},{
    "value": "Thai Chicken-Coconut Soup",
    "synonyms": ["Thai Chicken-Coconut Soup"]
},{
    "value": "Chipotle-Orange Shrimp",
    "synonyms": ["Chipotle-Orange Shrimp"]
},{
    "value": "Pumpkin and Shrimp Bisque",
    "synonyms": ["Pumpkin and Shrimp Bisque"]
},{
    "value": "Green Beans With Sweet and Sour Red Onions",
    "synonyms": ["Green Beans With Sweet and Sour Red Onions"]
},{
    "value": "Pumpkin Spice Ganache",
    "synonyms": ["Pumpkin Spice Ganache"]
},{
    "value": "Chargrilled BBQ Burgers",
    "synonyms": ["Chargrilled BBQ Burgers"]
},{
    "value": "Spiced Chicken & Couscous",
    "synonyms": ["Spiced Chicken & Couscous"]
},{
    "value": "Green Beans With Pine Nuts",
    "synonyms": ["Green Beans With Pine Nuts"]
},{
    "value": "Cold Soba Noodles W/Vietnamese Pork",
    "synonyms": ["Cold Soba Noodles W/Vietnamese Pork"]
},{
    "value": "Caramel Pumpkin Flan",
    "synonyms": ["Caramel Pumpkin Flan"]
},{
    "value": "Maple & Mustard Glazed Salmon 3 Ingredients",
    "synonyms": ["Maple & Mustard Glazed Salmon 3 Ingredients"]
},{
    "value": "Pizza Tofu",
    "synonyms": ["Pizza Tofu"]
},{
    "value": "Crackling Rice Paper-Wrapped Fish",
    "synonyms": ["Crackling Rice Paper-Wrapped Fish"]
},{
    "value": "Salmon & Ikura Salmon Roe Temari Sushi",
    "synonyms": ["Salmon & Ikura Salmon Roe Temari Sushi"]
},{
    "value": "Chinese-style Napa Cabbage Rolls",
    "synonyms": ["Chinese-style Napa Cabbage Rolls"]
},{
    "value": "Sausage Paella",
    "synonyms": ["Sausage Paella"]
},{
    "value": "Sloppy Joes Ww and Crock-Pot",
    "synonyms": ["Sloppy Joes Ww and Crock-Pot"]
},{
    "value": "New Orleans Stew with Smoked Andouille Chicken Sausage",
    "synonyms": ["New Orleans Stew with Smoked Andouille Chicken Sausage"]
},{
    "value": "Lentil Loaf",
    "synonyms": ["Lentil Loaf"]
},{
    "value": "Crock Pot Rigatoni",
    "synonyms": ["Crock Pot Rigatoni"]
},{
    "value": "Aimees Famous Baked Nachos",
    "synonyms": ["Aimees Famous Baked Nachos"]
},{
    "value": "Roasted Asparagus Prosciutto and Egg",
    "synonyms": ["Roasted Asparagus Prosciutto and Egg"]
},{
    "value": "Green Lemonade",
    "synonyms": ["Green Lemonade"]
},{
    "value": "Mustard Potato Salad",
    "synonyms": ["Mustard Potato Salad"]
},{
    "value": "Creamy Greek-Style Caprese Salad",
    "synonyms": ["Creamy Greek-Style Caprese Salad"]
},{
    "value": "Super Food Slaw",
    "synonyms": ["Super Food Slaw"]
},{
    "value": "Gyro-Style Burgers",
    "synonyms": ["Gyro-Style Burgers"]
},{
    "value": "Pickled Garden Vegetables",
    "synonyms": ["Pickled Garden Vegetables"]
},{
    "value": "Cuban Potato Salad",
    "synonyms": ["Cuban Potato Salad"]
},{
    "value": "Cheesy Potato, Sausage & Kale Soup",
    "synonyms": ["Cheesy Potato, Sausage & Kale Soup"]
},{
    "value": "No-Knead Swedish Cardamom Braid",
    "synonyms": ["No-Knead Swedish Cardamom Braid"]
},{
    "value": "Superb Cabbage Rolls Simmered in Consomme",
    "synonyms": ["Superb Cabbage Rolls Simmered in Consomme"]
},{
    "value": "Egg & Spinach Open-Faced Sandwich",
    "synonyms": ["Egg & Spinach Open-Faced Sandwich"]
},{
    "value": "Spaghetti With Asparagus, Smoked Mozzarella and Prosciutto",
    "synonyms": ["Spaghetti With Asparagus, Smoked Mozzarella and Prosciutto"]
},{
    "value": "Sushi Rolls",
    "synonyms": ["Sushi Rolls"]
},{
    "value": "Asparagus Cashew Stir-Fry",
    "synonyms": ["Asparagus Cashew Stir-Fry"]
},{
    "value": "My Mexican Meatloaf",
    "synonyms": ["My Mexican Meatloaf"]
},{
    "value": "Shrimp tacos with red cabbage lime slaw",
    "synonyms": ["Shrimp tacos with red cabbage lime slaw"]
},{
    "value": "Amazing Baked Ziti",
    "synonyms": ["Amazing Baked Ziti"]
},{
    "value": "Salmon & Chinese Cabbage Gratin Soup",
    "synonyms": ["Salmon & Chinese Cabbage Gratin Soup"]
},{
    "value": "Mustard-Crusted Tofu with Kale and Sweet Potato",
    "synonyms": ["Mustard-Crusted Tofu with Kale and Sweet Potato"]
},{
    "value": "Pepper Beef",
    "synonyms": ["Pepper Beef"]
},{
    "value": "Asparagus and Spinach Salad",
    "synonyms": ["Asparagus and Spinach Salad"]
},{
    "value": "Chicken Florentine Artichoke Bake",
    "synonyms": ["Chicken Florentine Artichoke Bake"]
},{
    "value": "Mediterranean Pasta Salad",
    "synonyms": ["Mediterranean Pasta Salad"]
},{
    "value": "Lemon Asparagus Risotto",
    "synonyms": ["Lemon Asparagus Risotto"]
},{
    "value": "Baked Sambal Fish",
    "synonyms": ["Baked Sambal Fish"]
},{
    "value": "Easy Fish Stew",
    "synonyms": ["Easy Fish Stew"]
},{
    "value": "Shrimp with Roasted Garlic Alfredo Sauce",
    "synonyms": ["Shrimp with Roasted Garlic Alfredo Sauce"]
},{
    "value": "Russian Borsh/Borsch",
    "synonyms": ["Russian Borsh/Borsch"]
},{
    "value": "Cheesy Chicken Asparagus Casserole",
    "synonyms": ["Cheesy Chicken Asparagus Casserole"]
},{
    "value": "Classic Baked Spaghetti",
    "synonyms": ["Classic Baked Spaghetti"]
},{
    "value": "Gruyere Tuna Melt Recipe",
    "synonyms": ["Gruyere Tuna Melt Recipe"]
},{
    "value": "Noodles and Stir Fried Shrimp Medley",
    "synonyms": ["Noodles and Stir Fried Shrimp Medley"]
},{
    "value": "Deli Coleslaw",
    "synonyms": ["Deli Coleslaw"]
},{
    "value": "Rice With Caramelized Shallots from Melissa D'arabian",
    "synonyms": ["Rice With Caramelized Shallots from Melissa D'arabian"]
},{
    "value": "Raisin-Studded Apple Bread Pudding",
    "synonyms": ["Raisin-Studded Apple Bread Pudding"]
},{
    "value": "Linguine with Brussels Sprouts, Bacon, and Caramelized Shallots",
    "synonyms": ["Linguine with Brussels Sprouts, Bacon, and Caramelized Shallots"]
},{
    "value": "Creamy Rice Grits with Tomato Relish",
    "synonyms": ["Creamy Rice Grits with Tomato Relish"]
},{
    "value": "Sig's Tofu and Mushroom Pasta",
    "synonyms": ["Sig's Tofu and Mushroom Pasta"]
},{
    "value": "Fresh Tuna Steaks on a Bed of Couscous",
    "synonyms": ["Fresh Tuna Steaks on a Bed of Couscous"]
},{
    "value": "Phat Thai Pad Thai",
    "synonyms": ["Phat Thai Pad Thai"]
},{
    "value": "Polenta Lasagna with Creamy Mushroom Sauce",
    "synonyms": ["Polenta Lasagna with Creamy Mushroom Sauce"]
},{
    "value": "Tuscan Bean Soup with Prosciutto and Grated Parmigiano-Reggiano",
    "synonyms": ["Tuscan Bean Soup with Prosciutto and Grated Parmigiano-Reggiano"]
},{
    "value": "Wild Mushroom Whole Wheat Stuffing with Tarragon",
    "synonyms": ["Wild Mushroom Whole Wheat Stuffing with Tarragon"]
},{
    "value": "Tofu With Tomatoes and Coriander",
    "synonyms": ["Tofu With Tomatoes and Coriander"]
},{
    "value": "Ina Garten's Baked Sweet Potato Fries",
    "synonyms": ["Ina Garten's Baked Sweet Potato Fries"]
},{
    "value": "Asparagus-Mushroom Bread Pudding",
    "synonyms": ["Asparagus-Mushroom Bread Pudding"]
},{
    "value": "Cheesy Beef-Corn Chip Skillet",
    "synonyms": ["Cheesy Beef-Corn Chip Skillet"]
},{
    "value": "Berry Bread Pudding with Brown Sugar Sauce",
    "synonyms": ["Berry Bread Pudding with Brown Sugar Sauce"]
},{
    "value": "Turkish Cacik",
    "synonyms": ["Turkish Cacik"]
},{
    "value": "Cheesy Vegetable Chowder",
    "synonyms": ["Cheesy Vegetable Chowder"]
},{
    "value": "Broccoli Salad",
    "synonyms": ["Broccoli Salad"]
},{
    "value": "Simple Cream of Veggie Soup",
    "synonyms": ["Simple Cream of Veggie Soup"]
},{
    "value": "Cheesefish",
    "synonyms": ["Cheesefish"]
},{
    "value": "Two-Bean Toss",
    "synonyms": ["Two-Bean Toss"]
},{
    "value": "Healthy French Onion Dip with Leeks and Shallots",
    "synonyms": ["Healthy French Onion Dip with Leeks and Shallots"]
},{
    "value": "I Lost My Noodles! Low Carb/South Beach Eggplant Lasagna",
    "synonyms": ["I Lost My Noodles! Low Carb/South Beach Eggplant Lasagna"]
},{
    "value": "Butternut Squash Ravioli in Brown Butter and Sage",
    "synonyms": ["Butternut Squash Ravioli in Brown Butter and Sage"]
},{
    "value": "Savory Bread Pudding",
    "synonyms": ["Savory Bread Pudding"]
},{
    "value": "Vegetable Lasagna With White Sauce",
    "synonyms": ["Vegetable Lasagna With White Sauce"]
},{
    "value": "Hot Tofu in Spinach Soup",
    "synonyms": ["Hot Tofu in Spinach Soup"]
},{
    "value": "Barbecue Lamb on Mediterranean Salad",
    "synonyms": ["Barbecue Lamb on Mediterranean Salad"]
},{
    "value": "Hearty Soup in a Jar",
    "synonyms": ["Hearty Soup in a Jar"]
},{
    "value": "HEALTHY LIVING Cheesy Chicken Pot Pie",
    "synonyms": ["HEALTHY LIVING Cheesy Chicken Pot Pie"]
},{
    "value": "Zucchini Hash Browns",
    "synonyms": ["Zucchini Hash Browns"]
},{
    "value": "Cabbage and Potatoes with Garlic Butter",
    "synonyms": ["Cabbage and Potatoes with Garlic Butter"]
},{
    "value": "A Great Plate",
    "synonyms": ["A Great Plate"]
},{
    "value": "Thanksgiving Asparagus Casserole",
    "synonyms": ["Thanksgiving Asparagus Casserole"]
},{
    "value": "Tuna Enchilada Casserole",
    "synonyms": ["Tuna Enchilada Casserole"]
},{
    "value": "Turkey & Swiss Sandwich",
    "synonyms": ["Turkey & Swiss Sandwich"]
},{
    "value": "Broccoli Pizza Pie",
    "synonyms": ["Broccoli Pizza Pie"]
},{
    "value": "Grilled Veggie Quesadillas",
    "synonyms": ["Grilled Veggie Quesadillas"]
},{
    "value": "Zucchini Cakes",
    "synonyms": ["Zucchini Cakes"]
},{
    "value": "Zucchini With Salsa",
    "synonyms": ["Zucchini With Salsa"]
},{
    "value": "Butternut Squash Soup Recipe",
    "synonyms": ["Butternut Squash Soup Recipe"]
},{
    "value": "Gluten-Free Banana-Blueberry Waffles Recipe",
    "synonyms": ["Gluten-Free Banana-Blueberry Waffles Recipe"]
},{
    "value": "Ligurian Seafood Stew",
    "synonyms": ["Ligurian Seafood Stew"]
},{
    "value": "Balsamic-and-Black-Pepper Pumpkin Seeds",
    "synonyms": ["Balsamic-and-Black-Pepper Pumpkin Seeds"]
},{
    "value": "Asparagus, Ricotta, and Egg Pizza",
    "synonyms": ["Asparagus, Ricotta, and Egg Pizza"]
},{
    "value": "Spicy Mabo Tofu",
    "synonyms": ["Spicy Mabo Tofu"]
},{
    "value": "Johnny Jalapeno's 'cancun Vacation' Citrus Shrimp Medly",
    "synonyms": ["Johnny Jalapeno's 'cancun Vacation' Citrus Shrimp Medly"]
},{
    "value": "Greens With Warm Turkey Bacon Dressing Ww",
    "synonyms": ["Greens With Warm Turkey Bacon Dressing Ww"]
},{
    "value": "Cranberry Pesto Swirled Pumpkin Bread",
    "synonyms": ["Cranberry Pesto Swirled Pumpkin Bread"]
},{
    "value": "Braised Lamb Shanks with Ginger and Five-Spice",
    "synonyms": ["Braised Lamb Shanks with Ginger and Five-Spice"]
},{
    "value": "Creamy Tomato Tortellini",
    "synonyms": ["Creamy Tomato Tortellini"]
},{
    "value": "Family Favorite Potato Salad! German Kartoffelsalat",
    "synonyms": ["Family Favorite Potato Salad! German Kartoffelsalat"]
},{
    "value": "Newman's Own Sesame Ginger Shrimp Salad",
    "synonyms": ["Newman's Own Sesame Ginger Shrimp Salad"]
},{
    "value": "Braised Sage Chicken with Kale & Chickpeas",
    "synonyms": ["Braised Sage Chicken with Kale & Chickpeas"]
},{
    "value": "Apple Pumpkin Oatmeal Raisin Cookies",
    "synonyms": ["Apple Pumpkin Oatmeal Raisin Cookies"]
},{
    "value": "Pumpkin Patch Cake",
    "synonyms": ["Pumpkin Patch Cake"]
},{
    "value": "Curried Shrimp Au Gratin",
    "synonyms": ["Curried Shrimp Au Gratin"]
},{
    "value": "Chinese Broccoli With Ginger Sauce",
    "synonyms": ["Chinese Broccoli With Ginger Sauce"]
},{
    "value": "Basil Chile Chicken Stir-Fry",
    "synonyms": ["Basil Chile Chicken Stir-Fry"]
},{
    "value": "Poached Oysters and Artichokes with Champagne Cream",
    "synonyms": ["Poached Oysters and Artichokes with Champagne Cream"]
},{
    "value": "Butternut Squash and Chorizo Tortilla",
    "synonyms": ["Butternut Squash and Chorizo Tortilla"]
},{
    "value": "Miso-Glazed Burdock with Red Lettuces",
    "synonyms": ["Miso-Glazed Burdock with Red Lettuces"]
},{
    "value": "Roasted Sweet Potato & Garlic Soup",
    "synonyms": ["Roasted Sweet Potato & Garlic Soup"]
},{
    "value": "Cajun Jambalaya With Catfish, Scallops and Shrimp",
    "synonyms": ["Cajun Jambalaya With Catfish, Scallops and Shrimp"]
},{
    "value": "Taco Sandwich",
    "synonyms": ["Taco Sandwich"]
},{
    "value": "Chicken Nacho Bake Made Over",
    "synonyms": ["Chicken Nacho Bake Made Over"]
},{
    "value": "Pumpkin Carrot Pudding Kugel",
    "synonyms": ["Pumpkin Carrot Pudding Kugel"]
},{
    "value": "Mike's Portuguese Tuna Rice Casserole",
    "synonyms": ["Mike's Portuguese Tuna Rice Casserole"]
},{
    "value": "Chicken with Black Beans & Ham",
    "synonyms": ["Chicken with Black Beans & Ham"]
},{
    "value": "White Bean and Kale Stuffed Peppers",
    "synonyms": ["White Bean and Kale Stuffed Peppers"]
},{
    "value": "Nicoise Potato Salad",
    "synonyms": ["Nicoise Potato Salad"]
},{
    "value": "Buffalo Chicken Wraps",
    "synonyms": ["Buffalo Chicken Wraps"]
},{
    "value": "Creamy Cabbage with Apples and Bacon",
    "synonyms": ["Creamy Cabbage with Apples and Bacon"]
},{
    "value": "Marinated Cucumbers",
    "synonyms": ["Marinated Cucumbers"]
},{
    "value": "Korean Bibimbap",
    "synonyms": ["Korean Bibimbap"]
},{
    "value": "Asparagi Alla Parmigiana Italian Asparagus Gratin",
    "synonyms": ["Asparagi Alla Parmigiana Italian Asparagus Gratin"]
},{
    "value": "Sweet Potato Biscuits",
    "synonyms": ["Sweet Potato Biscuits"]
},{
    "value": "'V' Fruity red cabbage",
    "synonyms": ["'V' Fruity red cabbage"]
},{
    "value": "Montokers",
    "synonyms": ["Montokers"]
},{
    "value": "Spiced Pumpkin Butter Spread",
    "synonyms": ["Spiced Pumpkin Butter Spread"]
},{
    "value": "Calico Squash Casserole",
    "synonyms": ["Calico Squash Casserole"]
},{
    "value": "Mushroom and Spinach Quesadilla With Garden Salad",
    "synonyms": ["Mushroom and Spinach Quesadilla With Garden Salad"]
},{
    "value": "The Perfect Basic Tabbouleh",
    "synonyms": ["The Perfect Basic Tabbouleh"]
},{
    "value": "Cranberry 'N Cheese Grill",
    "synonyms": ["Cranberry 'N Cheese Grill"]
},{
    "value": "Green Beans with Bacon and Shallots",
    "synonyms": ["Green Beans with Bacon and Shallots"]
},{
    "value": "Butternut Squash and Apple Soup",
    "synonyms": ["Butternut Squash and Apple Soup"]
},{
    "value": "Yummy Mexican Lasagna W/ a Healthier Kick!",
    "synonyms": ["Yummy Mexican Lasagna W/ a Healthier Kick!"]
},{
    "value": "Honey-Ginger Shrimp and Vegetables",
    "synonyms": ["Honey-Ginger Shrimp and Vegetables"]
},{
    "value": "Shrimp Stir-Fry",
    "synonyms": ["Shrimp Stir-Fry"]
},{
    "value": "Halloween Jack-o'-Lantern Curry",
    "synonyms": ["Halloween Jack-o'-Lantern Curry"]
},{
    "value": "Egyptian Red Lentil Soup",
    "synonyms": ["Egyptian Red Lentil Soup"]
},{
    "value": "Silken Tofu Topped with Umeboshi",
    "synonyms": ["Silken Tofu Topped with Umeboshi"]
},{
    "value": "Dal Makhani Indian Lentils",
    "synonyms": ["Dal Makhani Indian Lentils"]
},{
    "value": "The Real Deal: Osaka-Style Okonomiyaki",
    "synonyms": ["The Real Deal: Osaka-Style Okonomiyaki"]
},{
    "value": "Quinoa Spinach Power Salad with Lemon Vinaigrette",
    "synonyms": ["Quinoa Spinach Power Salad with Lemon Vinaigrette"]
},{
    "value": "The Best Summer Time Tofu Scramble - Vegan",
    "synonyms": ["The Best Summer Time Tofu Scramble - Vegan"]
},{
    "value": "Asian Cashew Curry",
    "synonyms": ["Asian Cashew Curry"]
},{
    "value": "Chocolate Zucchini Muffins",
    "synonyms": ["Chocolate Zucchini Muffins"]
},{
    "value": "Mexicali Casserole",
    "synonyms": ["Mexicali Casserole"]
},{
    "value": "Skinny Three-Cheese Pumpkin Pasta",
    "synonyms": ["Skinny Three-Cheese Pumpkin Pasta"]
},{
    "value": "Hobo's",
    "synonyms": ["Hobo's"]
},{
    "value": "Bread-Maker Multi Grain Bread - Soft",
    "synonyms": ["Bread-Maker Multi Grain Bread - Soft"]
},{
    "value": "Jimmy Brown's Shrimp Cocktail",
    "synonyms": ["Jimmy Brown's Shrimp Cocktail"]
},{
    "value": "Sweet Potato Bread",
    "synonyms": ["Sweet Potato Bread"]
},{
    "value": "Homemade Sauerkraut with Caraway and Apples",
    "synonyms": ["Homemade Sauerkraut with Caraway and Apples"]
},{
    "value": "Hearty Caesar Salad",
    "synonyms": ["Hearty Caesar Salad"]
},{
    "value": "Tabouleh",
    "synonyms": ["Tabouleh"]
},{
    "value": "Easy and Quick Cauliflower Apple Salad",
    "synonyms": ["Easy and Quick Cauliflower Apple Salad"]
},{
    "value": "Shepherd's Pie - All the Taste, Lower Fat!",
    "synonyms": ["Shepherd's Pie - All the Taste, Lower Fat!"]
},{
    "value": "Kale Hummus",
    "synonyms": ["Kale Hummus"]
},{
    "value": "Simple roast turkey",
    "synonyms": ["Simple roast turkey"]
},{
    "value": "Artichoke Chicken",
    "synonyms": ["Artichoke Chicken"]
},{
    "value": "California Rice Salad",
    "synonyms": ["California Rice Salad"]
},{
    "value": "Toasted Millet Salad Recipe",
    "synonyms": ["Toasted Millet Salad Recipe"]
},{
    "value": "Savory Pecan Roast",
    "synonyms": ["Savory Pecan Roast"]
},{
    "value": "Wheat Pasta With Sauteed Beet Greens and Tomatoes",
    "synonyms": ["Wheat Pasta With Sauteed Beet Greens and Tomatoes"]
},{
    "value": "Lamb and Sweet Potato Shepherd's Pies",
    "synonyms": ["Lamb and Sweet Potato Shepherd's Pies"]
},{
    "value": "Curried Red Kidney Beans and Cauliflower Rajma Masala",
    "synonyms": ["Curried Red Kidney Beans and Cauliflower Rajma Masala"]
},{
    "value": "Slow Cooker Pumpkin Salsa Shredded Beef",
    "synonyms": ["Slow Cooker Pumpkin Salsa Shredded Beef"]
},{
    "value": "Chicken BLT Sandwich",
    "synonyms": ["Chicken BLT Sandwich"]
},{
    "value": "Dixie Cafe Baked Squash",
    "synonyms": ["Dixie Cafe Baked Squash"]
},{
    "value": "Maryland Pumpkin Seeds",
    "synonyms": ["Maryland Pumpkin Seeds"]
},{
    "value": "Abm Italian Breadsticks- Grissini",
    "synonyms": ["Abm Italian Breadsticks- Grissini"]
},{
    "value": "Serbian Pork and Beef Casserole",
    "synonyms": ["Serbian Pork and Beef Casserole"]
},{
    "value": "Chickpea Salad With Cumin and Lemon",
    "synonyms": ["Chickpea Salad With Cumin and Lemon"]
},{
    "value": "CHOW Veggie Burger Recipe",
    "synonyms": ["CHOW Veggie Burger Recipe"]
},{
    "value": "Kale Banana Smoothie",
    "synonyms": ["Kale Banana Smoothie"]
},{
    "value": "Easy One Pot Soup with Kabocha Squash",
    "synonyms": ["Easy One Pot Soup with Kabocha Squash"]
},{
    "value": "Spicy Cabbage Salad",
    "synonyms": ["Spicy Cabbage Salad"]
},{
    "value": "Caribbean Curried Peas Lentils",
    "synonyms": ["Caribbean Curried Peas Lentils"]
},{
    "value": "Slow Cooker Italian Turkey",
    "synonyms": ["Slow Cooker Italian Turkey"]
},{
    "value": "Thai Pumpkin Soup",
    "synonyms": ["Thai Pumpkin Soup"]
},{
    "value": "Sweet Potato Gnudi with Sage Butter",
    "synonyms": ["Sweet Potato Gnudi with Sage Butter"]
},{
    "value": "Sausage-and-Zucchini Skillet Lasagna",
    "synonyms": ["Sausage-and-Zucchini Skillet Lasagna"]
},{
    "value": "Hot and Sour Chicken and Cabbage Soup",
    "synonyms": ["Hot and Sour Chicken and Cabbage Soup"]
},{
    "value": "Kale and Chard Supreme",
    "synonyms": ["Kale and Chard Supreme"]
},{
    "value": "Veggie Lo Mein",
    "synonyms": ["Veggie Lo Mein"]
},{
    "value": "Steamed Snapper with Mustard Greens",
    "synonyms": ["Steamed Snapper with Mustard Greens"]
},{
    "value": "Hearty Corn Cauliflower and Potato Chowder",
    "synonyms": ["Hearty Corn Cauliflower and Potato Chowder"]
},{
    "value": "Bacon & Brussel sprouts",
    "synonyms": ["Bacon & Brussel sprouts"]
},{
    "value": "Asparagus Frittata",
    "synonyms": ["Asparagus Frittata"]
},{
    "value": "Stuffed Chicken Breast with White Wine Sauce",
    "synonyms": ["Stuffed Chicken Breast with White Wine Sauce"]
},{
    "value": "Easy Pepper Basil Spaghetti",
    "synonyms": ["Easy Pepper Basil Spaghetti"]
},{
    "value": "Sausage and Potatoes",
    "synonyms": ["Sausage and Potatoes"]
},{
    "value": "Chicken Broccoli Cheese Soup",
    "synonyms": ["Chicken Broccoli Cheese Soup"]
},{
    "value": "Baby Girl's Fried Rice",
    "synonyms": ["Baby Girl's Fried Rice"]
},{
    "value": "Lemon Cucumber Dressing",
    "synonyms": ["Lemon Cucumber Dressing"]
},{
    "value": "Shakshuka Egg White Frittata with Turkey Sausage",
    "synonyms": ["Shakshuka Egg White Frittata with Turkey Sausage"]
},{
    "value": "Greek Garbanzo Bean Salad",
    "synonyms": ["Greek Garbanzo Bean Salad"]
},{
    "value": "Grilled Chicken Breasts in Raspberry Vinegar Marinade",
    "synonyms": ["Grilled Chicken Breasts in Raspberry Vinegar Marinade"]
},{
    "value": "Hoisin Chicken Lettuce Wraps",
    "synonyms": ["Hoisin Chicken Lettuce Wraps"]
},{
    "value": "Oatflakes and Courgette Soup",
    "synonyms": ["Oatflakes and Courgette Soup"]
},{
    "value": "Sweet Potato-Apple Gratin",
    "synonyms": ["Sweet Potato-Apple Gratin"]
},{
    "value": "Spicy Corn Chowder",
    "synonyms": ["Spicy Corn Chowder"]
},{
    "value": "Jamaican Jerk Potato Salad",
    "synonyms": ["Jamaican Jerk Potato Salad"]
},{
    "value": "Seaside Garden Sandwich",
    "synonyms": ["Seaside Garden Sandwich"]
},{
    "value": "Meatball Stroganoff",
    "synonyms": ["Meatball Stroganoff"]
},{
    "value": "Dark Chocolate Greek Yogurt",
    "synonyms": ["Dark Chocolate Greek Yogurt"]
},{
    "value": "Mediterranean Pasta Salad",
    "synonyms": ["Mediterranean Pasta Salad"]
},{
    "value": "Cheeseburger Casserole",
    "synonyms": ["Cheeseburger Casserole"]
},{
    "value": "Ranch Parm Asparagus",
    "synonyms": ["Ranch Parm Asparagus"]
},{
    "value": "Hot Artichoke Dip",
    "synonyms": ["Hot Artichoke Dip"]
},{
    "value": "Meatloaf Cupcakes",
    "synonyms": ["Meatloaf Cupcakes"]
},{
    "value": "Lemon-Tarragon Crab Cakes",
    "synonyms": ["Lemon-Tarragon Crab Cakes"]
},{
    "value": "Rilakkuma Tri-Color Charaben",
    "synonyms": ["Rilakkuma Tri-Color Charaben"]
},{
    "value": "skye's spicy 3 bean chili",
    "synonyms": ["skye's spicy 3 bean chili"]
},{
    "value": "Acorn Squash Ice Cream",
    "synonyms": ["Acorn Squash Ice Cream"]
},{
    "value": "Grilled Parmesan Vegetables",
    "synonyms": ["Grilled Parmesan Vegetables"]
},{
    "value": "California Veggie Burger",
    "synonyms": ["California Veggie Burger"]
},{
    "value": "Herb Roast Chicken",
    "synonyms": ["Herb Roast Chicken"]
},{
    "value": "Quinoa with Moroccan Winter Squash and Carrot Stew",
    "synonyms": ["Quinoa with Moroccan Winter Squash and Carrot Stew"]
},{
    "value": "Oven Roasted Breakfast Potatoes",
    "synonyms": ["Oven Roasted Breakfast Potatoes"]
},{
    "value": "Flavorful Rice",
    "synonyms": ["Flavorful Rice"]
},{
    "value": "Low-Fat Hot Artichoke and Spinach Dip",
    "synonyms": ["Low-Fat Hot Artichoke and Spinach Dip"]
},{
    "value": "Shortcut Japanese Snack ~Sakura Mochi-Style Rice Balls",
    "synonyms": ["Shortcut Japanese Snack ~Sakura Mochi-Style Rice Balls"]
},{
    "value": "Kale Pasta Salad",
    "synonyms": ["Kale Pasta Salad"]
},{
    "value": "Mashed Butternut Squash",
    "synonyms": ["Mashed Butternut Squash"]
},{
    "value": "Sausage Stuffing with Fennel and Roasted Squash",
    "synonyms": ["Sausage Stuffing with Fennel and Roasted Squash"]
},{
    "value": "Shanghai Asparagus",
    "synonyms": ["Shanghai Asparagus"]
},{
    "value": "The \"Perfect\" Pasta Primavera",
    "synonyms": ["The \"Perfect\" Pasta Primavera"]
},{
    "value": "Flora's Bacon and Blue Burger",
    "synonyms": ["Flora's Bacon and Blue Burger"]
},{
    "value": "Ramen Broccoli Stir Fry",
    "synonyms": ["Ramen Broccoli Stir Fry"]
},{
    "value": "Pineapple Fried Rice/ Vegetarian",
    "synonyms": ["Pineapple Fried Rice/ Vegetarian"]
},{
    "value": "Amy's Delicious Turkey Burgers",
    "synonyms": ["Amy's Delicious Turkey Burgers"]
},{
    "value": "Asian Beef Stir-Fry",
    "synonyms": ["Asian Beef Stir-Fry"]
},{
    "value": "Broccoli Cauliflower Salad",
    "synonyms": ["Broccoli Cauliflower Salad"]
},{
    "value": "STOVE TOP Easy Turkey Bake",
    "synonyms": ["STOVE TOP Easy Turkey Bake"]
},{
    "value": "Lemon Chicken and Artichokes",
    "synonyms": ["Lemon Chicken and Artichokes"]
},{
    "value": "Marinated Skirt Steak with Pan-Roasted Vegetables",
    "synonyms": ["Marinated Skirt Steak with Pan-Roasted Vegetables"]
},{
    "value": "A More Healthy Vegetable Medley",
    "synonyms": ["A More Healthy Vegetable Medley"]
},{
    "value": "Perfect Pumpkin Seeds",
    "synonyms": ["Perfect Pumpkin Seeds"]
},{
    "value": "Sweet & Sour Chicken Curry Hot, Sour & Sweet!",
    "synonyms": ["Sweet & Sour Chicken Curry Hot, Sour & Sweet!"]
},{
    "value": "Thanksgiving's Leftovers: Pinwheel",
    "synonyms": ["Thanksgiving's Leftovers: Pinwheel"]
},{
    "value": "Spicy Sausage Broccoli Rabe Parmesan",
    "synonyms": ["Spicy Sausage Broccoli Rabe Parmesan"]
},{
    "value": "Pumpkin Pie V",
    "synonyms": ["Pumpkin Pie V"]
},{
    "value": "Broccoli Salad III",
    "synonyms": ["Broccoli Salad III"]
},{
    "value": "Mediterranean Lemon Chicken",
    "synonyms": ["Mediterranean Lemon Chicken"]
},{
    "value": "Broccoli-Walnut Salad",
    "synonyms": ["Broccoli-Walnut Salad"]
},{
    "value": "Vegetable No Cheese Lasagna",
    "synonyms": ["Vegetable No Cheese Lasagna"]
},{
    "value": "Calabazas y Camotes al Horno",
    "synonyms": ["Calabazas y Camotes al Horno"]
},{
    "value": "Feta and Mint Rice",
    "synonyms": ["Feta and Mint Rice"]
},{
    "value": "Ragu Southern Chicken, Sausage and Shrimp Stew #Ragu",
    "synonyms": ["Ragu Southern Chicken, Sausage and Shrimp Stew #Ragu"]
},{
    "value": "Steak Diane",
    "synonyms": ["Steak Diane"]
},{
    "value": "15 Minute Taco In A Pan",
    "synonyms": ["15 Minute Taco In A Pan"]
},{
    "value": "Cauliflower and Crab Ravioli",
    "synonyms": ["Cauliflower and Crab Ravioli"]
},{
    "value": "Oh Boy, Salad!",
    "synonyms": ["Oh Boy, Salad!"]
},{
    "value": "German Style Green Kale Gruenkohl",
    "synonyms": ["German Style Green Kale Gruenkohl"]
},{
    "value": "Broccoli Salad I",
    "synonyms": ["Broccoli Salad I"]
},{
    "value": "Ham And Cheese Casserole",
    "synonyms": ["Ham And Cheese Casserole"]
},{
    "value": "Easy! Tasty! Sweet Potato Chips",
    "synonyms": ["Easy! Tasty! Sweet Potato Chips"]
},{
    "value": "How to Cook Spoil-Resistant Bento Rice",
    "synonyms": ["How to Cook Spoil-Resistant Bento Rice"]
},{
    "value": "Vegetable Barley Salad",
    "synonyms": ["Vegetable Barley Salad"]
},{
    "value": "The Best Cucumber Salad!!",
    "synonyms": ["The Best Cucumber Salad!!"]
},{
    "value": "Cheesy Artichoke Dip Recipe",
    "synonyms": ["Cheesy Artichoke Dip Recipe"]
},{
    "value": "Green Bean Bundles",
    "synonyms": ["Green Bean Bundles"]
},{
    "value": "Salad of Warm White Asparagus Spargel, Baby Arugula and Beefst",
    "synonyms": ["Salad of Warm White Asparagus Spargel, Baby Arugula and Beefst"]
},{
    "value": "Sweet Potato Enchiladas in Creamy Chipotle Sauce",
    "synonyms": ["Sweet Potato Enchiladas in Creamy Chipotle Sauce"]
},{
    "value": "Asparagus Parmesan Pita Rounds",
    "synonyms": ["Asparagus Parmesan Pita Rounds"]
},{
    "value": "Pecan-Parmesan Fish Fillets",
    "synonyms": ["Pecan-Parmesan Fish Fillets"]
},{
    "value": "Hearty Tuscan Soup Zuppa Toscana",
    "synonyms": ["Hearty Tuscan Soup Zuppa Toscana"]
},{
    "value": "Lentil and Green Bean Salad",
    "synonyms": ["Lentil and Green Bean Salad"]
},{
    "value": "Pizza Potato Skins",
    "synonyms": ["Pizza Potato Skins"]
},{
    "value": "Healthy Spicy Tofu, Spinach and Beans Soup",
    "synonyms": ["Healthy Spicy Tofu, Spinach and Beans Soup"]
},{
    "value": "Grilled Cornish Hens with Coconut Curry Sauce",
    "synonyms": ["Grilled Cornish Hens with Coconut Curry Sauce"]
},{
    "value": "Asparagus, Pickled Beets & Blue Cheese Salad",
    "synonyms": ["Asparagus, Pickled Beets & Blue Cheese Salad"]
},{
    "value": "Salmon and Asparagus Pasta - Clean Eating",
    "synonyms": ["Salmon and Asparagus Pasta - Clean Eating"]
},{
    "value": "Chips With Smoked Salmon and Avocado Salsa",
    "synonyms": ["Chips With Smoked Salmon and Avocado Salsa"]
},{
    "value": "Yummy No-Bake Cinnamon Rolls for Kids",
    "synonyms": ["Yummy No-Bake Cinnamon Rolls for Kids"]
},{
    "value": "Butternut Squash and Sweet Potato Soup",
    "synonyms": ["Butternut Squash and Sweet Potato Soup"]
},{
    "value": "Acorn Squash with Apple Stuffing",
    "synonyms": ["Acorn Squash with Apple Stuffing"]
},{
    "value": "Thai Coconut Curry Soup",
    "synonyms": ["Thai Coconut Curry Soup"]
},{
    "value": "Healthy Meatloaf",
    "synonyms": ["Healthy Meatloaf"]
},{
    "value": "Old South Lost Bread",
    "synonyms": ["Old South Lost Bread"]
},{
    "value": "Sausage, Kale, and White Bean Soup",
    "synonyms": ["Sausage, Kale, and White Bean Soup"]
},{
    "value": "Apple butter and cinnamon custard toasts recipe",
    "synonyms": ["Apple butter and cinnamon custard toasts recipe"]
},{
    "value": "Kinako Mochi - Tofu Version",
    "synonyms": ["Kinako Mochi - Tofu Version"]
},{
    "value": "Parmesan Potato Wedges",
    "synonyms": ["Parmesan Potato Wedges"]
},{
    "value": "Red Lentil, Miso and Shiitake Mushroom Soup",
    "synonyms": ["Red Lentil, Miso and Shiitake Mushroom Soup"]
},{
    "value": "Cranberry-Glazed Sweet Potatoes",
    "synonyms": ["Cranberry-Glazed Sweet Potatoes"]
},{
    "value": "Grilled Sweet Potato Wedges with Spicy Adobo Greek Yogurt Sauce",
    "synonyms": ["Grilled Sweet Potato Wedges with Spicy Adobo Greek Yogurt Sauce"]
},{
    "value": "Saskatchewan City Steak Soup",
    "synonyms": ["Saskatchewan City Steak Soup"]
},{
    "value": "Asparagus, Ham, and Fontina Bread Puddings",
    "synonyms": ["Asparagus, Ham, and Fontina Bread Puddings"]
},{
    "value": "Spanish Chicken & Rice Bake",
    "synonyms": ["Spanish Chicken & Rice Bake"]
},{
    "value": "Chicken Soup with Kale",
    "synonyms": ["Chicken Soup with Kale"]
},{
    "value": "Thanksgiving Leftover Pot Pie",
    "synonyms": ["Thanksgiving Leftover Pot Pie"]
},{
    "value": "Spinach Artichoke Feta Ball",
    "synonyms": ["Spinach Artichoke Feta Ball"]
},{
    "value": "Southern Candied Sweet Potatoes",
    "synonyms": ["Southern Candied Sweet Potatoes"]
},{
    "value": "Crumbly Fried Rice in a Rice Cooker",
    "synonyms": ["Crumbly Fried Rice in a Rice Cooker"]
},{
    "value": "St. Paul Sandwich",
    "synonyms": ["St. Paul Sandwich"]
},{
    "value": "Acropolis Salad",
    "synonyms": ["Acropolis Salad"]
},{
    "value": "Veggie Frittata",
    "synonyms": ["Veggie Frittata"]
},{
    "value": "Warm Garbanzo Bean and Kale Salad with Smoked Paprika and Parsley Vinaigrette",
    "synonyms": ["Warm Garbanzo Bean and Kale Salad with Smoked Paprika and Parsley Vinaigrette"]
},{
    "value": "Garlicky Cauliflower, Kale and Avocado Puree with Caramelized Mushrooms",
    "synonyms": ["Garlicky Cauliflower, Kale and Avocado Puree with Caramelized Mushrooms"]
},{
    "value": "Cucumber and Pineapple Relish",
    "synonyms": ["Cucumber and Pineapple Relish"]
},{
    "value": "Baked Loaded Mashed Potatoes",
    "synonyms": ["Baked Loaded Mashed Potatoes"]
},{
    "value": "Lentil Noodle Soup With Greens",
    "synonyms": ["Lentil Noodle Soup With Greens"]
},{
    "value": "Natto & Tofu Doria Rice Gratin",
    "synonyms": ["Natto & Tofu Doria Rice Gratin"]
},{
    "value": "Thyme-Garlic Roasted Asparagus",
    "synonyms": ["Thyme-Garlic Roasted Asparagus"]
},{
    "value": "Mayan Pumpkin-Seed Dip",
    "synonyms": ["Mayan Pumpkin-Seed Dip"]
},{
    "value": "Halibut Braised in Red Wine",
    "synonyms": ["Halibut Braised in Red Wine"]
},{
    "value": "Artichoke Dip with Crispy Shallots",
    "synonyms": ["Artichoke Dip with Crispy Shallots"]
},{
    "value": "Sausage Lentil Soup",
    "synonyms": ["Sausage Lentil Soup"]
},{
    "value": "Stuffed Breakfast Sweet Potato",
    "synonyms": ["Stuffed Breakfast Sweet Potato"]
},{
    "value": "Sherried Lentil Soup With Sausage and Croutons",
    "synonyms": ["Sherried Lentil Soup With Sausage and Croutons"]
},{
    "value": "Skillet Lasagna",
    "synonyms": ["Skillet Lasagna"]
},{
    "value": "Fennel And Preserved Duck",
    "synonyms": ["Fennel And Preserved Duck"]
},{
    "value": "Mamas Sweet Potato Pudding",
    "synonyms": ["Mamas Sweet Potato Pudding"]
},{
    "value": "Low Fat Pasta Salad",
    "synonyms": ["Low Fat Pasta Salad"]
},{
    "value": "Mediterranean Risotto With Other Influences!",
    "synonyms": ["Mediterranean Risotto With Other Influences!"]
},{
    "value": "Indian Dahl with Spinach",
    "synonyms": ["Indian Dahl with Spinach"]
},{
    "value": "Tofu Cheesecake",
    "synonyms": ["Tofu Cheesecake"]
},{
    "value": "Smokey Parmesan Sweet Potato Chips",
    "synonyms": ["Smokey Parmesan Sweet Potato Chips"]
},{
    "value": "Mashed Potatoes with Ranch Dressing",
    "synonyms": ["Mashed Potatoes with Ranch Dressing"]
},{
    "value": "Tortellini-Asparagus Salad",
    "synonyms": ["Tortellini-Asparagus Salad"]
},{
    "value": "Split Pea and Lentil Soup With Vegetables",
    "synonyms": ["Split Pea and Lentil Soup With Vegetables"]
},{
    "value": "Lemon Pepper Shrimp Scampi With Sauteed Asparagus",
    "synonyms": ["Lemon Pepper Shrimp Scampi With Sauteed Asparagus"]
},{
    "value": "Easy Cheesy Lentil & Rice Casserole",
    "synonyms": ["Easy Cheesy Lentil & Rice Casserole"]
},{
    "value": "Tennessee Whiskey Sweet Potato Casserole",
    "synonyms": ["Tennessee Whiskey Sweet Potato Casserole"]
},{
    "value": "Curried Red Lentil Soup",
    "synonyms": ["Curried Red Lentil Soup"]
},{
    "value": "Hipquest's Baked Potatoes",
    "synonyms": ["Hipquest's Baked Potatoes"]
},{
    "value": "Roasted Cauliflower and Grapes",
    "synonyms": ["Roasted Cauliflower and Grapes"]
},{
    "value": "Cauliflower Patties",
    "synonyms": ["Cauliflower Patties"]
},{
    "value": "Vegan Italian-Style Wild Rice and Vegetables",
    "synonyms": ["Vegan Italian-Style Wild Rice and Vegetables"]
},{
    "value": "Holiday Green Beans",
    "synonyms": ["Holiday Green Beans"]
},{
    "value": "Kale Caesar with Parmesan Crusted Shrimp, Avocado, and Pickled Shallots",
    "synonyms": ["Kale Caesar with Parmesan Crusted Shrimp, Avocado, and Pickled Shallots"]
},{
    "value": "Dutch Slavinken With Milk Gravy Ground Pork Cylinders",
    "synonyms": ["Dutch Slavinken With Milk Gravy Ground Pork Cylinders"]
},{
    "value": "Vegan Red Potato Salad from Whole Foods Cookbook",
    "synonyms": ["Vegan Red Potato Salad from Whole Foods Cookbook"]
},{
    "value": "Coco-Banana's Thai Green Vegetable Curry",
    "synonyms": ["Coco-Banana's Thai Green Vegetable Curry"]
},{
    "value": "Chicken, Veggie, Noodle, and Dumpling Soup With Flavor",
    "synonyms": ["Chicken, Veggie, Noodle, and Dumpling Soup With Flavor"]
},{
    "value": "Simple Venison Stew",
    "synonyms": ["Simple Venison Stew"]
},{
    "value": "Smoked Pistachio Sambal",
    "synonyms": ["Smoked Pistachio Sambal"]
},{
    "value": "Yellow Dhal - Sweet Potato Soup",
    "synonyms": ["Yellow Dhal - Sweet Potato Soup"]
},{
    "value": "Blood Orange Polenta Upside Down-Cake with Whipped Creme Fraiche",
    "synonyms": ["Blood Orange Polenta Upside Down-Cake with Whipped Creme Fraiche"]
},{
    "value": "Easy Mushroom Rice Pilaf",
    "synonyms": ["Easy Mushroom Rice Pilaf"]
},{
    "value": "Rigatoni ai Carciofi",
    "synonyms": ["Rigatoni ai Carciofi"]
},{
    "value": "Grilled Cedar-Planked Salmon",
    "synonyms": ["Grilled Cedar-Planked Salmon"]
},{
    "value": "Hearty Sausage Supper",
    "synonyms": ["Hearty Sausage Supper"]
},{
    "value": "Strawberry-Rhubarb Bread Pudding",
    "synonyms": ["Strawberry-Rhubarb Bread Pudding"]
},{
    "value": "Slow-Cooker Barbecue Beef Stew",
    "synonyms": ["Slow-Cooker Barbecue Beef Stew"]
},{
    "value": "Cookies and Cream Ice Cream from Cooking Light",
    "synonyms": ["Cookies and Cream Ice Cream from Cooking Light"]
},{
    "value": "Romano Rice and Beans",
    "synonyms": ["Romano Rice and Beans"]
},{
    "value": "Red Lentil and Vegetable Soup",
    "synonyms": ["Red Lentil and Vegetable Soup"]
},{
    "value": "Cajun Green Beans",
    "synonyms": ["Cajun Green Beans"]
},{
    "value": "Veggie-Chili Sweet Potato Wedges",
    "synonyms": ["Veggie-Chili Sweet Potato Wedges"]
},{
    "value": "Green Curry Thai for Kings",
    "synonyms": ["Green Curry Thai for Kings"]
},{
    "value": "Bonnell's Roasted Green Chili Cheese Grits",
    "synonyms": ["Bonnell's Roasted Green Chili Cheese Grits"]
},{
    "value": "Caribbean Shrimp and Vegetable Soup",
    "synonyms": ["Caribbean Shrimp and Vegetable Soup"]
},{
    "value": "Balsamic Vinaigrette Chicken & Veggies Stir Fry",
    "synonyms": ["Balsamic Vinaigrette Chicken & Veggies Stir Fry"]
},{
    "value": "Spinach and Kale Turnovers",
    "synonyms": ["Spinach and Kale Turnovers"]
},{
    "value": "Authentic & Easy Mapo Doufu",
    "synonyms": ["Authentic & Easy Mapo Doufu"]
},{
    "value": "Angel Hair with Chicken Sausages, Tomatoes and Kale",
    "synonyms": ["Angel Hair with Chicken Sausages, Tomatoes and Kale"]
},{
    "value": "Moroccan Ramadan Soup",
    "synonyms": ["Moroccan Ramadan Soup"]
},{
    "value": "Patriotic Pasta",
    "synonyms": ["Patriotic Pasta"]
},{
    "value": "Garlic Sesame Noodle Salad",
    "synonyms": ["Garlic Sesame Noodle Salad"]
},{
    "value": "Garlic Cheese Grits",
    "synonyms": ["Garlic Cheese Grits"]
},{
    "value": "Crock Pot Ranch Potatoes",
    "synonyms": ["Crock Pot Ranch Potatoes"]
},{
    "value": "Les Petits Pomme De Terre Roasted Fingerling Potatoes",
    "synonyms": ["Les Petits Pomme De Terre Roasted Fingerling Potatoes"]
},{
    "value": "Garlicky Shrimp & Parmesan Grits",
    "synonyms": ["Garlicky Shrimp & Parmesan Grits"]
},{
    "value": "Vegan Pierogies",
    "synonyms": ["Vegan Pierogies"]
},{
    "value": "Maryland Crab Cakes I",
    "synonyms": ["Maryland Crab Cakes I"]
},{
    "value": "Quick Cream Cheesy Taters",
    "synonyms": ["Quick Cream Cheesy Taters"]
},{
    "value": "Polenta with balsamic blistered tomatoes",
    "synonyms": ["Polenta with balsamic blistered tomatoes"]
},{
    "value": "Twice Baked Potatoes with Cheddar and Bacon",
    "synonyms": ["Twice Baked Potatoes with Cheddar and Bacon"]
},{
    "value": "Over-Easy BLT",
    "synonyms": ["Over-Easy BLT"]
},{
    "value": "Tofu Turkey I",
    "synonyms": ["Tofu Turkey I"]
},{
    "value": "Mediterranean Bean Salad",
    "synonyms": ["Mediterranean Bean Salad"]
},{
    "value": "Chicken and Chorizo Pasta",
    "synonyms": ["Chicken and Chorizo Pasta"]
},{
    "value": "Shrimp and Watermelon Skillet",
    "synonyms": ["Shrimp and Watermelon Skillet"]
},{
    "value": "Sublime Non-Fried Creamed Corn Tofu Croquettes",
    "synonyms": ["Sublime Non-Fried Creamed Corn Tofu Croquettes"]
},{
    "value": "' Drowned' Cauliflower Calzone Pie",
    "synonyms": ["' Drowned' Cauliflower Calzone Pie"]
},{
    "value": "Cowboy Scramble",
    "synonyms": ["Cowboy Scramble"]
},{
    "value": "Tuscan Chicken Salad with Arugula and Artichokes",
    "synonyms": ["Tuscan Chicken Salad with Arugula and Artichokes"]
},{
    "value": "Grown up Mac and Cheese",
    "synonyms": ["Grown up Mac and Cheese"]
},{
    "value": "Perfect for Fall Veggie Curry",
    "synonyms": ["Perfect for Fall Veggie Curry"]
},{
    "value": "Baked Tofu Bites on a Bed of Leafy Romaine",
    "synonyms": ["Baked Tofu Bites on a Bed of Leafy Romaine"]
},{
    "value": "Roasted Thyme Potato Slices",
    "synonyms": ["Roasted Thyme Potato Slices"]
},{
    "value": "Slow-Cooker Sweet & Sour Pork",
    "synonyms": ["Slow-Cooker Sweet & Sour Pork"]
},{
    "value": "Everything Juice!",
    "synonyms": ["Everything Juice!"]
},{
    "value": "Celyne's Green Juice - Juicer Recipe",
    "synonyms": ["Celyne's Green Juice - Juicer Recipe"]
},{
    "value": "Bacon, Potato and Onion Frittata",
    "synonyms": ["Bacon, Potato and Onion Frittata"]
},{
    "value": "Seasoned Potato Wedges",
    "synonyms": ["Seasoned Potato Wedges"]
},{
    "value": "Cauliflower Fritters",
    "synonyms": ["Cauliflower Fritters"]
},{
    "value": "Cream of Broccoli Soup",
    "synonyms": ["Cream of Broccoli Soup"]
},{
    "value": "Chipotle Queso Grilled Corn and Chicken Pasta",
    "synonyms": ["Chipotle Queso Grilled Corn and Chicken Pasta"]
},{
    "value": "Vegan Lasagna II",
    "synonyms": ["Vegan Lasagna II"]
},{
    "value": "Easy Breakfast Bake",
    "synonyms": ["Easy Breakfast Bake"]
},{
    "value": "Soft Chocolate Cookies Made with Tofu",
    "synonyms": ["Soft Chocolate Cookies Made with Tofu"]
},{
    "value": "Tofu Fried Rice",
    "synonyms": ["Tofu Fried Rice"]
},{
    "value": "Tofu Gumbo",
    "synonyms": ["Tofu Gumbo"]
},{
    "value": "Artichoke Babies With Shrimp and Feta",
    "synonyms": ["Artichoke Babies With Shrimp and Feta"]
},{
    "value": "Savory French Toast with Homemade Sausage Patties and Warm Honey, Strawberries and Basil",
    "synonyms": ["Savory French Toast with Homemade Sausage Patties and Warm Honey, Strawberries and Basil"]
},{
    "value": "Pesto Green Beans",
    "synonyms": ["Pesto Green Beans"]
},{
    "value": "Sesame Tofu Stir-Fry",
    "synonyms": ["Sesame Tofu Stir-Fry"]
},{
    "value": "Stout-Braised Short Ribs",
    "synonyms": ["Stout-Braised Short Ribs"]
},{
    "value": "Oven-Roasted Fries",
    "synonyms": ["Oven-Roasted Fries"]
},{
    "value": "Easy Crock Pot Rotisserie Chicken",
    "synonyms": ["Easy Crock Pot Rotisserie Chicken"]
},{
    "value": "Curried Brown Rice, Lentil & Chorizo Salad",
    "synonyms": ["Curried Brown Rice, Lentil & Chorizo Salad"]
},{
    "value": "Pav Bhaji",
    "synonyms": ["Pav Bhaji"]
},{
    "value": "Teriyaki Grilled Tofu",
    "synonyms": ["Teriyaki Grilled Tofu"]
},{
    "value": "Shrimp, Sausage and Grits",
    "synonyms": ["Shrimp, Sausage and Grits"]
},{
    "value": "H's Creamy Cauli-mash",
    "synonyms": ["H's Creamy Cauli-mash"]
},{
    "value": "Potatoes Madras",
    "synonyms": ["Potatoes Madras"]
},{
    "value": "Creamy Polenta",
    "synonyms": ["Creamy Polenta"]
},{
    "value": "Green Bean and Potatoes",
    "synonyms": ["Green Bean and Potatoes"]
},{
    "value": "Cream of Onion Soup",
    "synonyms": ["Cream of Onion Soup"]
},{
    "value": "Tortilla Espanola Spanish Tortilla",
    "synonyms": ["Tortilla Espanola Spanish Tortilla"]
},{
    "value": "Layered Cauliflower Salad",
    "synonyms": ["Layered Cauliflower Salad"]
},{
    "value": "Easy Green Beans Masala",
    "synonyms": ["Easy Green Beans Masala"]
},{
    "value": "Presto Pasta Salad",
    "synonyms": ["Presto Pasta Salad"]
},{
    "value": "Le Bernardin's Salmon-Caviar Croque-Monsieur",
    "synonyms": ["Le Bernardin's Salmon-Caviar Croque-Monsieur"]
},{
    "value": "Baked Lentil Munchies",
    "synonyms": ["Baked Lentil Munchies"]
},{
    "value": "Thai Lettuce Wraps",
    "synonyms": ["Thai Lettuce Wraps"]
},{
    "value": "Chicken Rustigo",
    "synonyms": ["Chicken Rustigo"]
},{
    "value": "Green Beans Chicken",
    "synonyms": ["Green Beans Chicken"]
},{
    "value": "Avocado, Bacon, Ham & Cheese Sandwich",
    "synonyms": ["Avocado, Bacon, Ham & Cheese Sandwich"]
},{
    "value": "Green Beans with Sage and Pancetta",
    "synonyms": ["Green Beans with Sage and Pancetta"]
},{
    "value": "Awesome Green Beans",
    "synonyms": ["Awesome Green Beans"]
},{
    "value": "Herbed Potatoes 'n' Peas",
    "synonyms": ["Herbed Potatoes 'n' Peas"]
},{
    "value": "Detox Salad",
    "synonyms": ["Detox Salad"]
},{
    "value": "Slow Cooker Tortellini",
    "synonyms": ["Slow Cooker Tortellini"]
},{
    "value": "Slow Cooker Potatoes Au Gratin by Kim",
    "synonyms": ["Slow Cooker Potatoes Au Gratin by Kim"]
},{
    "value": "Lentil Pate",
    "synonyms": ["Lentil Pate"]
},{
    "value": "Penne With Heirloom Tomatoes, Basil, Green Beans and Feta",
    "synonyms": ["Penne With Heirloom Tomatoes, Basil, Green Beans and Feta"]
},{
    "value": "Fresh Artichokes, Preparing and Serving",
    "synonyms": ["Fresh Artichokes, Preparing and Serving"]
},{
    "value": "Egyptian Lentil Soup",
    "synonyms": ["Egyptian Lentil Soup"]
},{
    "value": "Mustard-Crusted Beef Tenderloin with Arugula, Red Onion, and Wax Bean Salad",
    "synonyms": ["Mustard-Crusted Beef Tenderloin with Arugula, Red Onion, and Wax Bean Salad"]
},{
    "value": "Curried Lentil Soup",
    "synonyms": ["Curried Lentil Soup"]
},{
    "value": "Broccoli Cauliflower Shrimp Casserole",
    "synonyms": ["Broccoli Cauliflower Shrimp Casserole"]
},{
    "value": "Salad of Lentils and Coriander",
    "synonyms": ["Salad of Lentils and Coriander"]
},{
    "value": "Cauliflower Pizza Crust Recipe",
    "synonyms": ["Cauliflower Pizza Crust Recipe"]
},{
    "value": "Crock Pot Chicken Corn Soup",
    "synonyms": ["Crock Pot Chicken Corn Soup"]
},{
    "value": "Jelly Grill Sandwich",
    "synonyms": ["Jelly Grill Sandwich"]
},{
    "value": "Roasted Root Vegetables With Truffle Oil & Thyme",
    "synonyms": ["Roasted Root Vegetables With Truffle Oil & Thyme"]
},{
    "value": "Luis Troyano's tikka masala breadsticks recipe",
    "synonyms": ["Luis Troyano's tikka masala breadsticks recipe"]
},{
    "value": "Glutton's Own Potato Salad",
    "synonyms": ["Glutton's Own Potato Salad"]
},{
    "value": "Italian Beluga Lentil Salad",
    "synonyms": ["Italian Beluga Lentil Salad"]
},{
    "value": "Cauliflower and Sun-Dried Tomato Pie / Quiche",
    "synonyms": ["Cauliflower and Sun-Dried Tomato Pie / Quiche"]
},{
    "value": "Pasta with Artichokes, Sun Dried Tomatoes, and Spinach",
    "synonyms": ["Pasta with Artichokes, Sun Dried Tomatoes, and Spinach"]
},{
    "value": "Aloo Gobi",
    "synonyms": ["Aloo Gobi"]
},{
    "value": "Chamomile Cauliflower Soup",
    "synonyms": ["Chamomile Cauliflower Soup"]
},{
    "value": "Creamy Cauliflower Casserole",
    "synonyms": ["Creamy Cauliflower Casserole"]
},{
    "value": "Basil Lemon Green Beans",
    "synonyms": ["Basil Lemon Green Beans"]
},{
    "value": "Boone's Dr. Peppercorned Beef",
    "synonyms": ["Boone's Dr. Peppercorned Beef"]
},{
    "value": "Sweet and Sour Green Beans",
    "synonyms": ["Sweet and Sour Green Beans"]
},{
    "value": "Spinach Mushroom Pasta",
    "synonyms": ["Spinach Mushroom Pasta"]
},{
    "value": "Roasted Garlic and Smoky Greens Soup Recipe",
    "synonyms": ["Roasted Garlic and Smoky Greens Soup Recipe"]
},{
    "value": "Lentil Soup for People Who Thought They Hated Lentils! - Meat Op",
    "synonyms": ["Lentil Soup for People Who Thought They Hated Lentils! - Meat Op"]
},{
    "value": "Oven Fries II",
    "synonyms": ["Oven Fries II"]
},{
    "value": "Parsley-Basil Sauce",
    "synonyms": ["Parsley-Basil Sauce"]
},{
    "value": "Pesto Chicken Packets with Wheat Penne Pasta",
    "synonyms": ["Pesto Chicken Packets with Wheat Penne Pasta"]
},{
    "value": "Healthier Creamy Au Gratin Potatoes",
    "synonyms": ["Healthier Creamy Au Gratin Potatoes"]
},{
    "value": "Goody Girl Potatoes",
    "synonyms": ["Goody Girl Potatoes"]
},{
    "value": "Classic Grilled Cheese Sandwiches",
    "synonyms": ["Classic Grilled Cheese Sandwiches"]
},{
    "value": "Shiitake Scallopine",
    "synonyms": ["Shiitake Scallopine"]
},{
    "value": "Grilled Bacon Apple Sandwich",
    "synonyms": ["Grilled Bacon Apple Sandwich"]
},{
    "value": "Pasta Salad",
    "synonyms": ["Pasta Salad"]
},{
    "value": "Best Ever Homemade French Fries",
    "synonyms": ["Best Ever Homemade French Fries"]
},{
    "value": "Better Than Mother's Latkes",
    "synonyms": ["Better Than Mother's Latkes"]
},{
    "value": "Chicken Piccata Pasta",
    "synonyms": ["Chicken Piccata Pasta"]
},{
    "value": "Caramelized Vegetable and Polenta Cake",
    "synonyms": ["Caramelized Vegetable and Polenta Cake"]
},{
    "value": "Baked Farro with Artichoke and Peas",
    "synonyms": ["Baked Farro with Artichoke and Peas"]
},{
    "value": "Chicken Waldorf Sandwiches Diabetic",
    "synonyms": ["Chicken Waldorf Sandwiches Diabetic"]
},{
    "value": "Veggie Penne Pasta Salad",
    "synonyms": ["Veggie Penne Pasta Salad"]
},{
    "value": "Potato Pancakes",
    "synonyms": ["Potato Pancakes"]
},{
    "value": "Artichoke and Chicken Bake",
    "synonyms": ["Artichoke and Chicken Bake"]
},{
    "value": "Skillet Sausages with Lentil and Orange Salad",
    "synonyms": ["Skillet Sausages with Lentil and Orange Salad"]
},{
    "value": "Chicken-Artichoke Cheese Spread Gift Box",
    "synonyms": ["Chicken-Artichoke Cheese Spread Gift Box"]
},{
    "value": "Hot Artichoke and Spinach Dip",
    "synonyms": ["Hot Artichoke and Spinach Dip"]
},{
    "value": "Crispy BAKED Potato Skins",
    "synonyms": ["Crispy BAKED Potato Skins"]
},{
    "value": "Cheryl's Peppy Potatoes",
    "synonyms": ["Cheryl's Peppy Potatoes"]
},{
    "value": "Creamy Dreamy Mashed Potatoes",
    "synonyms": ["Creamy Dreamy Mashed Potatoes"]
},{
    "value": "Updated Spaghetti & Meatballs",
    "synonyms": ["Updated Spaghetti & Meatballs"]
},{
    "value": "Belizean Bread Pudding",
    "synonyms": ["Belizean Bread Pudding"]
},{
    "value": "Celebration Sandwich",
    "synonyms": ["Celebration Sandwich"]
},{
    "value": "Potato Rosemary Soup With Crispy Carrots",
    "synonyms": ["Potato Rosemary Soup With Crispy Carrots"]
},{
    "value": "Sweet Ho Yin Bread for Bread Machine",
    "synonyms": ["Sweet Ho Yin Bread for Bread Machine"]
},{
    "value": "Christmas Breakfast",
    "synonyms": ["Christmas Breakfast"]
},{
    "value": "Honey Wheat Bread With Chia and Flax",
    "synonyms": ["Honey Wheat Bread With Chia and Flax"]
},{
    "value": "Bread and butter pudding recipe",
    "synonyms": ["Bread and butter pudding recipe"]
},{
    "value": "Vegetarian Antipasto Pasta",
    "synonyms": ["Vegetarian Antipasto Pasta"]
},{
    "value": "Paul Hollywood's mozzarella and tomato bread recipe",
    "synonyms": ["Paul Hollywood's mozzarella and tomato bread recipe"]
},{
    "value": "Chocolate French Toast Pain Perdu by Melissa D'arabian",
    "synonyms": ["Chocolate French Toast Pain Perdu by Melissa D'arabian"]
},{
    "value": "Lentil-Walnut Loaf With Gravy",
    "synonyms": ["Lentil-Walnut Loaf With Gravy"]
},{
    "value": "Berry Pudding",
    "synonyms": ["Berry Pudding"]
},{
    "value": "French Onion Grilled Cheese",
    "synonyms": ["French Onion Grilled Cheese"]
},{
    "value": "Cheese & Pepper Omelet",
    "synonyms": ["Cheese & Pepper Omelet"]
},{
    "value": "Mac and Cheese",
    "synonyms": ["Mac and Cheese"]
},{
    "value": "Polenta Pasticciata, A Messed Up Polenta",
    "synonyms": ["Polenta Pasticciata, A Messed Up Polenta"]
},{
    "value": "Teff Polenta Croutons or Cakes",
    "synonyms": ["Teff Polenta Croutons or Cakes"]
},{
    "value": "Crispy Baked Chicken Tenders",
    "synonyms": ["Crispy Baked Chicken Tenders"]
},{
    "value": "Tomato pesto chicken penne",
    "synonyms": ["Tomato pesto chicken penne"]
},{
    "value": "Berry-Orange Sunrise Smoothie",
    "synonyms": ["Berry-Orange Sunrise Smoothie"]
},{
    "value": "Chicken Florentine",
    "synonyms": ["Chicken Florentine"]
},{
    "value": "Mushrooms and Tomatoes over Polenta",
    "synonyms": ["Mushrooms and Tomatoes over Polenta"]
},{
    "value": "Open faced tuna melt",
    "synonyms": ["Open faced tuna melt"]
},{
    "value": "Brazilian Fish Stew",
    "synonyms": ["Brazilian Fish Stew"]
},{
    "value": "Low-Fat Creamed Tuna on Toast",
    "synonyms": ["Low-Fat Creamed Tuna on Toast"]
},{
    "value": "Vegetarian Cincinnati Chili",
    "synonyms": ["Vegetarian Cincinnati Chili"]
},{
    "value": "Cheesy Polenta",
    "synonyms": ["Cheesy Polenta"]
},{
    "value": "Perfect Polenta",
    "synonyms": ["Perfect Polenta"]
},{
    "value": "Baked French Toast",
    "synonyms": ["Baked French Toast"]
},{
    "value": "Quinoa Veggie Burgers",
    "synonyms": ["Quinoa Veggie Burgers"]
},{
    "value": "Cream Cheese and Salmon Pinwheels",
    "synonyms": ["Cream Cheese and Salmon Pinwheels"]
},{
    "value": "Great South Bay Duck Ragu",
    "synonyms": ["Great South Bay Duck Ragu"]
},{
    "value": "Taco Pasta Toss",
    "synonyms": ["Taco Pasta Toss"]
},{
    "value": "Stir-Fried Sausage and Vegetables",
    "synonyms": ["Stir-Fried Sausage and Vegetables"]
},{
    "value": "Almond Butter and Fresh Blueberry Sandwich",
    "synonyms": ["Almond Butter and Fresh Blueberry Sandwich"]
},{
    "value": "Southwest Style Shrimp and Grits",
    "synonyms": ["Southwest Style Shrimp and Grits"]
},{
    "value": "Two Tomato Gratin",
    "synonyms": ["Two Tomato Gratin"]
},{
    "value": "Tuscan White Bean & Spinach Soup",
    "synonyms": ["Tuscan White Bean & Spinach Soup"]
},{
    "value": "Vegan Arepas Made with Polenta",
    "synonyms": ["Vegan Arepas Made with Polenta"]
},{
    "value": "Penne With Cherry Tomatoes, Asparagus, and Goat Cheese",
    "synonyms": ["Penne With Cherry Tomatoes, Asparagus, and Goat Cheese"]
},{
    "value": "Light & Fresh Tuna Melt",
    "synonyms": ["Light & Fresh Tuna Melt"]
},{
    "value": "Manchego Cheese Grits",
    "synonyms": ["Manchego Cheese Grits"]
},{
    "value": "Whole-Wheat Spaghetti with Tomato Sauce",
    "synonyms": ["Whole-Wheat Spaghetti with Tomato Sauce"]
},{
    "value": "Layered Polenta Loaf With Italian Sausage & Cheese",
    "synonyms": ["Layered Polenta Loaf With Italian Sausage & Cheese"]
},{
    "value": "Texas Forever Pasta Salad",
    "synonyms": ["Texas Forever Pasta Salad"]
},{
    "value": "Garlic Greens and White Beans over Creamy Polenta",
    "synonyms": ["Garlic Greens and White Beans over Creamy Polenta"]
},{
    "value": "Christmas French Toast",
    "synonyms": ["Christmas French Toast"]
},{
    "value": "Ragu Polenta and Sausage Bake #Ragu",
    "synonyms": ["Ragu Polenta and Sausage Bake #Ragu"]
},{
    "value": "Creamy Basil and Sun-Dried Tomato Vegan Pasta",
    "synonyms": ["Creamy Basil and Sun-Dried Tomato Vegan Pasta"]
},{
    "value": "Strawberry Stuffed French Toast",
    "synonyms": ["Strawberry Stuffed French Toast"]
},{
    "value": "Creamy Pasta With Roasted Zucchini, Almonds and Basil",
    "synonyms": ["Creamy Pasta With Roasted Zucchini, Almonds and Basil"]
},{
    "value": "Noodles with Edamame & Cashew-Lime Sauce",
    "synonyms": ["Noodles with Edamame & Cashew-Lime Sauce"]
},{
    "value": "Pancetta & Boursin Polenta With Sauteed Mixed Mushrooms",
    "synonyms": ["Pancetta & Boursin Polenta With Sauteed Mixed Mushrooms"]
},{
    "value": "Quick and Easy Cinnamon Toast",
    "synonyms": ["Quick and Easy Cinnamon Toast"]
},{
    "value": "A Feel Good Meal",
    "synonyms": ["A Feel Good Meal"]
},{
    "value": "Spinach Artichoke Mac and Cheese",
    "synonyms": ["Spinach Artichoke Mac and Cheese"]
},{
    "value": "Summer Evening Saute",
    "synonyms": ["Summer Evening Saute"]
},{
    "value": "Macaroni and Cheese Oven-Baked Casserole With a Dash of Spicy",
    "synonyms": ["Macaroni and Cheese Oven-Baked Casserole With a Dash of Spicy"]
},{
    "value": "Southern Shrimp and Grits",
    "synonyms": ["Southern Shrimp and Grits"]
},{
    "value": "Asian Inspired Brussels Sprout Salad",
    "synonyms": ["Asian Inspired Brussels Sprout Salad"]
},{
    "value": "Lighter Cheesy Pastitsio",
    "synonyms": ["Lighter Cheesy Pastitsio"]
},{
    "value": "Crisp Coleslaw Confetti Salad",
    "synonyms": ["Crisp Coleslaw Confetti Salad"]
},{
    "value": "Vegetarian Butternut Squash and Swiss Chard Polenta Lasagna Recipe",
    "synonyms": ["Vegetarian Butternut Squash and Swiss Chard Polenta Lasagna Recipe"]
},{
    "value": "English muffins recipe",
    "synonyms": ["English muffins recipe"]
},{
    "value": "Veggie Salad-Pasta Toss",
    "synonyms": ["Veggie Salad-Pasta Toss"]
},{
    "value": "Antonio Carluccio's polenta biscuits recipe",
    "synonyms": ["Antonio Carluccio's polenta biscuits recipe"]
},{
    "value": "Creamed Savoy Cabbage With Mushrooms And Buckwheat Pasta",
    "synonyms": ["Creamed Savoy Cabbage With Mushrooms And Buckwheat Pasta"]
},{
    "value": "Pepper Jack Shrimp and Grits",
    "synonyms": ["Pepper Jack Shrimp and Grits"]
},{
    "value": "Polenta with Green Beans, Mushrooms, Peas, and Leeks",
    "synonyms": ["Polenta with Green Beans, Mushrooms, Peas, and Leeks"]
},{
    "value": "Braised Pork Chops With Tomatoes",
    "synonyms": ["Braised Pork Chops With Tomatoes"]
},{
    "value": "Polenta With Goat Cheese and Rosemary",
    "synonyms": ["Polenta With Goat Cheese and Rosemary"]
},{
    "value": "Tomato Basil Pasta Bruschetta Pasta",
    "synonyms": ["Tomato Basil Pasta Bruschetta Pasta"]
},{
    "value": "Soft Polenta with Wild Mushroom Saute",
    "synonyms": ["Soft Polenta with Wild Mushroom Saute"]
},{
    "value": "Pomegranate Polenta Breakfast Bowl",
    "synonyms": ["Pomegranate Polenta Breakfast Bowl"]
},{
    "value": "Mini Polenta Pizzas With Tomatoes And Pesto",
    "synonyms": ["Mini Polenta Pizzas With Tomatoes And Pesto"]
},{
    "value": "Hominy Grits Souffle",
    "synonyms": ["Hominy Grits Souffle"]
},{
    "value": "Parmesan Polenta with Tomatoes and Prosciutto",
    "synonyms": ["Parmesan Polenta with Tomatoes and Prosciutto"]
},{
    "value": "Creamy White Polenta with Mushrooms and Mascarpone",
    "synonyms": ["Creamy White Polenta with Mushrooms and Mascarpone"]
},{
    "value": "Grits and Ham Casserole",
    "synonyms": ["Grits and Ham Casserole"]
},{
    "value": "Creamy Grits With Fontina Fonduta And Mushroom Stew",
    "synonyms": ["Creamy Grits With Fontina Fonduta And Mushroom Stew"]
},{
    "value": "TV's Grits Casserole",
    "synonyms": ["TV's Grits Casserole"]
},{
    "value": "Grits, Sausage, and Egg Casserole",
    "synonyms": ["Grits, Sausage, and Egg Casserole"]
},{
    "value": "Jack Cheese and Grits Souffle Recipe",
    "synonyms": ["Jack Cheese and Grits Souffle Recipe"]
},{
    "value": "Pork Chops with Apples and Creamy Bacon Cheese Grits",
    "synonyms": ["Pork Chops with Apples and Creamy Bacon Cheese Grits"]
},{
    "value": "Creamy Grits",
    "synonyms": ["Creamy Grits"]
},{
    "value": "Chicken with Slow-Roasted Tomatoes and Cheesy Grits",
    "synonyms": ["Chicken with Slow-Roasted Tomatoes and Cheesy Grits"]
},{
    "value": "Spicy Sweet Sausage Skillet w Rice",
    "synonyms": ["Spicy Sweet Sausage Skillet w Rice"]
},{
    "value": "Creamy Grits with Rosemary Bacon",
    "synonyms": ["Creamy Grits with Rosemary Bacon"]
},{
    "value": "Mrs. Payson's SPAM and Grits Brunch Casserole",
    "synonyms": ["Mrs. Payson's SPAM and Grits Brunch Casserole"]
},{
    "value": "Creamy Southern Shrimp and Cheese Grits",
    "synonyms": ["Creamy Southern Shrimp and Cheese Grits"]
},{
    "value": "Simple Shrimp and Grits",
    "synonyms": ["Simple Shrimp and Grits"]
},{
    "value": "Jalapeno Cheese Grits",
    "synonyms": ["Jalapeno Cheese Grits"]
},{
    "value": "Sausage and sprout stew",
    "synonyms": ["Sausage and sprout stew"]
},{
    "value": "Chili-Cheese Grits",
    "synonyms": ["Chili-Cheese Grits"]
},{
    "value": "Corn Grits Cornbread",
    "synonyms": ["Corn Grits Cornbread"]
},{
    "value": "Wafer Cookie Filled with Almonds and Figs",
    "synonyms": ["Wafer Cookie Filled with Almonds and Figs"]
},{
    "value": "Shrimp and Grits",
    "synonyms": ["Shrimp and Grits"]
},{
    "value": "tasty brussel sprouts",
    "synonyms": ["tasty brussel sprouts"]
},{
    "value": "Tahini Pasta",
    "synonyms": ["Tahini Pasta"]
},{
    "value": "Spring Pea Soup",
    "synonyms": ["Spring Pea Soup"]
},{
    "value": "Trifle",
    "synonyms": ["Trifle"]
},{
    "value": "Roasted Brussel Sprouts",
    "synonyms": ["Roasted Brussel Sprouts"]
},{
    "value": "Roasted Garlic Parmesan Brussel Sprouts",
    "synonyms": ["Roasted Garlic Parmesan Brussel Sprouts"]
},{
    "value": "Shredded Brussels Sprouts & Bacon Avocado Salad",
    "synonyms": ["Shredded Brussels Sprouts & Bacon Avocado Salad"]
},{
    "value": "Frozen Flower Bowls With Fruits",
    "synonyms": ["Frozen Flower Bowls With Fruits"]
},{
    "value": "Ambrosia Salad #100",
    "synonyms": ["Ambrosia Salad #100"]
},{
    "value": "Roasted Brussel Sprouts with Bacon and Apples",
    "synonyms": ["Roasted Brussel Sprouts with Bacon and Apples"]
},{
    "value": "Warm Brussel Sprout Pasta Salad",
    "synonyms": ["Warm Brussel Sprout Pasta Salad"]
},{
    "value": "Roasted Brussel Sprout Medley",
    "synonyms": ["Roasted Brussel Sprout Medley"]
},{
    "value": "Katies pan fried brussel sprouts",
    "synonyms": ["Katies pan fried brussel sprouts"]
},{
    "value": "Oven-Roasted Vegetables",
    "synonyms": ["Oven-Roasted Vegetables"]
},{
    "value": "Pan Fried Brussel Sprouts with Blood Orange and Applewood Bacon",
    "synonyms": ["Pan Fried Brussel Sprouts with Blood Orange and Applewood Bacon"]
},{
    "value": "Roasted brussel sprouts and potatoes",
    "synonyms": ["Roasted brussel sprouts and potatoes"]
},{
    "value": "Brussel Sprouts ala Angela from allrecipes.com posted by JimChicago52",
    "synonyms": ["Brussel Sprouts ala Angela from allrecipes.com posted by JimChicago52"]
},{
    "value": "Slow-Cooker Sweet and Savory Fruited Pork",
    "synonyms": ["Slow-Cooker Sweet and Savory Fruited Pork"]
},{
    "value": "Bacon Bleu Sprouts",
    "synonyms": ["Bacon Bleu Sprouts"]
},{
    "value": "Brussel Sprout Cole Slaw",
    "synonyms": ["Brussel Sprout Cole Slaw"]
},{
    "value": "Brussel sprouts with bacon and onion",
    "synonyms": ["Brussel sprouts with bacon and onion"]
},{
    "value": "Garlic, bacon brussels sprouts.",
    "synonyms": ["Garlic, bacon brussels sprouts."]
},{
    "value": "Butternut squash, Brussel sprout sausage mixture",
    "synonyms": ["Butternut squash, Brussel sprout sausage mixture"]
},{
    "value": "Bacon Roasted Brussel Sprouts",
    "synonyms": ["Bacon Roasted Brussel Sprouts"]
},{
    "value": "Spicy Brussels Sprouts with Garlic and Bacon",
    "synonyms": ["Spicy Brussels Sprouts with Garlic and Bacon"]
},{
    "value": "Oven baked brussel sprouts",
    "synonyms": ["Oven baked brussel sprouts"]
},{
    "value": "Homestead Hash",
    "synonyms": ["Homestead Hash"]
},{
    "value": "Stir-Fried Brussels Sprouts",
    "synonyms": ["Stir-Fried Brussels Sprouts"]
},{
    "value": "Catfish with Tropical Fruit Salsa",
    "synonyms": ["Catfish with Tropical Fruit Salsa"]
},{
    "value": "Biscuit-Topped Steak Pie",
    "synonyms": ["Biscuit-Topped Steak Pie"]
},{
    "value": "Pickled Brussel Sprouts",
    "synonyms": ["Pickled Brussel Sprouts"]
},{
    "value": "Bubble and Squeak",
    "synonyms": ["Bubble and Squeak"]
},{
    "value": "Brussels Sprouts",
    "synonyms": ["Brussels Sprouts"]
},{
    "value": "Summer Fruit Salad with Whipped Cream",
    "synonyms": ["Summer Fruit Salad with Whipped Cream"]
},{
    "value": "Cold pasta salad",
    "synonyms": ["Cold pasta salad"]
},{
    "value": "Couscous and Chicken Fruit Salad",
    "synonyms": ["Couscous and Chicken Fruit Salad"]
},{
    "value": "Almond Tofu",
    "synonyms": ["Almond Tofu"]
},{
    "value": "Tropical Fruit Whipped Delight",
    "synonyms": ["Tropical Fruit Whipped Delight"]
},{
    "value": "Easy Fruit Parfaits",
    "synonyms": ["Easy Fruit Parfaits"]
},{
    "value": "Crisp Confetti Salad",
    "synonyms": ["Crisp Confetti Salad"]
},{
    "value": "Greek Yoghurt and Fruit Salad",
    "synonyms": ["Greek Yoghurt and Fruit Salad"]
},{
    "value": "Ole! Crispy Garden Tacos",
    "synonyms": ["Ole! Crispy Garden Tacos"]
},{
    "value": "Minnie chicken pot pies",
    "synonyms": ["Minnie chicken pot pies"]
},{
    "value": "Summer Pea Soup",
    "synonyms": ["Summer Pea Soup"]
},{
    "value": "Spring Pea Soup",
    "synonyms": ["Spring Pea Soup"]
},{
    "value": "Easy Chicken Ala King",
    "synonyms": ["Easy Chicken Ala King"]
},{
    "value": "Taco Pie",
    "synonyms": ["Taco Pie"]
},{
    "value": "Easy Fruit Parfaits",
    "synonyms": ["Easy Fruit Parfaits"]
},{
    "value": "Balsamic Chicken Pasta with Fresh Cheese",
    "synonyms": ["Balsamic Chicken Pasta with Fresh Cheese"]
},{
    "value": "Ww 5 Points - Orange Chinese Chicken",
    "synonyms": ["Ww 5 Points - Orange Chinese Chicken"]
},{
    "value": "Butternut Kisses",
    "synonyms": ["Butternut Kisses"]
},{
    "value": "Vanilla Crumb Cakes / Muffins - Southern Living",
    "synonyms": ["Vanilla Crumb Cakes / Muffins - Southern Living"]
},{
    "value": "Hearty Soup in a Jar",
    "synonyms": ["Hearty Soup in a Jar"]
},{
    "value": "Greens With Warm Turkey Bacon Dressing Ww",
    "synonyms": ["Greens With Warm Turkey Bacon Dressing Ww"]
},{
    "value": "My Mamas Fried Chicken With Milk Gravy",
    "synonyms": ["My Mamas Fried Chicken With Milk Gravy"]
},{
    "value": "Xcellent Scalloped Eggplant! Aubergine",
    "synonyms": ["Xcellent Scalloped Eggplant! Aubergine"]
},{
    "value": "Marinade for Grilled Vegetables",
    "synonyms": ["Marinade for Grilled Vegetables"]
},{
    "value": "Golden Roast Turkey",
    "synonyms": ["Golden Roast Turkey"]
},{
    "value": "Caprese Panini Mozzarella, Tomatoes and Basil",
    "synonyms": ["Caprese Panini Mozzarella, Tomatoes and Basil"]
},{
    "value": "Healthy Blueberry Scones",
    "synonyms": ["Healthy Blueberry Scones"]
},{
    "value": "Arugula, Fennel & Pecan Salad with Sesame Orange Dressing",
    "synonyms": ["Arugula, Fennel & Pecan Salad with Sesame Orange Dressing"]
},{
    "value": "Pesto Bruschetta",
    "synonyms": ["Pesto Bruschetta"]
},{
    "value": "Dutch Slavinken With Milk Gravy Ground Pork Cylinders",
    "synonyms": ["Dutch Slavinken With Milk Gravy Ground Pork Cylinders"]
},{
    "value": "Crafty Crescent Lasagna",
    "synonyms": ["Crafty Crescent Lasagna"]
},{
    "value": "Turkey Club Casserole",
    "synonyms": ["Turkey Club Casserole"]
},{
    "value": "Healthy Chile Verde",
    "synonyms": ["Healthy Chile Verde"]
},{
    "value": "Slow-Cooked White Chili Crock Pot",
    "synonyms": ["Slow-Cooked White Chili Crock Pot"]
},{
    "value": "Homemade Marinara Sauce",
    "synonyms": ["Homemade Marinara Sauce"]
},{
    "value": "Vietnamese Spring Roll with Glass Noodles, Shrimp, Avocado & Arugula",
    "synonyms": ["Vietnamese Spring Roll with Glass Noodles, Shrimp, Avocado & Arugula"]
},{
    "value": "Turkey and Roasted Red Pepper Panini",
    "synonyms": ["Turkey and Roasted Red Pepper Panini"]
},{
    "value": "Almost Magic Pan Orange Almond Salad",
    "synonyms": ["Almost Magic Pan Orange Almond Salad"]
},{
    "value": "Spicy Tomato Juice",
    "synonyms": ["Spicy Tomato Juice"]
},{
    "value": "Pistachio Pancakes",
    "synonyms": ["Pistachio Pancakes"]
},{
    "value": "Ginger-Roasted Green Beans",
    "synonyms": ["Ginger-Roasted Green Beans"]
},{
    "value": "Garlic & Herb Fried Pork Chops",
    "synonyms": ["Garlic & Herb Fried Pork Chops"]
},{
    "value": "Pecan Cheese Bites",
    "synonyms": ["Pecan Cheese Bites"]
},{
    "value": "Molasses Drop Cookies",
    "synonyms": ["Molasses Drop Cookies"]
},{
    "value": "Angel Food Cake in a Loaf Pan",
    "synonyms": ["Angel Food Cake in a Loaf Pan"]
},{
    "value": "Homemade Ricotta with Lemon and Basil",
    "synonyms": ["Homemade Ricotta with Lemon and Basil"]
},{
    "value": "Chicken and Dumplings",
    "synonyms": ["Chicken and Dumplings"]
},{
    "value": "Low Fat Turkey Sloppy Joes",
    "synonyms": ["Low Fat Turkey Sloppy Joes"]
},{
    "value": "Paneed Pork Medallions",
    "synonyms": ["Paneed Pork Medallions"]
},{
    "value": "Lentil Soup with Chard",
    "synonyms": ["Lentil Soup with Chard"]
},{
    "value": "Zimbabwe Greens",
    "synonyms": ["Zimbabwe Greens"]
},{
    "value": "Church's Fried Chicken",
    "synonyms": ["Church's Fried Chicken"]
},{
    "value": "P-Nutty Rice Pudding",
    "synonyms": ["P-Nutty Rice Pudding"]
},{
    "value": "Skinny Jalapeno Cilantro Ranch Dip",
    "synonyms": ["Skinny Jalapeno Cilantro Ranch Dip"]
},{
    "value": "Pasta, Sausage, Cheese Supreme",
    "synonyms": ["Pasta, Sausage, Cheese Supreme"]
},{
    "value": "Corn Pudding",
    "synonyms": ["Corn Pudding"]
},{
    "value": "Chicken Cashew",
    "synonyms": ["Chicken Cashew"]
},{
    "value": "Watercress in Miso, Red Pepper Paste & Sesame Marinade",
    "synonyms": ["Watercress in Miso, Red Pepper Paste & Sesame Marinade"]
},{
    "value": "Basil Burgers",
    "synonyms": ["Basil Burgers"]
},{
    "value": "Roasted Sweet Potato Oven Fries",
    "synonyms": ["Roasted Sweet Potato Oven Fries"]
},{
    "value": "Beef Jerky",
    "synonyms": ["Beef Jerky"]
},{
    "value": "Roasted Parmesan Basil Tomatoes",
    "synonyms": ["Roasted Parmesan Basil Tomatoes"]
},{
    "value": "Chipotle Grilled Chicken Tacos with Pineapple Slaw",
    "synonyms": ["Chipotle Grilled Chicken Tacos with Pineapple Slaw"]
},{
    "value": "Our Special Caesar Salad",
    "synonyms": ["Our Special Caesar Salad"]
},{
    "value": "Cilantro-Pecan Pesto Layered Mediterranean Dip",
    "synonyms": ["Cilantro-Pecan Pesto Layered Mediterranean Dip"]
},{
    "value": "Beef 'n Bean Burrito Stack Crock Pot, Slow Cooker",
    "synonyms": ["Beef 'n Bean Burrito Stack Crock Pot, Slow Cooker"]
},{
    "value": "Valley Hot & Cool Blue Barnyard Burgers #RSC",
    "synonyms": ["Valley Hot & Cool Blue Barnyard Burgers #RSC"]
},{
    "value": "Mulligatawny Soup",
    "synonyms": ["Mulligatawny Soup"]
},{
    "value": "Pho by Mean Chef Vietnamese Beef & Rice-Noodle Soup",
    "synonyms": ["Pho by Mean Chef Vietnamese Beef & Rice-Noodle Soup"]
},{
    "value": "Weight Watchers Five Ingredient Pineapple Upside Down Cake",
    "synonyms": ["Weight Watchers Five Ingredient Pineapple Upside Down Cake"]
},{
    "value": "Taiwanese Daikon Radish Cakes",
    "synonyms": ["Taiwanese Daikon Radish Cakes"]
},{
    "value": "Ww Red Velvet Angel Food Cake",
    "synonyms": ["Ww Red Velvet Angel Food Cake"]
},{
    "value": "Daffodillies",
    "synonyms": ["Daffodillies"]
},{
    "value": "Dippy Eggs",
    "synonyms": ["Dippy Eggs"]
},{
    "value": "shrimp soup/ Chupa de camarones",
    "synonyms": ["shrimp soup/ Chupa de camarones"]
},{
    "value": "Beef Tenderloin Salad",
    "synonyms": ["Beef Tenderloin Salad"]
},{
    "value": "Crock Pot Tagine of Squash and Chickpeas With Mushrooms",
    "synonyms": ["Crock Pot Tagine of Squash and Chickpeas With Mushrooms"]
},{
    "value": "Pumpkin Spice Pound Cake",
    "synonyms": ["Pumpkin Spice Pound Cake"]
},{
    "value": "Isaan-Inspired Larb",
    "synonyms": ["Isaan-Inspired Larb"]
},{
    "value": "Asparagus Pasta With Toasted Pecans",
    "synonyms": ["Asparagus Pasta With Toasted Pecans"]
},{
    "value": "Hot Dog Soup",
    "synonyms": ["Hot Dog Soup"]
},{
    "value": "Beer Battered Fried Dove Breast",
    "synonyms": ["Beer Battered Fried Dove Breast"]
},{
    "value": "3 Minute Hot Cocoa Mug Cake",
    "synonyms": ["3 Minute Hot Cocoa Mug Cake"]
},{
    "value": "Lasagna Mexicana",
    "synonyms": ["Lasagna Mexicana"]
},{
    "value": "More Please! Ground Turkey Casserole",
    "synonyms": ["More Please! Ground Turkey Casserole"]
},{
    "value": "Baked Oatmeal",
    "synonyms": ["Baked Oatmeal"]
},{
    "value": "Beefy Calzones",
    "synonyms": ["Beefy Calzones"]
},{
    "value": "Blueberry Clafouti",
    "synonyms": ["Blueberry Clafouti"]
},{
    "value": "Tortilla Chip Casserole",
    "synonyms": ["Tortilla Chip Casserole"]
},{
    "value": "Pecan and Blue Cheese Brussels Sprout Salad",
    "synonyms": ["Pecan and Blue Cheese Brussels Sprout Salad"]
},{
    "value": "Southern Shrimp and Grits",
    "synonyms": ["Southern Shrimp and Grits"]
},{
    "value": "Banana Berry \"green\" Smoothie",
    "synonyms": ["Banana Berry \"green\" Smoothie"]
},{
    "value": "Granny's Cornbread Dressing",
    "synonyms": ["Granny's Cornbread Dressing"]
},{
    "value": "Creme Brulee Pie Pops",
    "synonyms": ["Creme Brulee Pie Pops"]
},{
    "value": "Only 4 Ingredients Peach Cobbler",
    "synonyms": ["Only 4 Ingredients Peach Cobbler"]
},{
    "value": "Mamma Mia! Fresh Italian Lasagne!",
    "synonyms": ["Mamma Mia! Fresh Italian Lasagne!"]
},{
    "value": "Red Pepper Couscous",
    "synonyms": ["Red Pepper Couscous"]
},{
    "value": "Better Than Hamburger Helper!",
    "synonyms": ["Better Than Hamburger Helper!"]
},{
    "value": "Fish Wellington",
    "synonyms": ["Fish Wellington"]
},{
    "value": "AMIEs FENNEL, ORANGE & HAZELNUT salad",
    "synonyms": ["AMIEs FENNEL, ORANGE & HAZELNUT salad"]
},{
    "value": "Quick & Delicious Hollandaise Sauce",
    "synonyms": ["Quick & Delicious Hollandaise Sauce"]
},{
    "value": "Southwest Roll-Ups",
    "synonyms": ["Southwest Roll-Ups"]
},{
    "value": "Strawberry Topped Angel Food Cake",
    "synonyms": ["Strawberry Topped Angel Food Cake"]
},{
    "value": "Saucy Steak Strips",
    "synonyms": ["Saucy Steak Strips"]
},{
    "value": "Gourmet Creations Roasted Garlic & Herb Shrimp Scampi",
    "synonyms": ["Gourmet Creations Roasted Garlic & Herb Shrimp Scampi"]
},{
    "value": "Cannoli Crepe Cake",
    "synonyms": ["Cannoli Crepe Cake"]
},{
    "value": "Panera Bread Tomato Mozzarella Salad",
    "synonyms": ["Panera Bread Tomato Mozzarella Salad"]
},{
    "value": "Soba Noodles in Cool Lime & Soy Sauce Broth with Chicken & Fresh Asian Turnip",
    "synonyms": ["Soba Noodles in Cool Lime & Soy Sauce Broth with Chicken & Fresh Asian Turnip"]
},{
    "value": "Greek Salad With Grilled Chicken",
    "synonyms": ["Greek Salad With Grilled Chicken"]
},{
    "value": "Espresso Chocolate Toffee Cookies",
    "synonyms": ["Espresso Chocolate Toffee Cookies"]
},{
    "value": "Leftover Turkey Tetrazzini",
    "synonyms": ["Leftover Turkey Tetrazzini"]
},{
    "value": "Chicken Cranberry Ring",
    "synonyms": ["Chicken Cranberry Ring"]
},{
    "value": "Blueberry Lime Salsa",
    "synonyms": ["Blueberry Lime Salsa"]
},{
    "value": "Mom's Banana Bread",
    "synonyms": ["Mom's Banana Bread"]
},{
    "value": "Bolludagur Buns - Icelandic Cream-Filled Buns",
    "synonyms": ["Bolludagur Buns - Icelandic Cream-Filled Buns"]
},{
    "value": "Brie Brunch Casserole",
    "synonyms": ["Brie Brunch Casserole"]
},{
    "value": "Denise's Taco Soup",
    "synonyms": ["Denise's Taco Soup"]
},{
    "value": "Southwest Chicken Soup in a Crockpot",
    "synonyms": ["Southwest Chicken Soup in a Crockpot"]
},{
    "value": "Chicken Fricassee",
    "synonyms": ["Chicken Fricassee"]
},{
    "value": "Unrolled Cabbage Rolls",
    "synonyms": ["Unrolled Cabbage Rolls"]
},{
    "value": "BCs Barbecue Sauce",
    "synonyms": ["BCs Barbecue Sauce"]
},{
    "value": "Baked Mini Pumpkin Donuts",
    "synonyms": ["Baked Mini Pumpkin Donuts"]
},{
    "value": "Fresh Peach Cobbler With Praline Biscuits",
    "synonyms": ["Fresh Peach Cobbler With Praline Biscuits"]
},{
    "value": "Strawberry Banana Stuffed Pancakes",
    "synonyms": ["Strawberry Banana Stuffed Pancakes"]
},{
    "value": "Potato-Plum Dumplings",
    "synonyms": ["Potato-Plum Dumplings"]
},{
    "value": "Oh No! Not Another Salad Recipe!",
    "synonyms": ["Oh No! Not Another Salad Recipe!"]
},{
    "value": "African Couscous Salad",
    "synonyms": ["African Couscous Salad"]
},{
    "value": "Rao's Blueberry Cream Cheese Filled Muffins",
    "synonyms": ["Rao's Blueberry Cream Cheese Filled Muffins"]
},{
    "value": "Fire Roasted Oysters",
    "synonyms": ["Fire Roasted Oysters"]
},{
    "value": "Balsamic Chicken Drumsticks Recipe",
    "synonyms": ["Balsamic Chicken Drumsticks Recipe"]
},{
    "value": "Loaded Olive Oil Dipping Sauce",
    "synonyms": ["Loaded Olive Oil Dipping Sauce"]
},{
    "value": "Coconut Barley Pilaf With Corn, Chicken and Cashews",
    "synonyms": ["Coconut Barley Pilaf With Corn, Chicken and Cashews"]
},{
    "value": "Eggs Italiano",
    "synonyms": ["Eggs Italiano"]
},{
    "value": "Roasted Brussels Sprouts with Cider Vinegar Sauce",
    "synonyms": ["Roasted Brussels Sprouts with Cider Vinegar Sauce"]
},{
    "value": "Make Delicious Savory Egg Custard Chawanmushi Using Instant Soup Mix",
    "synonyms": ["Make Delicious Savory Egg Custard Chawanmushi Using Instant Soup Mix"]
},{
    "value": "Couldn't Be Easier Slow Cooker Bread",
    "synonyms": ["Couldn't Be Easier Slow Cooker Bread"]
},{
    "value": "Cherry Winks",
    "synonyms": ["Cherry Winks"]
},{
    "value": "Salted Caramel Cashew Popcorn",
    "synonyms": ["Salted Caramel Cashew Popcorn"]
},{
    "value": "Spinach-Quinoa-Peach Salad with Honey-Sage Vinaigrette",
    "synonyms": ["Spinach-Quinoa-Peach Salad with Honey-Sage Vinaigrette"]
},{
    "value": "Sauteed Cabbage and Apples",
    "synonyms": ["Sauteed Cabbage and Apples"]
},{
    "value": "Sparkling Peach & Thyme Sorbet",
    "synonyms": ["Sparkling Peach & Thyme Sorbet"]
},{
    "value": "Homemade Almond Paste",
    "synonyms": ["Homemade Almond Paste"]
},{
    "value": "Homemade Mayonnaise",
    "synonyms": ["Homemade Mayonnaise"]
},{
    "value": "Wallys Beef Chili",
    "synonyms": ["Wallys Beef Chili"]
},{
    "value": "Apple-Cinnamon Waffles",
    "synonyms": ["Apple-Cinnamon Waffles"]
},{
    "value": "Mamma Mia! Fresh Italian Eggplant Parmigiano!",
    "synonyms": ["Mamma Mia! Fresh Italian Eggplant Parmigiano!"]
},{
    "value": "AMIEs SPAGHETTINI with OIL, GARLIC and CHILI",
    "synonyms": ["AMIEs SPAGHETTINI with OIL, GARLIC and CHILI"]
},{
    "value": "Carrot Raisin Salad",
    "synonyms": ["Carrot Raisin Salad"]
},{
    "value": "Gingerbread Waffles",
    "synonyms": ["Gingerbread Waffles"]
},{
    "value": "Crispy & Sweet Nut-Free Garbanzo Bean Snack Recipe",
    "synonyms": ["Crispy & Sweet Nut-Free Garbanzo Bean Snack Recipe"]
},{
    "value": "Chickpea Salad",
    "synonyms": ["Chickpea Salad"]
},{
    "value": "Meatballs in Rich Mushroom Sauce",
    "synonyms": ["Meatballs in Rich Mushroom Sauce"]
},{
    "value": "Spritz Cookies A Classic",
    "synonyms": ["Spritz Cookies A Classic"]
},{
    "value": "Boston Baked Beans in Bean Pot - Durgin-Park",
    "synonyms": ["Boston Baked Beans in Bean Pot - Durgin-Park"]
},{
    "value": "Morels in Marsala",
    "synonyms": ["Morels in Marsala"]
},{
    "value": "Caramel Pecan Sticky Bun Cookies",
    "synonyms": ["Caramel Pecan Sticky Bun Cookies"]
},{
    "value": "Quinoa Edamame Feta Salad",
    "synonyms": ["Quinoa Edamame Feta Salad"]
},{
    "value": "Stromboli",
    "synonyms": ["Stromboli"]
},{
    "value": "South of the Border Sandwiches",
    "synonyms": ["South of the Border Sandwiches"]
},{
    "value": "Zucchini Patties",
    "synonyms": ["Zucchini Patties"]
},{
    "value": "Grandmas Coleslaw",
    "synonyms": ["Grandmas Coleslaw"]
},{
    "value": "Pinwheels and Checkerboard Cookies",
    "synonyms": ["Pinwheels and Checkerboard Cookies"]
},{
    "value": "Alfredo Pasta Bake",
    "synonyms": ["Alfredo Pasta Bake"]
},{
    "value": "Blockbuster Cookies",
    "synonyms": ["Blockbuster Cookies"]
},{
    "value": "Mimi's Cafe Buttermilk Spice Muffins",
    "synonyms": ["Mimi's Cafe Buttermilk Spice Muffins"]
},{
    "value": "Party Time Taco Dip",
    "synonyms": ["Party Time Taco Dip"]
},{
    "value": "Country Fried Steak with Cream Gravy",
    "synonyms": ["Country Fried Steak with Cream Gravy"]
},{
    "value": "Healthy Red Velvet Oatmeal",
    "synonyms": ["Healthy Red Velvet Oatmeal"]
},{
    "value": "Mixed Berry Pavlova",
    "synonyms": ["Mixed Berry Pavlova"]
},{
    "value": "Vanilla Cupcakes with Cranberry Filling and Whipped Cream Frosting",
    "synonyms": ["Vanilla Cupcakes with Cranberry Filling and Whipped Cream Frosting"]
},{
    "value": "Biscuit Mix Tempura Vegetables",
    "synonyms": ["Biscuit Mix Tempura Vegetables"]
},{
    "value": "Garlic-Lemon Rotisserie Chicken With Moroccan Spices",
    "synonyms": ["Garlic-Lemon Rotisserie Chicken With Moroccan Spices"]
},{
    "value": "Chicken and Scallion Skewers",
    "synonyms": ["Chicken and Scallion Skewers"]
},{
    "value": "minty snowmen",
    "synonyms": ["minty snowmen"]
},{
    "value": "Cheddar Dill Buttermilk Biscuits",
    "synonyms": ["Cheddar Dill Buttermilk Biscuits"]
},{
    "value": "Egg Salad for a Crowd",
    "synonyms": ["Egg Salad for a Crowd"]
},{
    "value": "Braised Brussels Sprouts Rachael Ray",
    "synonyms": ["Braised Brussels Sprouts Rachael Ray"]
},{
    "value": "STRAWBERRY CHEESE CAKE TRIFLE",
    "synonyms": ["STRAWBERRY CHEESE CAKE TRIFLE"]
},{
    "value": "S.O.A.P Sausage, Onions, Apples and Potatoes",
    "synonyms": ["S.O.A.P Sausage, Onions, Apples and Potatoes"]
},{
    "value": "Basic Scones",
    "synonyms": ["Basic Scones"]
},{
    "value": "Chicken Quesadillas",
    "synonyms": ["Chicken Quesadillas"]
},{
    "value": "Vegetarian Lentil Burgers",
    "synonyms": ["Vegetarian Lentil Burgers"]
},{
    "value": "Braised Duck With Apples Recipe",
    "synonyms": ["Braised Duck With Apples Recipe"]
},{
    "value": "Anne's Chicken For Crock Pot",
    "synonyms": ["Anne's Chicken For Crock Pot"]
},{
    "value": "Popcorn chicken, Peaches, Pecan, Spinach Salad",
    "synonyms": ["Popcorn chicken, Peaches, Pecan, Spinach Salad"]
},{
    "value": "Sweet Corn Bread",
    "synonyms": ["Sweet Corn Bread"]
},{
    "value": "Sesame Noodles",
    "synonyms": ["Sesame Noodles"]
},{
    "value": "Crockpot Teriyaki Chicken",
    "synonyms": ["Crockpot Teriyaki Chicken"]
},{
    "value": "Deannes Sugar Cookies and Icing Recipe",
    "synonyms": ["Deannes Sugar Cookies and Icing Recipe"]
},{
    "value": "Dijon-Rosemary Crusted Prime Rib Roast with Pinot Noir Au jus",
    "synonyms": ["Dijon-Rosemary Crusted Prime Rib Roast with Pinot Noir Au jus"]
},{
    "value": "Buffalo Chip Cookies",
    "synonyms": ["Buffalo Chip Cookies"]
},{
    "value": "Chicken and White Bean Chili Verde",
    "synonyms": ["Chicken and White Bean Chili Verde"]
},{
    "value": "Cucumber Feta Salad",
    "synonyms": ["Cucumber Feta Salad"]
},{
    "value": "Vanilla Strawberry Pancakes",
    "synonyms": ["Vanilla Strawberry Pancakes"]
},{
    "value": "Pork Tenderloin with Caramelized Balsamic Pears",
    "synonyms": ["Pork Tenderloin with Caramelized Balsamic Pears"]
},{
    "value": "Oatmeal Coconut Pecan Cookies",
    "synonyms": ["Oatmeal Coconut Pecan Cookies"]
},{
    "value": "Blueberry Pie Ice Cream",
    "synonyms": ["Blueberry Pie Ice Cream"]
},{
    "value": "Jalapeno and Cheese Cornbread",
    "synonyms": ["Jalapeno and Cheese Cornbread"]
},{
    "value": "Our Favorite Banana Bread",
    "synonyms": ["Our Favorite Banana Bread"]
},{
    "value": "South African Brandy Squares",
    "synonyms": ["South African Brandy Squares"]
},{
    "value": "Chicken With Fettuccine",
    "synonyms": ["Chicken With Fettuccine"]
},{
    "value": "Grannys Sweet Dinner Rolls",
    "synonyms": ["Grannys Sweet Dinner Rolls"]
},{
    "value": "Kellys Country-style Fresh Pear Pie",
    "synonyms": ["Kellys Country-style Fresh Pear Pie"]
},{
    "value": "Brunch Casserole",
    "synonyms": ["Brunch Casserole"]
},{
    "value": "Pumpkin Thyme Risotto",
    "synonyms": ["Pumpkin Thyme Risotto"]
},{
    "value": "Thai Grilled Chicken Thighs",
    "synonyms": ["Thai Grilled Chicken Thighs"]
},{
    "value": "Rotisserie Duck with Hoisin Baste served with Grilled Oranges, Scallions and Pancakes",
    "synonyms": ["Rotisserie Duck with Hoisin Baste served with Grilled Oranges, Scallions and Pancakes"]
},{
    "value": "Bacon, Lettuce and Tomato Soup",
    "synonyms": ["Bacon, Lettuce and Tomato Soup"]
},{
    "value": "Dream Coffee Cake",
    "synonyms": ["Dream Coffee Cake"]
},{
    "value": "Pimento Cheese Quiche Appetizers",
    "synonyms": ["Pimento Cheese Quiche Appetizers"]
},{
    "value": "Salmon Rice Porcupines",
    "synonyms": ["Salmon Rice Porcupines"]
},{
    "value": "Brussels Sprouts With Kumquats And Smoked Salt",
    "synonyms": ["Brussels Sprouts With Kumquats And Smoked Salt"]
},{
    "value": "Asian Beef and Bok Choy Salad",
    "synonyms": ["Asian Beef and Bok Choy Salad"]
},{
    "value": "Mini Chocolate Oreo Donuts",
    "synonyms": ["Mini Chocolate Oreo Donuts"]
},{
    "value": "Chewy Peanut Butter Cookies",
    "synonyms": ["Chewy Peanut Butter Cookies"]
},{
    "value": "Risotto With Mushrooms",
    "synonyms": ["Risotto With Mushrooms"]
},{
    "value": "Ranch Dressing With Tofu!",
    "synonyms": ["Ranch Dressing With Tofu!"]
},{
    "value": "Grapefruit and Fennel Salad with Pear",
    "synonyms": ["Grapefruit and Fennel Salad with Pear"]
},{
    "value": "Summer Squash Saute",
    "synonyms": ["Summer Squash Saute"]
},{
    "value": "Spaghetti Squash Pasta with Shrimp, Asparagus, and Cherry Tomatoes",
    "synonyms": ["Spaghetti Squash Pasta with Shrimp, Asparagus, and Cherry Tomatoes"]
},{
    "value": "Boston Market Bake and Baste Chicken",
    "synonyms": ["Boston Market Bake and Baste Chicken"]
},{
    "value": "Slow Cooker Pork Chop And Potatoes in Mustard Sauce",
    "synonyms": ["Slow Cooker Pork Chop And Potatoes in Mustard Sauce"]
},{
    "value": "Bisquick Blueberry Muffins",
    "synonyms": ["Bisquick Blueberry Muffins"]
},{
    "value": "Mini Cheddar Swirl Buns",
    "synonyms": ["Mini Cheddar Swirl Buns"]
},{
    "value": "Miso Tomato Salsa",
    "synonyms": ["Miso Tomato Salsa"]
},{
    "value": "Zucchini Mushroom Rice Skillet",
    "synonyms": ["Zucchini Mushroom Rice Skillet"]
},{
    "value": "Chinese Honey-Soy Braised Chicken Wings Mut Jup Mun Gai Yik",
    "synonyms": ["Chinese Honey-Soy Braised Chicken Wings Mut Jup Mun Gai Yik"]
},{
    "value": "Creamy Mashed Potatoes",
    "synonyms": ["Creamy Mashed Potatoes"]
},{
    "value": "Crock Pot Beef Bourguinon",
    "synonyms": ["Crock Pot Beef Bourguinon"]
},{
    "value": "Emmas Wedding Cookies",
    "synonyms": ["Emmas Wedding Cookies"]
},{
    "value": "Pear Bread Delight",
    "synonyms": ["Pear Bread Delight"]
},{
    "value": "Asparagus Guacamole",
    "synonyms": ["Asparagus Guacamole"]
},{
    "value": "Raw Kale Salad with Almonds and Cranberries",
    "synonyms": ["Raw Kale Salad with Almonds and Cranberries"]
},{
    "value": "BBQ Pulled Venison",
    "synonyms": ["BBQ Pulled Venison"]
},{
    "value": "Old-Fashioned Baked Custard",
    "synonyms": ["Old-Fashioned Baked Custard"]
},{
    "value": "Frogmore Stew for 12",
    "synonyms": ["Frogmore Stew for 12"]
},{
    "value": "Cherry Pancakes for One",
    "synonyms": ["Cherry Pancakes for One"]
},{
    "value": "Carolyn & Nate's Caramel Apple Cheesecake",
    "synonyms": ["Carolyn & Nate's Caramel Apple Cheesecake"]
},{
    "value": "Spinach Salad with Strawberry Vinaigrette",
    "synonyms": ["Spinach Salad with Strawberry Vinaigrette"]
},{
    "value": "Scratch Gooey Butter Cake",
    "synonyms": ["Scratch Gooey Butter Cake"]
},{
    "value": "Duck Cassoulet",
    "synonyms": ["Duck Cassoulet"]
},{
    "value": "Cherry-Cream Crumble Pie",
    "synonyms": ["Cherry-Cream Crumble Pie"]
},{
    "value": "Oreo Key Lime Tartlets Brulee",
    "synonyms": ["Oreo Key Lime Tartlets Brulee"]
},{
    "value": "Bacon-Cheddar-Chive Scones",
    "synonyms": ["Bacon-Cheddar-Chive Scones"]
},{
    "value": "Nacho Chicken and Rice Wraps 4 Ww Points",
    "synonyms": ["Nacho Chicken and Rice Wraps 4 Ww Points"]
},{
    "value": "BBQ Chicken Stromboli",
    "synonyms": ["BBQ Chicken Stromboli"]
},{
    "value": "White Meat and Beans",
    "synonyms": ["White Meat and Beans"]
},{
    "value": "Spicy Ostrich Roast",
    "synonyms": ["Spicy Ostrich Roast"]
},{
    "value": "Baked Ziti",
    "synonyms": ["Baked Ziti"]
},{
    "value": "Papas Chorreadas Colombia, South America",
    "synonyms": ["Papas Chorreadas Colombia, South America"]
},{
    "value": "Red Curry with Shrimp & String Beans",
    "synonyms": ["Red Curry with Shrimp & String Beans"]
},{
    "value": "Panko Baked Avocado tacos",
    "synonyms": ["Panko Baked Avocado tacos"]
},{
    "value": "Butterfly Cake",
    "synonyms": ["Butterfly Cake"]
},{
    "value": "Basic Beer-Cheese Bread",
    "synonyms": ["Basic Beer-Cheese Bread"]
},{
    "value": "Pretty Bell Pepper Party Salad",
    "synonyms": ["Pretty Bell Pepper Party Salad"]
},{
    "value": "Chicken Tetrazzini",
    "synonyms": ["Chicken Tetrazzini"]
},{
    "value": "Dipped Gingersnaps",
    "synonyms": ["Dipped Gingersnaps"]
},{
    "value": "Sweet Potato Flan",
    "synonyms": ["Sweet Potato Flan"]
},{
    "value": "Spiral Macaroni Salad",
    "synonyms": ["Spiral Macaroni Salad"]
},{
    "value": "Red, White & Blue Nachos",
    "synonyms": ["Red, White & Blue Nachos"]
},{
    "value": "Cream Cheese Herb Biscuits",
    "synonyms": ["Cream Cheese Herb Biscuits"]
},{
    "value": "Fried Rice with Sweet Soy Sauce",
    "synonyms": ["Fried Rice with Sweet Soy Sauce"]
},{
    "value": "Lemon Ice Cream with Very Berry Swirl",
    "synonyms": ["Lemon Ice Cream with Very Berry Swirl"]
},{
    "value": "Pork Roast With Herbed Pepper Rub",
    "synonyms": ["Pork Roast With Herbed Pepper Rub"]
},{
    "value": "Pesto Zucchini Spaghetti",
    "synonyms": ["Pesto Zucchini Spaghetti"]
},{
    "value": "Chicken Burgers with Peanut Sauce",
    "synonyms": ["Chicken Burgers with Peanut Sauce"]
},{
    "value": "Low Fat Roasted Sweet Potato Salad",
    "synonyms": ["Low Fat Roasted Sweet Potato Salad"]
},{
    "value": "Pizza with Gluten Free Crust",
    "synonyms": ["Pizza with Gluten Free Crust"]
},{
    "value": "Flaked Salmon and Cucumber",
    "synonyms": ["Flaked Salmon and Cucumber"]
},{
    "value": "Lentil Goat Cheese Burgers",
    "synonyms": ["Lentil Goat Cheese Burgers"]
},{
    "value": "T - Bone Steaks With Garlic Butter Sauce",
    "synonyms": ["T - Bone Steaks With Garlic Butter Sauce"]
},{
    "value": "Asian Popcorn -Wasabi Sesame",
    "synonyms": ["Asian Popcorn -Wasabi Sesame"]
},{
    "value": "Honey Garlic Dressing",
    "synonyms": ["Honey Garlic Dressing"]
},{
    "value": "Classic Tenderloin With Balsamic Portabella Sauce",
    "synonyms": ["Classic Tenderloin With Balsamic Portabella Sauce"]
},{
    "value": "Reduced-Fat Baked Chiles Rellenos with Cheese and Mushrooms",
    "synonyms": ["Reduced-Fat Baked Chiles Rellenos with Cheese and Mushrooms"]
},{
    "value": "Pork Sausage Main Dish Pizza Puff Recipe",
    "synonyms": ["Pork Sausage Main Dish Pizza Puff Recipe"]
},{
    "value": "Cranberry Orange Bundt Cake",
    "synonyms": ["Cranberry Orange Bundt Cake"]
},{
    "value": "Pinwheel Cookies",
    "synonyms": ["Pinwheel Cookies"]
},{
    "value": "Simply Lasagna",
    "synonyms": ["Simply Lasagna"]
},{
    "value": "Almond Thin Crisp Coolies",
    "synonyms": ["Almond Thin Crisp Coolies"]
},{
    "value": "Aloo Ko Achar Potato Salad",
    "synonyms": ["Aloo Ko Achar Potato Salad"]
},{
    "value": "Grilled Flank Steak Salad with Chimichurri Dressing",
    "synonyms": ["Grilled Flank Steak Salad with Chimichurri Dressing"]
},{
    "value": "Chicken and Black Bean Taquitos With Adobo Sour Cream 3 Ww Poi",
    "synonyms": ["Chicken and Black Bean Taquitos With Adobo Sour Cream 3 Ww Poi"]
},{
    "value": "Redeye BBQ Rub",
    "synonyms": ["Redeye BBQ Rub"]
},{
    "value": "White Wine Basted Turkey",
    "synonyms": ["White Wine Basted Turkey"]
},{
    "value": "Filipino Arroz Caldo",
    "synonyms": ["Filipino Arroz Caldo"]
},{
    "value": "German Grilled Trout",
    "synonyms": ["German Grilled Trout"]
},{
    "value": "BBQ Spaghetti",
    "synonyms": ["BBQ Spaghetti"]
},{
    "value": "Spicy Shrimp Boil",
    "synonyms": ["Spicy Shrimp Boil"]
},{
    "value": "Sweet & Salty Zucchini Bacon Pasta",
    "synonyms": ["Sweet & Salty Zucchini Bacon Pasta"]
},{
    "value": "Weight Watchers Vanilla Pound Cake",
    "synonyms": ["Weight Watchers Vanilla Pound Cake"]
},{
    "value": "Sun-Dried Tomato Corn Bread",
    "synonyms": ["Sun-Dried Tomato Corn Bread"]
},{
    "value": "Easy Bake Cod",
    "synonyms": ["Easy Bake Cod"]
},{
    "value": "Lamb Tagine With Apricots, Olives and Buttered Almonds",
    "synonyms": ["Lamb Tagine With Apricots, Olives and Buttered Almonds"]
},{
    "value": "Tomato-Garlic Shrimp over Creamy Corn and Maple Sticky Bread",
    "synonyms": ["Tomato-Garlic Shrimp over Creamy Corn and Maple Sticky Bread"]
},{
    "value": "Lettuce-Wrapped Chicken Sausage with Soy-Oyster Dipping Sauce",
    "synonyms": ["Lettuce-Wrapped Chicken Sausage with Soy-Oyster Dipping Sauce"]
},{
    "value": "Fried Brown Rice with Red Pepper and Almond",
    "synonyms": ["Fried Brown Rice with Red Pepper and Almond"]
},{
    "value": "Chicken Noodle Soup",
    "synonyms": ["Chicken Noodle Soup"]
},{
    "value": "Ken Schrader's Chicken Spaghetti",
    "synonyms": ["Ken Schrader's Chicken Spaghetti"]
},{
    "value": "Chili Soup",
    "synonyms": ["Chili Soup"]
},{
    "value": "Chicken Enchilada Casserole",
    "synonyms": ["Chicken Enchilada Casserole"]
},{
    "value": "Easy Chinjao Rosu",
    "synonyms": ["Easy Chinjao Rosu"]
},{
    "value": "Meat n Potatoes Skillet Meal",
    "synonyms": ["Meat n Potatoes Skillet Meal"]
},{
    "value": "Asparagus Risotto",
    "synonyms": ["Asparagus Risotto"]
},{
    "value": "Spicy Yellow Rice",
    "synonyms": ["Spicy Yellow Rice"]
},{
    "value": "Peach Preserves",
    "synonyms": ["Peach Preserves"]
},{
    "value": "Chipotle Chicken Mango Salad",
    "synonyms": ["Chipotle Chicken Mango Salad"]
},{
    "value": "Beef and Green Chili Soup! Sure to Warm You Up!",
    "synonyms": ["Beef and Green Chili Soup! Sure to Warm You Up!"]
},{
    "value": "Nutella Cheesecake Bites",
    "synonyms": ["Nutella Cheesecake Bites"]
},{
    "value": "Hot & Sweet Drumsticks",
    "synonyms": ["Hot & Sweet Drumsticks"]
},{
    "value": "Chilled Corn Soup",
    "synonyms": ["Chilled Corn Soup"]
},{
    "value": "Classic Clam Chowder",
    "synonyms": ["Classic Clam Chowder"]
},{
    "value": "Sweet & Savory Stuffed Bell Pepper Soup",
    "synonyms": ["Sweet & Savory Stuffed Bell Pepper Soup"]
},{
    "value": "Corn and Cheddar Chowder",
    "synonyms": ["Corn and Cheddar Chowder"]
},{
    "value": "Garlic Parmesan Roasted Chickpea Snack",
    "synonyms": ["Garlic Parmesan Roasted Chickpea Snack"]
},{
    "value": "Southern Sunday Chicken",
    "synonyms": ["Southern Sunday Chicken"]
},{
    "value": "Guacamole with Black Beans",
    "synonyms": ["Guacamole with Black Beans"]
},{
    "value": "Classic Peanut Butter Cookies",
    "synonyms": ["Classic Peanut Butter Cookies"]
},{
    "value": "Cocadas Bolivian Coconut Candies",
    "synonyms": ["Cocadas Bolivian Coconut Candies"]
},{
    "value": "Cranberry Orange Scones",
    "synonyms": ["Cranberry Orange Scones"]
},{
    "value": "Moms Winter Tomato Pudding",
    "synonyms": ["Moms Winter Tomato Pudding"]
},{
    "value": "Caramelised onion soup recipe",
    "synonyms": ["Caramelised onion soup recipe"]
},{
    "value": "Hummingbird Cake",
    "synonyms": ["Hummingbird Cake"]
},{
    "value": "Peppermint Bark Bars",
    "synonyms": ["Peppermint Bark Bars"]
},{
    "value": "Fiesta Cheese Ball",
    "synonyms": ["Fiesta Cheese Ball"]
},{
    "value": "5 Ingredient Spaghetti",
    "synonyms": ["5 Ingredient Spaghetti"]
},{
    "value": "Cheerio Treats",
    "synonyms": ["Cheerio Treats"]
},{
    "value": "Roasted Garlic and Carrot Hummus",
    "synonyms": ["Roasted Garlic and Carrot Hummus"]
},{
    "value": "Chili Enchiladas",
    "synonyms": ["Chili Enchiladas"]
},{
    "value": "All American Cheeseburger Ring",
    "synonyms": ["All American Cheeseburger Ring"]
},{
    "value": "Chicken Tater Bake",
    "synonyms": ["Chicken Tater Bake"]
},{
    "value": "Corn Chowder with Ham",
    "synonyms": ["Corn Chowder with Ham"]
},{
    "value": "Iced Jelly Doughnuts",
    "synonyms": ["Iced Jelly Doughnuts"]
},{
    "value": "Best Pork BBQ Ever!",
    "synonyms": ["Best Pork BBQ Ever!"]
},{
    "value": "Cream of the Crop Celery Soup",
    "synonyms": ["Cream of the Crop Celery Soup"]
},{
    "value": "Zesty Artichoke Spinach Dip",
    "synonyms": ["Zesty Artichoke Spinach Dip"]
},{
    "value": "The Best Banana Bundt Cake Dorie Greenspan",
    "synonyms": ["The Best Banana Bundt Cake Dorie Greenspan"]
},{
    "value": "Peppercorn Beef Tenderloin with Gorgonzola and Chive Mousse",
    "synonyms": ["Peppercorn Beef Tenderloin with Gorgonzola and Chive Mousse"]
},{
    "value": "Chipotle Crab Cakes with Avocado-Mango Salsa",
    "synonyms": ["Chipotle Crab Cakes with Avocado-Mango Salsa"]
},{
    "value": "Pecan Crisps with Cranberries and Rosemary",
    "synonyms": ["Pecan Crisps with Cranberries and Rosemary"]
},{
    "value": "Yam Quesadillas",
    "synonyms": ["Yam Quesadillas"]
},{
    "value": "Fried Catfish with Cajun Spice",
    "synonyms": ["Fried Catfish with Cajun Spice"]
},{
    "value": "Grilled Tandoori Chicken and Red Onion Skewers With Couscous",
    "synonyms": ["Grilled Tandoori Chicken and Red Onion Skewers With Couscous"]
},{
    "value": "Chilled Rice and Artichoke Salad",
    "synonyms": ["Chilled Rice and Artichoke Salad"]
},{
    "value": "Easy Cheesy Fondue #Ragu",
    "synonyms": ["Easy Cheesy Fondue #Ragu"]
},{
    "value": "Wild Rice and Pork Soup",
    "synonyms": ["Wild Rice and Pork Soup"]
},{
    "value": "Kale Salad",
    "synonyms": ["Kale Salad"]
},{
    "value": "Pork Chops & Bacon Caramelized Onion Jam With Alfredo Potato",
    "synonyms": ["Pork Chops & Bacon Caramelized Onion Jam With Alfredo Potato"]
},{
    "value": "Tofu and Pork Rice Bowl",
    "synonyms": ["Tofu and Pork Rice Bowl"]
},{
    "value": "Cheaters Shrimp Gumbo",
    "synonyms": ["Cheaters Shrimp Gumbo"]
},{
    "value": "AMIEs PEPERONI AMMOLLICATI Peppers Stuffed w/Breadcrumbs & Parsley",
    "synonyms": ["AMIEs PEPERONI AMMOLLICATI Peppers Stuffed w/Breadcrumbs & Parsley"]
},{
    "value": "Creamy Chicken Manicotti",
    "synonyms": ["Creamy Chicken Manicotti"]
},{
    "value": "Chicken Quesadillas",
    "synonyms": ["Chicken Quesadillas"]
},{
    "value": "Baby Bok Choy with Portobello Mushrooms & Ginger",
    "synonyms": ["Baby Bok Choy with Portobello Mushrooms & Ginger"]
},{
    "value": "Espresso Powdered BBQ Brisket & Rub Recipe",
    "synonyms": ["Espresso Powdered BBQ Brisket & Rub Recipe"]
},{
    "value": "Sun-Dried Tomatoes 101",
    "synonyms": ["Sun-Dried Tomatoes 101"]
},{
    "value": "German Potato-Cheese Soup",
    "synonyms": ["German Potato-Cheese Soup"]
},{
    "value": "Beef Scotch Fillet & Rocket Salad",
    "synonyms": ["Beef Scotch Fillet & Rocket Salad"]
},{
    "value": "Corn Chowder",
    "synonyms": ["Corn Chowder"]
},{
    "value": "Grilled Potato Wedges",
    "synonyms": ["Grilled Potato Wedges"]
},{
    "value": "Mexican Chicken Gumbo",
    "synonyms": ["Mexican Chicken Gumbo"]
},{
    "value": "Greek Gyro",
    "synonyms": ["Greek Gyro"]
},{
    "value": "LEMON CHEESECAKE",
    "synonyms": ["LEMON CHEESECAKE"]
},{
    "value": "Butter Bean Hummus",
    "synonyms": ["Butter Bean Hummus"]
},{
    "value": "Pepperoncini Firecrackers",
    "synonyms": ["Pepperoncini Firecrackers"]
},{
    "value": "Vanilla Cinnamon Ice Cream",
    "synonyms": ["Vanilla Cinnamon Ice Cream"]
},{
    "value": "Nutella Caramel Corn",
    "synonyms": ["Nutella Caramel Corn"]
},{
    "value": "Spinach-Ricotta Rolled Turkey Breasts",
    "synonyms": ["Spinach-Ricotta Rolled Turkey Breasts"]
},{
    "value": "Chicken BLT Salad",
    "synonyms": ["Chicken BLT Salad"]
},{
    "value": "Monkey 74's Pumpkin Pie Moonshine",
    "synonyms": ["Monkey 74's Pumpkin Pie Moonshine"]
},{
    "value": "Buttery Deviled Eggs",
    "synonyms": ["Buttery Deviled Eggs"]
},{
    "value": "Smoked Prime Rib",
    "synonyms": ["Smoked Prime Rib"]
},{
    "value": "BBQ Shepherd's Pie",
    "synonyms": ["BBQ Shepherd's Pie"]
},{
    "value": "Scrambled Ktarian Eggs",
    "synonyms": ["Scrambled Ktarian Eggs"]
},{
    "value": "Cheesy Potato Skins",
    "synonyms": ["Cheesy Potato Skins"]
},{
    "value": "Non-Alcoholic Grasshopper Pie",
    "synonyms": ["Non-Alcoholic Grasshopper Pie"]
},{
    "value": "Crabmeat Remick",
    "synonyms": ["Crabmeat Remick"]
},{
    "value": "Boca Filled Enchiladas",
    "synonyms": ["Boca Filled Enchiladas"]
},{
    "value": "Indian Mava Minicakes With Pistachios & Saffron",
    "synonyms": ["Indian Mava Minicakes With Pistachios & Saffron"]
},{
    "value": "Crab n Cheese Spirals",
    "synonyms": ["Crab n Cheese Spirals"]
},{
    "value": "Partially Homemade Meatballs and Marinara",
    "synonyms": ["Partially Homemade Meatballs and Marinara"]
},{
    "value": "Bubble & Squeak",
    "synonyms": ["Bubble & Squeak"]
},{
    "value": "Caribbean Conch Fritters With Island ' Hot' Sauce",
    "synonyms": ["Caribbean Conch Fritters With Island ' Hot' Sauce"]
},{
    "value": "Tuna and Hummus Sandwiches",
    "synonyms": ["Tuna and Hummus Sandwiches"]
},{
    "value": "Wonderful Gluten-Free Sandwich Bread",
    "synonyms": ["Wonderful Gluten-Free Sandwich Bread"]
},{
    "value": "Zucchini-Cheese Puff",
    "synonyms": ["Zucchini-Cheese Puff"]
},{
    "value": "Down-Home Chicken Pot Pie",
    "synonyms": ["Down-Home Chicken Pot Pie"]
},{
    "value": "Personal Sized Baked Oatmeal with Individual Toppings Gluten-Free, Diabetic Friendly",
    "synonyms": ["Personal Sized Baked Oatmeal with Individual Toppings Gluten-Free, Diabetic Friendly"]
},{
    "value": "Spiced Pear Breakfast Crumble",
    "synonyms": ["Spiced Pear Breakfast Crumble"]
},{
    "value": "Double Dark Chocolate Cake",
    "synonyms": ["Double Dark Chocolate Cake"]
},{
    "value": "Red Velvet Ice Cream Sandwiches",
    "synonyms": ["Red Velvet Ice Cream Sandwiches"]
},{
    "value": "Spinach Baked Tilapia",
    "synonyms": ["Spinach Baked Tilapia"]
},{
    "value": "Peaches & Cream Ice Cream",
    "synonyms": ["Peaches & Cream Ice Cream"]
},{
    "value": "Mango-Tamarind Chutney",
    "synonyms": ["Mango-Tamarind Chutney"]
},{
    "value": "Twice-Baked Alfredo Potatoes",
    "synonyms": ["Twice-Baked Alfredo Potatoes"]
},{
    "value": "Homemade Onion Soup",
    "synonyms": ["Homemade Onion Soup"]
},{
    "value": "Vidalia Onion Pie",
    "synonyms": ["Vidalia Onion Pie"]
},{
    "value": "Corned Venison",
    "synonyms": ["Corned Venison"]
},{
    "value": "Easy Jacket Potatoes",
    "synonyms": ["Easy Jacket Potatoes"]
},{
    "value": "Honey-Nut Banana Muffins Gluten-free & Naturally Sweetened",
    "synonyms": ["Honey-Nut Banana Muffins Gluten-free & Naturally Sweetened"]
},{
    "value": "Marinated Grilled Asparagus",
    "synonyms": ["Marinated Grilled Asparagus"]
},{
    "value": "Raspberry Filled Crepes",
    "synonyms": ["Raspberry Filled Crepes"]
},{
    "value": "Japanese Steakhouse Ginger Salad Dressing CopyCat Shogun Steak",
    "synonyms": ["Japanese Steakhouse Ginger Salad Dressing CopyCat Shogun Steak"]
},{
    "value": "Cinnamon Sugar Cookies",
    "synonyms": ["Cinnamon Sugar Cookies"]
},{
    "value": "Eggs Monterey",
    "synonyms": ["Eggs Monterey"]
},{
    "value": "Drip Beef Quesadillas with Dijon Garlic Butter",
    "synonyms": ["Drip Beef Quesadillas with Dijon Garlic Butter"]
},{
    "value": "Scotcheroo Ice Cream",
    "synonyms": ["Scotcheroo Ice Cream"]
},{
    "value": "Grilled Turkey Legs",
    "synonyms": ["Grilled Turkey Legs"]
},{
    "value": "Steamed Green Beans with Lemon-Mint Dressing",
    "synonyms": ["Steamed Green Beans with Lemon-Mint Dressing"]
},{
    "value": "Artichokes Gourmet",
    "synonyms": ["Artichokes Gourmet"]
},{
    "value": "Quick Cinnamon Streusel Coffee Cake",
    "synonyms": ["Quick Cinnamon Streusel Coffee Cake"]
},{
    "value": "Basic Gorgonzola Sauce",
    "synonyms": ["Basic Gorgonzola Sauce"]
},{
    "value": "Tinklee's Tamale Pie",
    "synonyms": ["Tinklee's Tamale Pie"]
},{
    "value": "Tuscan Parmesan Pillows",
    "synonyms": ["Tuscan Parmesan Pillows"]
},{
    "value": "Homemade Pancake Mix",
    "synonyms": ["Homemade Pancake Mix"]
},{
    "value": "Chocolate Cherry Ice Cream Sandwiches",
    "synonyms": ["Chocolate Cherry Ice Cream Sandwiches"]
},{
    "value": "Lazy Daisy Cake",
    "synonyms": ["Lazy Daisy Cake"]
},{
    "value": "Homemade Gnocchi with Brown Butter and Sage",
    "synonyms": ["Homemade Gnocchi with Brown Butter and Sage"]
},{
    "value": "Acorn Squash Lasagna",
    "synonyms": ["Acorn Squash Lasagna"]
},{
    "value": "Classic from the Box All Bran Muffins Recipe",
    "synonyms": ["Classic from the Box All Bran Muffins Recipe"]
},{
    "value": "Thai Coconut Curry Chicken Soup",
    "synonyms": ["Thai Coconut Curry Chicken Soup"]
},{
    "value": "Creamy Peanut Butter Soup",
    "synonyms": ["Creamy Peanut Butter Soup"]
},{
    "value": "Easy Muffin Melts",
    "synonyms": ["Easy Muffin Melts"]
},{
    "value": "Creamed Peas and Onions",
    "synonyms": ["Creamed Peas and Onions"]
},{
    "value": "Sea Bass with Citrus and Soy",
    "synonyms": ["Sea Bass with Citrus and Soy"]
},{
    "value": "Chicken Fingers",
    "synonyms": ["Chicken Fingers"]
},{
    "value": "Super Bowl Cookies",
    "synonyms": ["Super Bowl Cookies"]
},{
    "value": "Ww 3 Points - Impossibly Easy Breakfast Casserole",
    "synonyms": ["Ww 3 Points - Impossibly Easy Breakfast Casserole"]
},{
    "value": "Lavender Honey Ice Cream",
    "synonyms": ["Lavender Honey Ice Cream"]
},{
    "value": "Glazed Snickerdoodle Pecan Muffins",
    "synonyms": ["Glazed Snickerdoodle Pecan Muffins"]
},{
    "value": "Crock Pot Teriyaki Pork Tenderloin",
    "synonyms": ["Crock Pot Teriyaki Pork Tenderloin"]
},{
    "value": "Dipped Gingersnaps",
    "synonyms": ["Dipped Gingersnaps"]
},{
    "value": "Lovely Lemon Curd",
    "synonyms": ["Lovely Lemon Curd"]
},{
    "value": "Hearty Meatball Sandwich",
    "synonyms": ["Hearty Meatball Sandwich"]
},{
    "value": "Lazy Carbonara Penne for a Crowd - Sacbee 11/5/08",
    "synonyms": ["Lazy Carbonara Penne for a Crowd - Sacbee 11/5/08"]
},{
    "value": "Aloha Quick Bread",
    "synonyms": ["Aloha Quick Bread"]
},{
    "value": "Sweet Snack Mix",
    "synonyms": ["Sweet Snack Mix"]
},{
    "value": "Orange Chicken Teriyaki",
    "synonyms": ["Orange Chicken Teriyaki"]
},{
    "value": "Chicken Mexican Soup  for Canning ",
    "synonyms": ["Chicken Mexican Soup  for Canning "]
},{
    "value": "Best Ever Low-Fat Baked Chicken",
    "synonyms": ["Best Ever Low-Fat Baked Chicken"]
},{
    "value": "Fiber One Banana Nut Muffins",
    "synonyms": ["Fiber One Banana Nut Muffins"]
},{
    "value": "Macrina's Orzo Salad With Cucumber, Bell Pepper, Basil and Feta",
    "synonyms": ["Macrina's Orzo Salad With Cucumber, Bell Pepper, Basil and Feta"]
},{
    "value": "Highlands Braised Beef",
    "synonyms": ["Highlands Braised Beef"]
},{
    "value": "Kentucky Pie",
    "synonyms": ["Kentucky Pie"]
},{
    "value": "Sweet Potato Biscuits",
    "synonyms": ["Sweet Potato Biscuits"]
},{
    "value": "Apple & Cabbage Slaw with Creamy Poppy Seed Dressing",
    "synonyms": ["Apple & Cabbage Slaw with Creamy Poppy Seed Dressing"]
},{
    "value": "Fried and Stuffed Rice Balls Arancini di Riso",
    "synonyms": ["Fried and Stuffed Rice Balls Arancini di Riso"]
},{
    "value": "Greek Stuffed Portabella Mushrooms",
    "synonyms": ["Greek Stuffed Portabella Mushrooms"]
},{
    "value": "Low Fat Blueberry Protein Smoothie: No Sugar Added",
    "synonyms": ["Low Fat Blueberry Protein Smoothie: No Sugar Added"]
},{
    "value": "Marinated Beet Salad",
    "synonyms": ["Marinated Beet Salad"]
},{
    "value": "Teriyaki Meatloaf",
    "synonyms": ["Teriyaki Meatloaf"]
},{
    "value": "Roasted Beet Risotto",
    "synonyms": ["Roasted Beet Risotto"]
},{
    "value": "Red Lentil Veggie Burger Patties - Healthy, Vegan and Delicious!",
    "synonyms": ["Red Lentil Veggie Burger Patties - Healthy, Vegan and Delicious!"]
},{
    "value": "Basic Tomato-Basil Sauce",
    "synonyms": ["Basic Tomato-Basil Sauce"]
},{
    "value": "Oatmeal Cookie Pancakes",
    "synonyms": ["Oatmeal Cookie Pancakes"]
},{
    "value": "Hungarian Cucumber Salad",
    "synonyms": ["Hungarian Cucumber Salad"]
},{
    "value": "P.g.'s Bow Tie Pasta Side",
    "synonyms": ["P.g.'s Bow Tie Pasta Side"]
},{
    "value": "Smoked Paprika Pan-Roasted Potatoes",
    "synonyms": ["Smoked Paprika Pan-Roasted Potatoes"]
},{
    "value": "Italian Salad Dressing",
    "synonyms": ["Italian Salad Dressing"]
},{
    "value": "Skinny Peppermint Mocha Meringue Cookies",
    "synonyms": ["Skinny Peppermint Mocha Meringue Cookies"]
},{
    "value": "Homemade Baking Powder",
    "synonyms": ["Homemade Baking Powder"]
},{
    "value": "Grilled Chicken Caesar Salad",
    "synonyms": ["Grilled Chicken Caesar Salad"]
},{
    "value": "Cheesy Tuna and Noodles",
    "synonyms": ["Cheesy Tuna and Noodles"]
},{
    "value": "Crispy Golden Pan Fried Tofu in Sesame Seed Soy Sauce",
    "synonyms": ["Crispy Golden Pan Fried Tofu in Sesame Seed Soy Sauce"]
},{
    "value": "Calico Couscous",
    "synonyms": ["Calico Couscous"]
},{
    "value": "Roasted Chickpeas with Rosemary and Sea Salt",
    "synonyms": ["Roasted Chickpeas with Rosemary and Sea Salt"]
},{
    "value": "Apple and Rice Stuffed Acorn Squash",
    "synonyms": ["Apple and Rice Stuffed Acorn Squash"]
},{
    "value": "Wonderful Salsa",
    "synonyms": ["Wonderful Salsa"]
},{
    "value": "Oven Sweet Potato Fries",
    "synonyms": ["Oven Sweet Potato Fries"]
},{
    "value": "Arugula Salad with Caramelized Mushrooms",
    "synonyms": ["Arugula Salad with Caramelized Mushrooms"]
},{
    "value": "Weight Watchers Cheese Fries",
    "synonyms": ["Weight Watchers Cheese Fries"]
},{
    "value": "Mexican Stuffed Peppers with Quinoa and Black Beans",
    "synonyms": ["Mexican Stuffed Peppers with Quinoa and Black Beans"]
},{
    "value": "Irish Corned Beef and Cabbage Dinner",
    "synonyms": ["Irish Corned Beef and Cabbage Dinner"]
},{
    "value": "Chicken and Noodles",
    "synonyms": ["Chicken and Noodles"]
},{
    "value": "Cheese Strata",
    "synonyms": ["Cheese Strata"]
},{
    "value": "Beef and Lentil Stew with Tarragon",
    "synonyms": ["Beef and Lentil Stew with Tarragon"]
},{
    "value": "Roast Crisp Duck",
    "synonyms": ["Roast Crisp Duck"]
},{
    "value": "Pumpkin Vanilla Sugar-Free Smoothie",
    "synonyms": ["Pumpkin Vanilla Sugar-Free Smoothie"]
},{
    "value": "Spicy Roasted Eggplant Marinara",
    "synonyms": ["Spicy Roasted Eggplant Marinara"]
},{
    "value": "Julie's Dilly Beans",
    "synonyms": ["Julie's Dilly Beans"]
},{
    "value": "Pumpkin Brownies",
    "synonyms": ["Pumpkin Brownies"]
},{
    "value": "Natural Red Velvet Whoopie Pies",
    "synonyms": ["Natural Red Velvet Whoopie Pies"]
},{
    "value": "Banana Pound Cake",
    "synonyms": ["Banana Pound Cake"]
},{
    "value": "sig's Kisir  Turkish Bulgur Patties",
    "synonyms": ["sig's Kisir  Turkish Bulgur Patties"]
},{
    "value": "Cookies and Cream Banana Bread",
    "synonyms": ["Cookies and Cream Banana Bread"]
},{
    "value": "German Pancake with Berries",
    "synonyms": ["German Pancake with Berries"]
},{
    "value": "Honey Lime Dressing",
    "synonyms": ["Honey Lime Dressing"]
},{
    "value": "Cranberry Apple Breakfast Pastries",
    "synonyms": ["Cranberry Apple Breakfast Pastries"]
},{
    "value": "Pheasant in Orange Sauce",
    "synonyms": ["Pheasant in Orange Sauce"]
},{
    "value": "Beef and Bean Ranch Bake",
    "synonyms": ["Beef and Bean Ranch Bake"]
},{
    "value": "Thai Pandan Custard",
    "synonyms": ["Thai Pandan Custard"]
},{
    "value": "Elmo's Sacher Dream Torte",
    "synonyms": ["Elmo's Sacher Dream Torte"]
},{
    "value": "Cherry Vanilla Pie with a Homemade Whole Wheat Crust",
    "synonyms": ["Cherry Vanilla Pie with a Homemade Whole Wheat Crust"]
},{
    "value": "Peanut Butter Cup Cupcakes",
    "synonyms": ["Peanut Butter Cup Cupcakes"]
},{
    "value": "Tomato & Yellow Pepper Pita Pizza",
    "synonyms": ["Tomato & Yellow Pepper Pita Pizza"]
},{
    "value": "Baked Sweet Potato Fries Weight Watchers Friendly",
    "synonyms": ["Baked Sweet Potato Fries Weight Watchers Friendly"]
},{
    "value": "Garlic Fries",
    "synonyms": ["Garlic Fries"]
},{
    "value": "Crackin' Pork Belly with Spinach-Bacon Mash",
    "synonyms": ["Crackin' Pork Belly with Spinach-Bacon Mash"]
},{
    "value": "Banana Cream Cake",
    "synonyms": ["Banana Cream Cake"]
},{
    "value": "Rotini with Spinach & Ricotta",
    "synonyms": ["Rotini with Spinach & Ricotta"]
},{
    "value": "Guilt-Free Onion Soup Crock Pot",
    "synonyms": ["Guilt-Free Onion Soup Crock Pot"]
},{
    "value": "Espresso Chocolate Drop Cookies",
    "synonyms": ["Espresso Chocolate Drop Cookies"]
},{
    "value": "Meyer Lemon Chicken",
    "synonyms": ["Meyer Lemon Chicken"]
},{
    "value": "Mint Chocolate Chip Cookies",
    "synonyms": ["Mint Chocolate Chip Cookies"]
},{
    "value": "Chicken Stuffed Pockets",
    "synonyms": ["Chicken Stuffed Pockets"]
},{
    "value": "Texas Mustard Coleslaw",
    "synonyms": ["Texas Mustard Coleslaw"]
},{
    "value": "Best Banana Bread",
    "synonyms": ["Best Banana Bread"]
},{
    "value": "Sarah McLachlan's Asparagus Frittata",
    "synonyms": ["Sarah McLachlan's Asparagus Frittata"]
},{
    "value": "Savory Olive Tapenade",
    "synonyms": ["Savory Olive Tapenade"]
},{
    "value": "Roasted Vegetable Tart",
    "synonyms": ["Roasted Vegetable Tart"]
},{
    "value": "Espresso Snickerdoodles",
    "synonyms": ["Espresso Snickerdoodles"]
},{
    "value": "Stromboli",
    "synonyms": ["Stromboli"]
},{
    "value": "BBQ Chicken Pizza with Fresh Mozzarella and Pickled Jalapenos",
    "synonyms": ["BBQ Chicken Pizza with Fresh Mozzarella and Pickled Jalapenos"]
},{
    "value": "Fried Rice",
    "synonyms": ["Fried Rice"]
},{
    "value": "Pumpkin Trifle",
    "synonyms": ["Pumpkin Trifle"]
},{
    "value": "Bacon-Veggie Fried Rice",
    "synonyms": ["Bacon-Veggie Fried Rice"]
},{
    "value": "Turkey Chops with Mushrooms",
    "synonyms": ["Turkey Chops with Mushrooms"]
},{
    "value": "Pizza Biscuit Bake",
    "synonyms": ["Pizza Biscuit Bake"]
},{
    "value": "Lemon Raspberry Bars",
    "synonyms": ["Lemon Raspberry Bars"]
},{
    "value": "Mango Salsa Chicken Stir-Fry & Asparagus",
    "synonyms": ["Mango Salsa Chicken Stir-Fry & Asparagus"]
},{
    "value": "Glazed Carrots and Onions",
    "synonyms": ["Glazed Carrots and Onions"]
},{
    "value": "'V' Garlic Prawns with Sweet chilli sauce..and Fried Rice.",
    "synonyms": ["'V' Garlic Prawns with Sweet chilli sauce..and Fried Rice."]
},{
    "value": "Baileys Mint Chocolate Chip Cookies",
    "synonyms": ["Baileys Mint Chocolate Chip Cookies"]
},{
    "value": "Spicy Asian Drumsticks",
    "synonyms": ["Spicy Asian Drumsticks"]
},{
    "value": "Broccoli and Garlic Penne Pasta",
    "synonyms": ["Broccoli and Garlic Penne Pasta"]
},{
    "value": "Spicy Sausage and Sweet Potato Hash over Creamy Polenta",
    "synonyms": ["Spicy Sausage and Sweet Potato Hash over Creamy Polenta"]
},{
    "value": "Amish Dinner Rolls",
    "synonyms": ["Amish Dinner Rolls"]
},{
    "value": "Cheddar-Apple Salad",
    "synonyms": ["Cheddar-Apple Salad"]
},{
    "value": "Cream of Celery Soup",
    "synonyms": ["Cream of Celery Soup"]
},{
    "value": "Vickys Moroccan-Style Carrot & Date Salad, Gluten, Dairy, Egg & Soy-Free",
    "synonyms": ["Vickys Moroccan-Style Carrot & Date Salad, Gluten, Dairy, Egg & Soy-Free"]
},{
    "value": "Favorite Brisket",
    "synonyms": ["Favorite Brisket"]
},{
    "value": "Cornbread Salad",
    "synonyms": ["Cornbread Salad"]
},{
    "value": "Cold Beef Noodle Salad",
    "synonyms": ["Cold Beef Noodle Salad"]
},{
    "value": "Neapolitan Pancakes and Strawberry Yogurt Sauce",
    "synonyms": ["Neapolitan Pancakes and Strawberry Yogurt Sauce"]
},{
    "value": "Corn and Crab Fritters With Garlic Aioli",
    "synonyms": ["Corn and Crab Fritters With Garlic Aioli"]
},{
    "value": "Vegan Tourtiere",
    "synonyms": ["Vegan Tourtiere"]
},{
    "value": "German Chocolate Cake ~ Cheesecake Style~",
    "synonyms": ["German Chocolate Cake ~ Cheesecake Style~"]
},{
    "value": "Spinach Salad With Oranges + Dressing",
    "synonyms": ["Spinach Salad With Oranges + Dressing"]
},{
    "value": "Butter Me Up! Baked Butter Bean, Bacon and Thyme Cassoulet",
    "synonyms": ["Butter Me Up! Baked Butter Bean, Bacon and Thyme Cassoulet"]
},{
    "value": "Sesame-Miso Cucumber Salad",
    "synonyms": ["Sesame-Miso Cucumber Salad"]
},{
    "value": "Healthy Alphabet Soup",
    "synonyms": ["Healthy Alphabet Soup"]
},{
    "value": "Chili with Beef, Bell Peppers, Corn and Cookshack Chili Mix",
    "synonyms": ["Chili with Beef, Bell Peppers, Corn and Cookshack Chili Mix"]
},{
    "value": "Breakfast Quiche Bites - Low Carb, Gluten Free",
    "synonyms": ["Breakfast Quiche Bites - Low Carb, Gluten Free"]
},{
    "value": "Gingerbread",
    "synonyms": ["Gingerbread"]
},{
    "value": "Mix 'n' Match Squash Casserole",
    "synonyms": ["Mix 'n' Match Squash Casserole"]
},{
    "value": "Strawberry Shortcake Pancakes",
    "synonyms": ["Strawberry Shortcake Pancakes"]
},{
    "value": "Spaghetti and Meatballs",
    "synonyms": ["Spaghetti and Meatballs"]
},{
    "value": "Greek Chicken Salad with Lemon-Herb Dressing",
    "synonyms": ["Greek Chicken Salad with Lemon-Herb Dressing"]
},{
    "value": "Buttery English Toffee",
    "synonyms": ["Buttery English Toffee"]
},{
    "value": "Cocoa Ripple Squares",
    "synonyms": ["Cocoa Ripple Squares"]
},{
    "value": "Goulash",
    "synonyms": ["Goulash"]
},{
    "value": "Tuna Melt",
    "synonyms": ["Tuna Melt"]
},{
    "value": "Orzo / Tomato Salad with Feta and Olives",
    "synonyms": ["Orzo / Tomato Salad with Feta and Olives"]
},{
    "value": "Puffy Surprise Oven Pancakes",
    "synonyms": ["Puffy Surprise Oven Pancakes"]
},{
    "value": "Hunt's Barbecued Pork Chops",
    "synonyms": ["Hunt's Barbecued Pork Chops"]
},{
    "value": "Pink Lemonade No-Bake Mini Cheesecakes",
    "synonyms": ["Pink Lemonade No-Bake Mini Cheesecakes"]
},{
    "value": "Savory Braised Green Beans With Red Bell Pepper and Walnuts",
    "synonyms": ["Savory Braised Green Beans With Red Bell Pepper and Walnuts"]
},{
    "value": "Asparagus with Mushroom Cream Sauce",
    "synonyms": ["Asparagus with Mushroom Cream Sauce"]
},{
    "value": "Creamy Asparagus Soup With Fennel",
    "synonyms": ["Creamy Asparagus Soup With Fennel"]
},{
    "value": "Braised Chicken Thighs With Button Mushrooms",
    "synonyms": ["Braised Chicken Thighs With Button Mushrooms"]
},{
    "value": "Chicken Enchiladas",
    "synonyms": ["Chicken Enchiladas"]
},{
    "value": "Chicken and Green Chile Pumpkin Bean Stew",
    "synonyms": ["Chicken and Green Chile Pumpkin Bean Stew"]
},{
    "value": "Toaster Tarts",
    "synonyms": ["Toaster Tarts"]
},{
    "value": "Strawberry and Cream Scones",
    "synonyms": ["Strawberry and Cream Scones"]
},{
    "value": "Spinach and Ricotta Calzone",
    "synonyms": ["Spinach and Ricotta Calzone"]
},{
    "value": "Mini Banana Muffins",
    "synonyms": ["Mini Banana Muffins"]
},{
    "value": "Instant Cucumber Kimchi using Versatile Korean Flavoring Mix",
    "synonyms": ["Instant Cucumber Kimchi using Versatile Korean Flavoring Mix"]
},{
    "value": "Enchilada Sauce",
    "synonyms": ["Enchilada Sauce"]
},{
    "value": "Southern Quiche",
    "synonyms": ["Southern Quiche"]
},{
    "value": "Spiced Mango Chutney With Chiles",
    "synonyms": ["Spiced Mango Chutney With Chiles"]
},{
    "value": "Super Bowl Bratwursts!",
    "synonyms": ["Super Bowl Bratwursts!"]
},{
    "value": "Green Rice",
    "synonyms": ["Green Rice"]
},{
    "value": "Ww Bacon, Egg and Hash Browns Stacks",
    "synonyms": ["Ww Bacon, Egg and Hash Browns Stacks"]
},{
    "value": "Cola Steak Crock Pot",
    "synonyms": ["Cola Steak Crock Pot"]
},{
    "value": "Spicy Tempeh Crepes With a Savoury Carrot Cream Sauce",
    "synonyms": ["Spicy Tempeh Crepes With a Savoury Carrot Cream Sauce"]
},{
    "value": "Potato Pumpkin Mashers",
    "synonyms": ["Potato Pumpkin Mashers"]
},{
    "value": "Creamed Celery With Blue Cheese",
    "synonyms": ["Creamed Celery With Blue Cheese"]
},{
    "value": "Banana Nut Cake with Whipped Cream Frosting",
    "synonyms": ["Banana Nut Cake with Whipped Cream Frosting"]
},{
    "value": "Bechamel Sauce",
    "synonyms": ["Bechamel Sauce"]
},{
    "value": "Fried Brown Rice with Chicken, Broccoli, Carrot & Bean Sprouts",
    "synonyms": ["Fried Brown Rice with Chicken, Broccoli, Carrot & Bean Sprouts"]
},{
    "value": "AMIEs SINIGANG na HIPON",
    "synonyms": ["AMIEs SINIGANG na HIPON"]
},{
    "value": "6 Ingredient Peanut Butter Chocolate Chip Cookies",
    "synonyms": ["6 Ingredient Peanut Butter Chocolate Chip Cookies"]
},{
    "value": "Warm Roasted Potato and Green Bean Salad",
    "synonyms": ["Warm Roasted Potato and Green Bean Salad"]
},{
    "value": "French Croissant",
    "synonyms": ["French Croissant"]
},{
    "value": "Broccoli Cheddar Soup",
    "synonyms": ["Broccoli Cheddar Soup"]
},{
    "value": "Golden Crescents",
    "synonyms": ["Golden Crescents"]
},{
    "value": "Jolenes Oatmeal Chocolate Chip Cookies",
    "synonyms": ["Jolenes Oatmeal Chocolate Chip Cookies"]
},{
    "value": "Gingered Acorn Squash Soup",
    "synonyms": ["Gingered Acorn Squash Soup"]
},{
    "value": "White Chocolate Blackberry Ice Cream",
    "synonyms": ["White Chocolate Blackberry Ice Cream"]
},{
    "value": "Smashed Potatoes with Goat Cheese and Chives",
    "synonyms": ["Smashed Potatoes with Goat Cheese and Chives"]
},{
    "value": "Ww Mini Chocolate Chip Cookies Ww",
    "synonyms": ["Ww Mini Chocolate Chip Cookies Ww"]
},{
    "value": "Lemon-Garlic Swiss Chard",
    "synonyms": ["Lemon-Garlic Swiss Chard"]
},{
    "value": "Hi-Protein Honey Wheat Bread",
    "synonyms": ["Hi-Protein Honey Wheat Bread"]
},{
    "value": "AMIEs GINATANG TULINGAN tuna fish with coco milk",
    "synonyms": ["AMIEs GINATANG TULINGAN tuna fish with coco milk"]
},{
    "value": "Blueberry Kuchen",
    "synonyms": ["Blueberry Kuchen"]
},{
    "value": "Light Cucumber Salad with a Thai Red Pepper Lime Dressing",
    "synonyms": ["Light Cucumber Salad with a Thai Red Pepper Lime Dressing"]
},{
    "value": "Barbecued Black-Eyed Peas",
    "synonyms": ["Barbecued Black-Eyed Peas"]
},{
    "value": "Yakhni Puloa",
    "synonyms": ["Yakhni Puloa"]
},{
    "value": "Ww 5 Points - Mexican Beef and Cheese Wontons With Salsa",
    "synonyms": ["Ww 5 Points - Mexican Beef and Cheese Wontons With Salsa"]
},{
    "value": "Taco Salad",
    "synonyms": ["Taco Salad"]
},{
    "value": "Pear Jam",
    "synonyms": ["Pear Jam"]
},{
    "value": "Honey-Orange Glazed Corn Bread",
    "synonyms": ["Honey-Orange Glazed Corn Bread"]
},{
    "value": "Salisbury Steak",
    "synonyms": ["Salisbury Steak"]
},{
    "value": "Salted Caramel Apple Cheesecake Bites",
    "synonyms": ["Salted Caramel Apple Cheesecake Bites"]
},{
    "value": "Taco Wontons",
    "synonyms": ["Taco Wontons"]
},{
    "value": "Spicy and Sweet Peach Jam",
    "synonyms": ["Spicy and Sweet Peach Jam"]
},{
    "value": "Tropical Carrot Layer Cake With Island Icing",
    "synonyms": ["Tropical Carrot Layer Cake With Island Icing"]
},{
    "value": "Spaghetti with Meat Sauce",
    "synonyms": ["Spaghetti with Meat Sauce"]
},{
    "value": "Chez Sovan Cambodian Beef Sticks",
    "synonyms": ["Chez Sovan Cambodian Beef Sticks"]
},{
    "value": "Roasted Autumn Panzanella Salad",
    "synonyms": ["Roasted Autumn Panzanella Salad"]
},{
    "value": "Chewy Granola Bars",
    "synonyms": ["Chewy Granola Bars"]
},{
    "value": "Sweet Potato Nutella Roll",
    "synonyms": ["Sweet Potato Nutella Roll"]
},{
    "value": "Whole Wheat Banana Puff Pancake Sugar-Free",
    "synonyms": ["Whole Wheat Banana Puff Pancake Sugar-Free"]
},{
    "value": "Sig's Garlic and Lemon Chicken Kebabs with a garlic dip",
    "synonyms": ["Sig's Garlic and Lemon Chicken Kebabs with a garlic dip"]
},{
    "value": "Luchows Sauerbraten",
    "synonyms": ["Luchows Sauerbraten"]
},{
    "value": "Spicy Lobster Pasta",
    "synonyms": ["Spicy Lobster Pasta"]
},{
    "value": "Orange Juice, Olive Oil and Pine Nuts Cake",
    "synonyms": ["Orange Juice, Olive Oil and Pine Nuts Cake"]
},{
    "value": "Dessert Apple Rings",
    "synonyms": ["Dessert Apple Rings"]
}]

var FuzzyMatching = require('fuzzy-matching');
 


function init_fuzzy_match(){ 
    var names = [];
    for (let obj of entities) { 
        names.push(obj['value']);
    }
    return new FuzzyMatching(names);
}

module.exports = {
    init_fuzzy_match : init_fuzzy_match
}